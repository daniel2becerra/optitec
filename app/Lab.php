<?php

namespace Optitec;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lab extends Model
{
    use SoftDeletes;

    protected $table = 'labs';
	protected $fillable = ['name', 'address', 'phone', 'contactName', 'contactPhone'];
    protected $dates = ['deleted_at'];

	public function filterlens()
    {
        return $this->hasMany('Optitec\Filterlen');
    }

    public function typelens()
    {
        return $this->hasMany('Optitec\Typelen');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($lab)
        {
            $lab->typelens->each(
                function($typelens)
                {
                    $typelens->delete();
                }); 
            $lab->filterlens()->delete();
        });
    }

}
