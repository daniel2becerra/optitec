<?php

namespace Optitec;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model

{
	use SoftDeletes;

    protected $table = 'clients';
	protected $fillable = ['identification', 'firstname', 'lastname', 'email', 'phone', 'birthday', 'profession'];
	protected $dates = ['deleted_at'];
}
