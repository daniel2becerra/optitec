<?php

namespace Optitec;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Settlement extends Model
{
    use SoftDeletes;
    protected $table = 'settlements';
	protected $fillable = ['user_id', 'entrega', 'recibe', 'gastos', 'revisado', 'pendiente', 'notas'];
	protected $dates = ['deleted_at'];
}
