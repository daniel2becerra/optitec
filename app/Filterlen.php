<?php

namespace Optitec;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Filterlen extends Model
{
	use SoftDeletes;
	
    protected $table = 'filterlens';
	protected $fillable = ['name', 'lab_id', 'price'];
	protected $dates = ['deleted_at'];

	public function lab()
    {
        return $this->belongsTo('Optitec\Lab');
    }
}
