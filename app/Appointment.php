<?php

namespace Optitec;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appointment extends Model
{
	use SoftDeletes;
    protected $table = 'appointments';

    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
        'title', 'phone','start', 'end', 'color'
    ];
    
    protected $dates = ['deleted_at'];
}
