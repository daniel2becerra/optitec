<?php

namespace Optitec;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orderproduct extends Model
{
    use SoftDeletes;
    protected $table = 'orderproducts';
	protected $fillable = ['order_id', 'typeproduct_id', 'product_id', 'productname', 'price'];
    protected $dates = ['deleted_at'];
}
