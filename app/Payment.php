<?php

namespace Optitec;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;
    protected $table = 'payments';
	protected $fillable = ['order_id', 'price', 'settlement_id', 'status_id', 'user_id'];
	protected $dates = ['deleted_at'];
}
