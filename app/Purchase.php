<?php

namespace Optitec;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{
    use SoftDeletes;
    protected $table = 'purchases';
	protected $fillable = ['invoicepurchase', 'seller'];
    protected $dates = ['deleted_at'];
}
