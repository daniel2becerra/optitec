<?php

namespace Optitec;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Diagnosis extends Model
{
    use SoftDeletes;
    protected $table = 'diagnoses';
	protected $fillable = ['medicalhistory_id', 'cie10_id'];
	protected $dates = ['deleted_at'];
}
