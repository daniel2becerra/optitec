<?php

namespace Optitec;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Materiallen extends Model
{
    use SoftDeletes;
    protected $table = 'materiallens';
	protected $fillable = ['name', 'typelen_id', 'price', 'lab_id'];
    protected $dates = ['deleted_at'];

	public function typelen()
    {
        return $this->belongsTo('Optitec\Typelen');
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function($materiallen)
        {
            $materiallen->materialmarkprices()->delete();
        });
    }
}
