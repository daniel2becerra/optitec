<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('Portal', 'AdminController@index');
Route::get('/', 'LogController@index');
Route::get('/logout', 'LogController@index');
Route::Resource('login', 'LogController');
Route::get('Calendar', 'AppointmentController@calendar');
Route::post('/uploadfile', 'FileController@uploadfile');
Route::get('/downloadfile', 'FileController@downloadfile');
Route::get('/Products/upload', 'ProductController@upload');
Route::get('/Clients/createwoh', 'ClientController@createwoh');
Route::get('/Reports/facturas', 'ReportController@factura');
Route::post('/Clients/quickstore', ['as' => 'Clients.quickstore', 'uses' =>  'ClientController@quickstore']);
Route::Resource('Users', 'UserController');
Route::Resource('Products', 'ProductController');
Route::Resource('Labs', 'LabController');
Route::Resource('Orders', 'OrderController');
Route::Resource('Orderproducts', 'OrderproductController');
Route::Resource('Payments', 'PaymentController');
Route::Resource('Clients', 'ClientController');
Route::Resource('Appointments', 'AppointmentController');
Route::Resource('Invoices', 'InvoiceController');
Route::Resource('Quicksale', 'QuicksaleController');
Route::Resource('Purchases', 'PurchaseController');
Route::Resource('Settlements', 'SettlementController');
Route::get('/JsonCliente', 'ClientController@jsonCliente');
Route::get('/JsonMedicalHistory', 'MedicalhistoryController@jsonMedicalHistory');
Route::get('/Medicalhistory', 'MedicalhistoryController@Medicalhistory');
Route::get('/JsonMedicalHistoryDate', 'MedicalhistoryController@jsonMedicalHistoryDate');
Route::get('/Factura/abono', ['as' => 'Factura.abono', 'uses' =>  'FacturaController@abono']);
Route::get('/Factura/facturaventa', ['as' => 'Factura.facturaventa', 'uses' => 'FacturaController@facturaventa']);
Route::get('/Factura/facturaventaold', ['as' => 'Factura.facturaventaold', 'uses' => 'FacturaController@facturaventaold']);
Route::get('/Factura/historiaclinica', ['as' => 'Factura.historiaclinica', 'uses' =>  'FacturaController@historiaclinica']);
Route::get('/Factura/lentes', ['as' => 'Factura.lentes', 'uses' =>  'FacturaController@lentes']);
Route::get('/Factura/pedirlentes', ['as' => 'Factura.pedirlentes', 'uses' =>  'FacturaController@pedirlentes']);
Route::get('/Factura/recomendaciones', ['as' => 'Factura.recomendaciones', 'uses' =>  'FacturaController@recomendaciones']);
Route::get('JsonProduct', 'ProductController@jsonProduct');
Route::get('JsonTypelen', 'OrderController@jsonTypelen');
Route::get('JsonFilterlenLab', 'OrderController@jsonFilterlenLab');
Route::get('JsonFilterlenId', 'OrderController@jsonFilterlenId');
Route::get('JsonMateriallen', 'OrderController@jsonMateriallen');
Route::get('JsonMaterialPrice', 'OrderController@jsonMaterialPrice');
Route::get('JsonMarklen', 'OrderController@jsonMarklen');
Route::get('JsonOrderIdentification', 'OrderController@jsonOrderIdentification');
Route::get('JsonOrderName', 'OrderController@jsonOrderName');
Route::get('JsonOrderNumber', 'OrderController@jsonOrderNumber');
Route::get('JsonOrderDate', 'OrderController@jsonOrderDate');
Route::get('JsonFacturaIdentification', 'ReportController@jsonFacturaIdentification');
Route::get('JsonFacturaName', 'ReportController@jsonFacturaName');
Route::get('JsonFacturaNumber', 'ReportController@jsonFacturaNumber');
Route::get('JsonFacturaDate', 'ReportController@jsonFacturaDate');
Route::get('JsonMarklen', 'OrderController@jsonMarklen');
Route::get('pdf', 'AdminController@pdf');
Route::post('Mailmassive', ['as' => 'Mailmassive', 'uses' => 'EmailController@mailmassive']);
Route::get('Contactmassive', 'EmailController@contactmassive');
Route::post('Mailsingle', ['as' => 'Mailsingle', 'uses' => 'EmailController@mailsingle']);
Route::get('Contactsingle', 'EmailController@contactsingle');