<?php

namespace Optitec\Http\Middleware;

use Closure;
use Auth;
use Session;
use Redirect;

class Administrador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::User()->role_id == 2)
        {
            Session::flash('message-error', 'Sin permisos');
            return Redirect('/Portal');
        }
        return $next($request);
    }
}
