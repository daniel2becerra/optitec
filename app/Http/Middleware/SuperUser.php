<?php

namespace Optitec\Http\Middleware;

use Closure;
use Auth;
use Session;

class SuperUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::User()->role_id == 1)
        {
            Session::flash('message-error', 'Sin permisos');
            return Redirect('/Portal');
        }
        return $next($request);
    }
}
