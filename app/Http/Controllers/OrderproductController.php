<?php

namespace Optitec\Http\Controllers;

use Illuminate\Http\Request;

use Optitec\Http\Requests;
use Optitec\Http\Controllers\Controller;
use Optitec\Orderproduct;
use Optitec\Product;
use Optitec\Purchaseproduct;
use Session;
use Response;

class OrderproductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Product = Product::where('products.id', $request->product_id)->select('products.*')->first();
        $PriceProduct = Purchaseproduct::where('purchaseproducts.product_id', $request->product_id)->select('purchaseproducts.*')->orderBy('purchaseproducts.created_at', 'desc')->first();
        if($request->typeproduct_id ==3)
        {
            $Nproduct=
            [
                'order_id'=>$request->order_id,
                'typeproduct_id' =>$request->typeproduct_id,
                'product_id'=>$request->product_id,
                'productname'=>$Product->name."-".$Product->reference."-".$Product->color,
                'price'=>$PriceProduct->salePrice,
            ];
            $NewProduct = Orderproduct::create($Nproduct);
        }
        else if($request->typeproduct_id ==4)
        {
            $Nproduct=
            [
                'order_id'=>$request->order_id,
                'typeproduct_id' =>$request->typeproduct_id,
                'product_id'=>$request->product_id,
                'productname'=>'Descuento',
                'price'=>$request->price * -1,
            ];
            $NewProduct = Orderproduct::create($Nproduct);
        }
        return Response::Json(['created_at'=> $NewProduct->created_at, 'name' =>$Product->name."-".$Product->reference."-".$Product->color, 'price'=>$NewProduct->price, 'id'=>$NewProduct->id]); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $p = Orderproduct::find($id);
        Orderproduct::destroy($id);
        return Response::Json(['price'=> $p->price, 'type'=> 1]); 
    }
}
