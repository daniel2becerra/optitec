<?php

namespace Optitec\Http\Controllers;

use Illuminate\Http\Request;
use Optitec\Http\Requests;
use Optitec\Http\Controllers\Controller;
use Optitec\Client;
use Optitec\Diagnosis;
use Optitec\cie10;
use Optitec\Medicalhistory;
use Optitec\Order;
use Input;
use Response;
use Session;
use Redirect;
use DB;


class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('superuser', ['only' => []]);
        $this->middleware('administrador', ['only' => []]);
        $this->middleware('medico', ['only' => ['destroy']]);
        $this->middleware('asesor', ['except' => ['jsonCliente', 'quickstore']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function jsonCliente()
    {
        $identification = Input::get('identification');
        $client = Client::where('identification', $identification)->get();

        return Response::json($client);
    }

    public function index()
    {
        $clients = Client::paginate(15); 
        return view('Client.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cie10s = DB::table('cie10s')->lists('dec10','id');
        $labs = DB::table('labs')->lists('name','id');
         return view('Client.create', compact('cie10s', 'labs'));
    }

    public function createwoh()
    {
        $cie10s = DB::table('cie10s')->lists('dec10','id');
         return view('Client.createwoh', compact('cie10s'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $requesttype_id = DB::table('typelens')->max('id');
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = Client::where("identification","=",$request->identification)->first();
        if(count($client)==0)
        {
            $client = Client::create($request->all());
        }
         $client_id = $client->id;
        $MH = Medicalhistory::create(array_merge($request->all(), ['client_id' => $client_id]));
        for ($i=0; $i < count($request->diagnosticos); $i++)
        { 
            $diagnostico=
                [
                    'medicalhistory_id'=>$MH->id,
                    'cie10_id'=>$request->diagnosticos[$i],
                ];
                Diagnosis::create($diagnostico);
        }
        return Response::Json(['id'=> $MH->id]);  
    }

    public function quickstore(Request $request)
    {
        $Client = Client::where("identification","=",$request->identificationinit)->first();
        if(count($Client)==0)
        {
            $client =
            [
                'identification'=>$request->identificationinit,
                'firstname'=>$request->firstname,
                'lastname'=>$request->lastname,
                'email'=>$request->email,
                'phone'=>$request->phone,
                'birthday'=>$request->birthday,
                'profession'=>$request->profession,
            ];
        $Client = Client::create($client);
        }
        $order =
        [
            'client_id'=>$Client->id,
            'typeorder_id'=>$request->typeorder_id,
        ];
        $Order = Order::create($order);
        return Response::Json(['client_id'=> $Client->id, 'order_id' => $Order->id]);  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Client = Client::find($id);
        return view('Client.edit', ['client'=>$Client]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Client = Client::find($id);
        $Client->fill($request->all());
        $Client->save();
        Session::flash('message', 'cliente modificado correctamente');
        return Redirect::to('/Clients'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Client::destroy($id);
        Session::flash('message', 'cliente eliminado correctamente');
        return Redirect::to('/Clients'); 
    }
}
