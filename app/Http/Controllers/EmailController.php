<?php

namespace Optitec\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Optitec\Http\Requests;
use Optitec\Http\Controllers\Controller;

class EmailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('superuser', ['only' => []]);
        $this->middleware('administrador', ['only' => []]);
        $this->middleware('medico');
        $this->middleware('asesor', ['only' => []]);
    }
    
    public function contactmassive()
    {
        return view('Mail.mailmassive');
    }

    public function contactsingle()
    {
        return view('Mail.mailsingle');
    }

    public function mailmassive(Request $request)
    {
        //guarda el valor de los campos enviados desde el form en un array
      $data = $request->all();
      $name = 'Daniel Becerra';
       //se envia el array y la vista lo recibe en llaves individuales {{ $email }} , {{ $subject }}...
       Mail::send('Mail.sendMail', $data, function($message) use ($data) {
            $message->to($data['email'], $name)->subject($data['subject']);
        }); 
        return Redirect('/');
    }

    public function mailsingle(Request $request)
    {
        //guarda el valor de los campos enviados desde el form en un array
       $data = $request->all();
       //se envia el array y la vista lo recibe en llaves individuales {{ $email }} , {{ $subject }}...
       Mail::send('Mail.sendMail', $data, function($message) use ($data) {
            $name = 'Daniel Becerra';
            $message->to($data['email'], $name)->subject($data['subject']);
        }); 
      return Redirect('/');
    }
}
