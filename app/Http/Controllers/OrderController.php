<?php

namespace Optitec\Http\Controllers;

use Illuminate\Http\Request;
use Optitec\Lab;
use Optitec\Typelen;
use Optitec\Materiallen;
use Optitec\Filterlen;
use Optitec\Order;
use Optitec\Payment;
use Optitec\Medicalhistory;
use Optitec\Client;
use Optitec\Product;
use Optitec\Orderproduct;
use Optitec\Invoice;
use DB;
use Input;
use Response;
use Auth;
use Carbon\Carbon;
use Optitec\Http\Requests;
use Optitec\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('superuser', ['only' => []]);
        $this->middleware('administrador', ['only' => []]);
        $this->middleware('medico');
        $this->middleware('asesor', ['only' => ['destroy']]);
    }

    public function jsonTypelen()
    {
        $idlab = Input::get('lab_id');
        $typelens = DB::table('typelens')->where('lab_id', $idlab)->where('deleted_at', null)->lists('name','id'); 
        return Response::json($typelens);
    }

    public function jsonMateriallen()
    {
        $idtype = Input::get('typelen_id');
        $materiallens = DB::table('materiallens')->where('typelen_id', $idtype)->where('deleted_at', null)->lists('name','id'); 
        return Response::json($materiallens);
    }

    public function jsonMaterialPrice()
    {
        $idmaterial = Input::get('materiallen_id');
        $price = DB::table('materiallens')->where('id', $idmaterial)->where('deleted_at', null)->first(); 
        return Response::json($price);
    }

    public function jsonFilterlenLab()
    {
        $idlab = Input::get('lab_id');
        $filterlens = DB::table('filterlens')->where('lab_id', $idlab)->where('deleted_at', null)->lists('name','id'); 
        return Response::json($filterlens);
    }

    public function jsonFilterlenId()
    {
        $id = Input::get('id');
        $price = DB::table('filterlens')->where('id', $id)->first();
        return Response::json($price);
    }

    public function jsonOrderIdentification()
    {
        $identification = Input::get('identification');
        $Order = DB::table('orders')
                ->leftjoin('clients', 'clients.id', '=', 'orders.client_id')
                ->where('clients.identification', '=', $identification)
                ->select('clients.identification', 'clients.firstname', 'clients.lastname', 'orders.*')
                ->get();

        return Response::json($Order);
    }

    public function jsonOrderName()
    {
        $name = Input::get('name');
        if($name != "" || $name != null)
        {
            
            $Order = DB::table('orders')
                    ->join('clients', 'clients.id', '=', 'orders.client_id')
                    ->where('clients.firstname', 'like', '%'.$name.'%')
                    ->Orwhere('clients.lastname', 'like', '%'.$name.'%')
                    ->select('clients.identification', 'clients.firstname', 'clients.lastname', 'orders.*')
                    ->get();
        }

        return Response::json($Order);
    }

    public function jsonOrderNumber()
    {
        $number = Input::get('number');
        $Order = DB::table('orders')
                ->join('clients', 'clients.id', '=', 'orders.client_id')
                ->where('orders.id', '=', $number)
                ->select('clients.identification', 'clients.firstname', 'clients.lastname', 'orders.*')
                ->get();

        return Response::json($Order);
    }

    public function jsonOrderDate()
    {
        $FI = Input::get('FI');
        $FF = Carbon::parse(Input::get('FF'))->addDay(1);
        $Order = DB::table('orders')
                ->join('clients', 'clients.id', '=', 'orders.client_id')
                ->where('orders.created_at', '>', $FI)
                ->where('orders.created_at', '<=', $FF)
                ->select('clients.identification', 'clients.firstname', 'clients.lastname', 'orders.*')
                ->get();

        return Response::json($Order);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Order.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $labs = DB::table('labs')->lists('name','id');
        $productsquery = DB::select( DB::raw("SELECT products.id, CONCAT(products.name, '-', products.reference, '-',products.color, '-', purchaseproducts.salePrice)  AS NP
                                    FROM purchaseproducts 
                                    JOIN products
                                    ON purchaseproducts.product_id = products.id
                                     ORDER BY id ASC"));
        $keys = [];
        $values = [];
        $products = [];
        foreach ($productsquery as $productsquery)
        {
            array_push($keys, $productsquery->id);
            array_push($values, $productsquery->NP);
        }
        $products = array_combine($keys, $values);
        return view('Order.create', compact('labs', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Order = Order::create($request->all());
        $productnamemt= DB::table('materiallens')
                    ->where('materiallens.id', '=', $request->materialprice_id)
                    ->select('materiallens.name AS NAMEMATERIAL')->first();

        $fl= DB::table('filterlens')
                    ->where('filterlens.id', '=', $request->filterlen_id)
                    ->select('filterlens.name AS NAMEFILTER')->first();
        $productfilter = $fl->NAMEFILTER;

        $pr= DB::table('products')
                    ->where('products.id', '=', $request->product_id)
                    ->select(DB::raw("CONCAT(products.name, '-', products.reference, '-', products.color) AS NAMEPRODUCT"))->first();
        $product = $pr->NAMEPRODUCT;

        $lente=
        [
            'order_id'=>$Order->id,
            'typeproduct_id' => 1,
            'product_id'=>$request->materialprice_id,
            'productname'=>$productnamemt->NAMEMATERIAL,
            'price'=>$request->pricelen,
        ];
        Orderproduct::create($lente);

        $filtro=
        [
            'order_id'=>$Order->id,
            'typeproduct_id' => 2,
            'product_id'=>$request->filterlen_id,
            'productname'=>$fl->NAMEFILTER,
            'price'=>$request->pricefilter,
        ];
        Orderproduct::create($filtro);

        $montura=
        [
            'order_id'=>$Order->id,
            'typeproduct_id' => 3,
            'product_id'=>$request->product_id,
            'productname'=>$product,
            'price'=>$request->priceproduct,
        ];
        Orderproduct::create($montura);

        $pago=
        [
            'order_id'=>$Order->id,
            'price'=>$request->paymentprice,
            'user_id'=>Auth::User()->id,
        ];
        $P= Payment::create($pago);
        return Response::Json(['order_id'=> $Order->id, 'payment_id'=> $P->id]);  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $Order = Order::find($id);
        $Client = Client::find($Order->client_id);
        $MedicalHistory = Medicalhistory::find($Order->medicalhistory_id);
        $products = Product::select(DB::raw('CONCAT(name,"-",reference, "-", color)  AS NP'), 'id')->where('products.id', '<>', 1)->lists('NP', 'id');
        $Lab = Lab::find($Order->lab_id);
        $Typelen = Typelen::find($Order->typelen_id);
        $Materiallen = Materiallen::find($Order->materiallen_id);
        $Filterlen = Filterlen::find($Order->filterlen_id);
        $Payments = Payment::where('Payments.order_id', '=', $id)->get();
        $Invoice = Invoice::where('invoices.order_id', '=', $id)->get();
        $Factura = count($Invoice) > 0 ? True : False;
        $Orderproducts = Orderproduct::where('Orderproducts.order_id', '=', $id)->get();

        return view('Order.show', compact('Order', 'Client', 'Factura', 'MedicalHistory', 'products', 'Orderproducts','Lab', 'Typelen', 'Materiallen', 'Filterlen', 'Payments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
