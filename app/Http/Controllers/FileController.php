<?php

namespace Optitec\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use Session;
use Excel;
use Input;
use DB;
use Optitec\Product;
use Optitec\Purchase;
use Optitec\Purchaseproduct;
use Optitec\Http\Requests;
use Optitec\Http\Controllers\Controller;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function uploadfile(Request $request)
    {
      $archivo = $request->file('archivo');
      $nombre= "cargaproductos.xlsx";
      $r1=Storage::disk('archivos')->put($nombre,  \File::get($archivo) );
      $ruta  =  storage_path('archivos') ."/". $nombre;
      $errors = false;
      if($r1)
      {
        Excel::selectSheetsByIndex(0)->load($ruta, function($hoja) use($errors)
        {
          $hoja->each(function($fila) use($errors)
          {
            $this->errors = $errors;
            if(($fila->nombre=='' || $fila->nombre==null) &&
               ($fila->referencia=='' || $fila->referencia==null) &&
               ($fila->color=='' || $fila->color==null) &&
               ($fila->cantidad=='' || $fila->cantidad==null) && 
               ($fila->precio_compra=='' || $fila->precio_compra==null) && 
               ($fila->precio_venta=='' || $fila->precio_venta==null))
            {
              return false;
            }
            if($fila->nombre=='' || $fila->nombre==null || 
              $fila->referencia=='' || $fila->referencia==null ||
              $fila->color=='' || $fila->color==null || 
              $fila->cantidad=='' || $fila->cantidad==null || 
              $fila->precio_compra=='' || $fila->precio_compra==null || 
              $fila->precio_venta=='' || $fila->precio_venta==null)
            {
              $this->errors = true;
            }          
          });
          if(!$this->errors)
          {          
            $purchase = 
            [
              'invoicepurchase' => '00001',
              'seller' => 'inventario inicial'
            ];
            $Purchase = Purchase::create($purchase);
            $hoja->each(function($fila) use($Purchase)
            {
              $exist = DB::table('products')
                        ->where('products.name', '=', $fila->nombre)
                        ->where('products.reference', '=', $fila->referencia)
                        ->where('products.color', '=', $fila->color)
                        ->first();
              if($exist)
              {
                $purchaseproduct = 
                [
                  'purchase_id' => $Purchase->id,
                  'product_id' => $exist->id, 
                  'purchasePrice' => $fila->precio_compra, 
                  'salePrice' => $fila->precio_venta, 
                  'quantity' => $fila->cantidad
                ];
                Purchaseproduct::create($purchaseproduct);
                return;
              }
              if(($fila->nombre=='' || $fila->nombre==null) &&
                 ($fila->referencia=='' || $fila->referencia==null) &&
                 ($fila->color=='' || $fila->color==null) &&
                 ($fila->cantidad=='' || $fila->cantidad==null) && 
                 ($fila->precio_compra=='' || $fila->precio_compra==null) && 
                 ($fila->precio_venta=='' || $fila->precio_venta==null))
              {
                Session::flash('message', 'productos creados correctamente');
                return false;
              }
              $product = new Product;
              $product->name= $fila->nombre;
              $product->reference= $fila->referencia;
              $product->color= $fila->color;
              $product->save();  
              $purchaseproduct = 
                [
                  'purchase_id' => $Purchase->id,
                  'product_id' => $product->id, 
                  'purchasePrice' => $fila->precio_compra, 
                  'salePrice' => $fila->precio_venta, 
                  'quantity' => $fila->cantidad
                ];
                Purchaseproduct::create($purchaseproduct);           
            });
          }          
        });  
        if($this->errors)
        {
          Session::flash('message-error', 'existen campos vacios');
          return Redirect('/Products/upload');
        }
        else
        {
          Session::flash('message', 'Carga correcta!!');
          return Redirect('/Products/upload');
        }             
      }
      else
      {
        Session::flash('message-error', 'error al subir el archivo');
        return Redirect('/Products/upload');
      }
        
    }

    public function downloadfile()
    {
        $nameFile = Input::get('fileName');
        $path = storage_path('archivos/'.$nameFile);
        return response()->download($path);
    }          
}
