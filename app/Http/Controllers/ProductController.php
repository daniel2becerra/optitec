<?php

namespace Optitec\Http\Controllers;

use Illuminate\Http\Request;
use Optitec\Http\Requests\ProductRequest;
use Optitec\Http\Requests;
use Optitec\Http\Controllers\Controller;
use Optitec\Product;
use Input;
use Response;
use Session;
use Redirect;
use DB;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('superuser', ['only' => []]);
        $this->middleware('administrador', ['only' => []]);
        $this->middleware('medico', ['only' => ['create', 'store', 'edit', 'update', 'destroy', 'show']]);
        $this->middleware('asesor', ['only' => ['edit', 'update', 'destroy', 'show']]);
    }

    public function jsonProduct()
    {
        $product = Input::get('product_id');
        $products = DB::table('products')
                    ->select('products.*', 'P.PRECIOVENTA')
                    ->join(DB::raw('
                    (SELECT purchaseproducts.product_id, MAX(purchaseproducts.id), purchaseproducts.salePrice AS PRECIOVENTA FROM purchaseproducts
                    WHERE purchaseproducts.deleted_at IS NULL
                    GROUP BY purchaseproducts.product_id) P'), 
                    function($join)
                    {
                       $join->on('products.id', '=', 'P.product_id');
                    })
                    ->where('products.id', '=', $product)
                    ->groupBy('products.id')
                    ->OrderBy('products.name')->first();
        return Response::json($products);
    }

    public function upload()
    {
        return view('Product.upload');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(10);
        /*$products = DB::table('products')
                        ->leftjoin('purchaseproducts', 'products.id', '=', 'purchaseproducts.product_id')
                        ->leftjoin('orderproducts', 'products.id', '=', 'orderproducts.product_id')
                        ->where('products.id', '<>', 1)
                        ->where('orderproducts.deleted_at', '=', null)
                        ->select('products.*')
                        ->addSelect(DB::raw('COALESCE(SUM(purchaseproducts.quantity),0) AS COMPRAS'))
                        ->addSelect(DB::raw('COALESCE(COUNT(orderproducts.product_id),0) AS VENTAS'))
                        ->groupBy('products.id')
                        ->OrderBy('products.name')
                        ->paginate(10);

        $products = DB::select( DB::raw("SELECT products.*, COALESCE(SUM(purchaseproducts.quantity), 0) AS COMPRAS, COALESCE(V.VENTAS, 0) AS VENTAS
                    FROM products
                    LEFT JOIN (
                    SELECT orderproducts.*, COUNT(orderproducts.product_id) AS VENTAS FROM orderproducts
                    WHERE orderproducts.deleted_at IS NULL
                    GROUP BY orderproducts.product_id) AS V
                    ON products.id = V.product_id
                    LEFT JOIN purchaseproducts
                    ON purchaseproducts.product_id = products.id
                    WHERE products.id <> 1
                    GROUP BY products.id
                    ORDER BY products.name"));*/

        $products = DB::table('products')
                    ->select('products.*')
                    ->addSelect(DB::raw('COALESCE(SUM(purchaseproducts.quantity),0) AS COMPRAS'))
                    ->addSelect(DB::raw('COALESCE(V.VENTAS,0) AS VENTAS'))
                    ->leftjoin(DB::raw('
                    (SELECT orderproducts.product_id, COUNT(orderproducts.product_id) AS VENTAS FROM orderproducts
                    WHERE orderproducts.deleted_at IS NULL
                    GROUP BY orderproducts.product_id) V'), 
                    function($leftjoin)
                    {
                       $leftjoin->on('products.id', '=', 'V.product_id');
                    })
                    ->leftjoin('purchaseproducts', 'purchaseproducts.product_id', '=', 'products.id')
                    ->where('products.id', '<>', 1)
                    ->groupBy('products.id')
                    ->OrderBy('products.name')
                    ->paginate(10);
        return view('Product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('Product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        Product::create($request->all());
        Session::flash('message', 'producto creado correctamente');
        return Redirect('/Products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Product = Product::find($id);
        return view('Product.edit', ['product'=>$Product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Product = Product::find($id);
        $Product->fill($request->all());
        $Product->save();
        Session::flash('message', 'producto modificado correctamente');
        return Redirect::to('/Products'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);
        Session::flash('message', 'producto eliminado correctamente');
        return Redirect::to('/Products'); 
    }
}
