<?php

namespace Optitec\Http\Controllers;

use Illuminate\Http\Request;
use Optitec\Http\Requests;
use Optitec\Http\Controllers\Controller;
use Optitec\Http\Requests\PurchaseRequest;
use Optitec\Product;
use Optitec\Purchase;
use Optitec\Purchaseproduct;
use Redirect;
use DB;
use Session;

class PurchaseController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('superuser', ['only' => []]);
        $this->middleware('administrador', ['only' => []]);
        $this->middleware('medico');
        $this->middleware('asesor', ['only' => ['edit', 'update', 'destroy', 'show']]);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{

		$Purchases = DB::table('purchases')
						->join('purchaseproducts', 'purchases.id', '=', 'purchaseproducts.purchase_id')
						->where('purchases.deleted_at', '=', null)
						->where('purchaseproducts.deleted_at', '=', null)
						->groupBy('purchases.id')
						->select('purchases.*')
						->addSelect(DB::raw('SUM(purchaseproducts.purchasePrice * purchaseproducts.quantity) AS TOTAL'))
						->paginate(10);
		return view('Purchase.index', compact('Purchases'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$products = Product::select(DB::raw('CONCAT(name,"-",reference, "-", color)  AS NP'), 'id')->where('products.id', '<>', 1)->lists('NP', 'id');
		$purchaseproducts = Purchaseproduct::where('purchaseproducts.purchase_id', '=', -1)->get();
		return view('Purchase.create', compact('products', 'purchaseproducts'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(PurchaseRequest $request)
	{
		$Purchase = Purchase::create($request->all());
		for ($i=0; $i < count($request->product_id); $i++)
		{ 
			$purchaseproduct = 
			[
				'purchase_id'=>$Purchase->id,
				'product_id' =>$request->product_id[$i],
				'purchasePrice'=>$request->purchasePrice[$i],
				'salePrice'=>$request->salePrice[$i],
				'quantity'=>$request->quantity[$i],
			];
			purchaseproduct::create($purchaseproduct);
		}
		Session::flash('message', 'compra creada correctamente');
		return Redirect('/Purchases');   
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$Purchase = Purchase::find($id);
		$products = Product::select(DB::raw('CONCAT(name,"-",reference, "-", color)  AS NP'), 'id')->lists('NP', 'id');
		$purchaseproducts = Purchaseproduct::where('purchaseproducts.purchase_id', '=', $id)->get();
        return view('Purchase.edit', ['purchase'=>$Purchase], compact('products', 'purchaseproducts'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$Purchase = Purchase::find($id);
        $Purchase->fill($request->all());
        $Purchase->save();


        Purchaseproduct::where('purchaseproducts.purchase_id', $id)->delete();
        for ($i=0; $i < count($request->product_id); $i++)
		{ 
			$purchaseproduct = 
			[
				'purchase_id'=>$Purchase->id,
				'product_id' =>$request->product_id[$i],
				'purchasePrice'=>$request->purchasePrice[$i],
				'salePrice'=>$request->salePrice[$i],
				'quantity'=>$request->quantity[$i],
			];
			Purchaseproduct::create($purchaseproduct);
		}


        Session::flash('message', 'compra modificada correctamente');
        return Redirect::to('/Purchases'); 
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		Purchaseproduct::where('purchaseproducts.purchase_id', $id)->delete();
		Purchase::destroy($id);
        Session::flash('message', 'compra eliminada correctamente');
        return Redirect::to('/Purchases'); 
	}
}
