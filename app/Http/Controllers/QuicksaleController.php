<?php

namespace Optitec\Http\Controllers;

use Illuminate\Http\Request;

use Optitec\Http\Requests;
use Optitec\Http\Controllers\Controller;
use Optitec\Product;
use Optitec\Payment;
use Optitec\Orderproduct;
use Optitec\Order;
use Optitec\Client;
use DB;

class QuicksaleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('superuser', ['only' => []]);
        $this->middleware('administrador', ['only' => []]);
        $this->middleware('medico');
        $this->middleware('asesor', ['only' => ['index', 'edit', 'update', 'destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $labs = DB::table('labs')->lists('name','id');
        $productsquery = DB::select( DB::raw("SELECT products.id, CONCAT(products.name, '-', products.reference, '-', products.color, '-', purchaseproducts.salePrice)  AS NP
                                    FROM purchaseproducts 
                                    JOIN products
                                    ON purchaseproducts.product_id = products.id
                                    WHERE purchaseproducts.id IN(SELECT MAX(purchaseproducts.id)
                                    FROM purchaseproducts
                                    GROUP BY purchaseproducts.product_id) ORDER BY id ASC"));
        $keys = [];
        $values = [];
        $products = [];
        foreach ($productsquery as $productsquery)
        {
            array_push($keys, $productsquery->id);
            array_push($values, $productsquery->NP);
        }
        $products = array_combine($keys, $values);
        return view('Quicksale.create', compact('labs', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $client = Client::where("identification","=",$request->identification)->first();
        if(count($client)==0)
        {
            Client::create($request->all());
            $client_id = DB::table('clients')->max('id');
            Session::flash('message', 'cliente creado correctamente');
        }
        else
        {
            $client_id = $client->id;
            Session::flash('message', 'Historia creada correctamente');
        }

        $Order = Order::create($request->all());
        $mtmk= DB::table('materialmarkprices')
                    ->join('materiallens', 'materiallens.id', '=','materialmarkprices.materiallen_id')
                    ->join('marklens', 'marklens.id', '=', 'materialmarkprices.marklen_id')
                    ->where('materialmarkprices.id', '=', $request->materialmarkprice_id)
                    ->select('materiallens.name AS NAMEMATERIAL', 'marklens.name AS NAMEMARK')->first();
                    
        $productnamemtmk = ($mtmk->NAMEMATERIAL."-".$mtmk->NAMEMARK);

        $fl= DB::table('filterlens')
                    ->where('filterlens.id', '=', $request->filterlen_id)
                    ->select('filterlens.name AS NAMEFILTER')->first();
        $productfilter = $fl->NAMEFILTER;

        $pr= DB::table('products')
                    ->where('products.id', '=', $request->product_id)
                    ->select('products.name AS NAMEPRODUCT')->first();
        $product = $pr->NAMEPRODUCT;

        $lente=
        [
            'order_id'=>$Order->id,
            'typeproduct_id' => 1,
            'product_id'=>$request->materialmarkprice_id,
            'productname'=>$productnamemtmk,
            'price'=>$request->pricelen,
        ];
        Orderproduct::create($lente);

        $filtro=
        [
            'order_id'=>$Order->id,
            'typeproduct_id' => 2,
            'product_id'=>$request->filterlen_id,
            'productname'=>$fl->NAMEFILTER,
            'price'=>$request->pricefilter,
        ];
        Orderproduct::create($filtro);

        $montura=
        [
            'order_id'=>$Order->id,
            'typeproduct_id' => 3,
            'product_id'=>$request->product_id,
            'productname'=>$product,
            'price'=>$request->priceproduct,
        ];
        Orderproduct::create($montura);

        $pago=
        [
            'order_id'=>$Order->id,
            'price'=>$request->paymentprice,
        ];
        Payment::create($pago);

        $payment_id = DB::table('payments')->max('id');
        return Response::Json(['order_id'=> $Order->id, 'payment_id'=> $payment_id]);


        $Invoice = Invoice::create($request->all());
        return Response::Json(['invoice_id'=> $Invoice->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $labs = DB::table('labs')->lists('name','id');
        $productsquery = DB::select( DB::raw("SELECT products.id, CONCAT(products.name, '-', products.reference, '-', products.color, '-', purchaseproducts.salePrice)  AS NP
                                    FROM purchaseproducts 
                                    JOIN products
                                    ON purchaseproducts.product_id = products.id
                                    WHERE purchaseproducts.id IN(SELECT MAX(purchaseproducts.id)
                                    FROM purchaseproducts
                                    GROUP BY purchaseproducts.product_id) ORDER BY id ASC"));
        $keys = [];
        $values = [];
        $products = [];
        foreach ($productsquery as $productsquery)
        {
            array_push($keys, $productsquery->id);
            array_push($values, $productsquery->NP);
        }
        $products = array_combine($keys, $values);
        $Orderproducts = Orderproduct::where('orderproducts.order_id', '=', $id)->get();
        $Payments = Payment::where('payments.order_id', '=', $id)->get();
        $Order = Order::leftjoin('invoices', 'invoices.order_id', '=', 'orders.id')
                    ->where('orders.id', '=', $id)
                    ->select('orders.*', 'invoices.order_id')
                    ->first();
        $Client = Client::where('id', '=', $Order->client_id)->first();
        return view('Quicksale.show', compact('labs', 'products', 'Orderproducts', 'Payments', 'Order', 'Client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
