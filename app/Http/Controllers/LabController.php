<?php

namespace Optitec\Http\Controllers;

use Illuminate\Http\Request;
use Optitec\Http\Requests;
use Optitec\Http\Controllers\Controller;
use Optitec\Http\Requests\LabRequest;
use Optitec\Lab;
use Optitec\Marklen;
use Optitec\Typelen;
use Optitec\Materiallen;
use Optitec\Filterlen;
use Optitec\materialmarkprice;
use Validator;
use Response;
use Session;
use Redirect;
use DB;

class LabController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('superuser', ['only' => []]);
        $this->middleware('administrador', ['only' => []]);
        $this->middleware('medico', ['only' => ['create', 'store', 'edit', 'update', 'destroy', 'show']]);
        $this->middleware('asesor', ['only' => ['create', 'store', 'edit', 'update', 'destroy', 'show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $labs = Lab::paginate(15);
        return view('Lab.index', compact('labs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('Lab.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'contactName' => 'required',
            'contactPhone' => 'required',
        ]);
        $arr = array(
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'contactName' => $request->contactName,
            'contactPhone' => $request->contactPhone
            );

        if ($validator->fails()) 
        {
            return redirect('Labs/create')
                        ->withErrors($validator)
                        ->withInput($arr);
        }
        $Lab = Lab::create($request->all());
        $lab_id = $Lab->id;
        $tipos = count($request->nametypelens);
        $filtros = count($request->filter);
        for ($i=0; $i < $tipos; $i++) 
        {
            $Tipo=
            [
                'name'=>$request->nametypelens[$i],
                'lab_id'=>$lab_id,
            ];
            $Typelen = Typelen::create($Tipo);
            $type_id = $Typelen->id;
            $materiales = $request->material[$i];
            $precios = $request->precio[$i];
            $arrayIDsMateriales = [];

            for ($k=0; $k < count($materiales); $k++)
            { 
                $material = 
                [
                    'name'=>$materiales[$k],
                    'typelen_id'=>$type_id,
                    'lab_id' => $lab_id,
                    'price'=>$precios[$k],
                ];
                $Material = Materiallen::create($material);
            }      
        }

        for ($j=0; $j <$filtros ; $j++)
        { 
            $filter =
            [
                'name' => $request->filter[$j],
                'lab_id' => $lab_id,
                'price' =>$request->price[$j],
            ];                  
            Filterlen::create($filter);
        }
        return Redirect::to('/Labs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {  
        $Lab = Lab::find($id);
        $tipos = DB::select( DB::raw("SELECT typelens.id AS IDTYPE, typelens.name AS NAMETYPE
            FROM labs
            JOIN typelens
            ON labs.id = typelens.lab_id
            WHERE labs.id = $id and typelens.deleted_at is NULL"));
        $materiales = DB::select( DB::raw("SELECT typelens.id AS IDTYPE, typelens.name AS NAMETYPE,  materiallens.id AS IDMATERIAL, materiallens.name AS NAMEMATERIAL, materiallens.price AS PRICEMATERIAL
            FROM labs
            JOIN typelens
            ON labs.id = typelens.lab_id
            JOIN materiallens
            ON typelens.id = materiallens.typelen_id
            WHERE labs.id = $id and materiallens.deleted_at is NULL"));
        $filtros = FilterLen::where('lab_id','=',$id)->orderBy('id')->get();
        return view('Lab.edit', ['lab'=>$Lab], compact('tipos', 'materiales','precios', 'filtros'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Lab = Lab::find($id);
        $Lab->fill($request->all());
        $Lab->save();
        Typelen::where('lab_id', $id)->delete();
        Filterlen::where('lab_id', $id)->delete();
        $tipos = count($request->nametypelens);
        $filtros = count($request->filter);
        for ($i=0; $i < $tipos; $i++) 
        {
            $Tipo=
            [
                'name'=>$request->nametypelens[$i],
                'lab_id'=>$id,
            ];
            $Typelen = Typelen::create($Tipo);
            $type_id = $Typelen->id;
            $materiales = $request->material[$i];
            $precios = $request->precio[$i];
            $arrayIDsMateriales = [];

            for ($k=0; $k < count($materiales); $k++)
            { 
                $material = 
                [
                    'name'=>$materiales[$k],
                    'typelen_id'=>$type_id,
                    'lab_id' => $id,
                    'price'=>$precios[$k],
                ];
                $Material = Materiallen::create($material);
            }      
        }

        for ($j=0; $j <$filtros ; $j++)
        { 
            $filter =
            [
                'name' => $request->filter[$j],
                'lab_id' => $id,
                'price' =>$request->price[$j],
            ];                  
            Filterlen::create($filter);
        }
        Session::flash('message', 'laboratorio modificado correctamente');
        return Redirect::to('/Labs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        Lab::destroy($id);
        Session::flash('message', 'laboratorio eliminado correctamente');
        return Redirect::to('/Labs'); 
    }
}
