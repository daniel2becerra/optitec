<?php

namespace Optitec\Http\Controllers;

use Illuminate\Http\Request;

use Optitec\Http\Requests;
use Optitec\Http\Controllers\Controller;
use Optitec\Medicalhistory;
use DB;
use Response;
use Input;

class MedicalhistoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('superuser', ['only' => []]);
        $this->middleware('administrador', ['only' => []]);
        $this->middleware('medico', ['only' => ['create', 'store', 'edit', 'update', 'destroy', 'show']]);
        $this->middleware('asesor', ['only' => ['edit', 'update', 'destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function jsonMedicalHistory()
    {
        $client_id = Input::get('client_id');
        $medicalHistory = Medicalhistory::where('client_id', $client_id)->OrderBy('created_at', 'des')->get();

        return Response::json($medicalHistory);
    }

    public function jsonMedicalHistoryDate()
    {
        $fechainicial = Input::get('FechaInicial');
        $fechafinal = Input::get('FechaFinal');
        $medicalHistory = DB::table('medicalhistories')
                            ->join('clients', 'clients.id', '=', 'medicalhistories.client_id')
                            ->where('medicalhistories.created_at', '>=', $fechainicial)
                            ->where('medicalhistories.created_at', '<', $fechafinal)
                            ->select('clients.identification', 'clients.firstname', 'clients.lastname', 'medicalhistories.created_at')
                            ->OrderBy('created_at', 'des')->get();
        return Response::json($medicalHistory);
    }

    public function Medicalhistory()
    {
        return view('Reports.Medicalhistory');
    }

    
}
