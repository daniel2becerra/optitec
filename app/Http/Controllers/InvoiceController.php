<?php

namespace Optitec\Http\Controllers;

use Illuminate\Http\Request;
use Optitec\Http\Requests;
use Optitec\Http\Controllers\Controller;
use Optitec\Invoice;
use Optitec\Client;
use Optitec\Order;
use Response;
use DB;
use Input;
use PDF;
use Excel;


class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Invoice = Invoice::where('invoices.order_id', '=', $request->order_id)->first();
        if($Invoice)
        {
            return Response::Json(['invoice_id'=> $Invoice->id]);
        }
        else
        {
            if($request->identification)
            {
                $Client = Client::where('clients.identification', '=', $request->identification)->first();
                if($Client)
                {
                    Order::where('id', $request->order_id)
                      ->update(['client_id' => $Client->id]);
                }
                else
                {
                    $Client = Client::create($request->all());
                    Order::where('id', $request->order_id)
                      ->update(['client_id' => $Client->id]);
                }
            }
            $Invoice = Invoice::create($request->all());
            return Response::Json(['invoice_id'=> $Invoice->id]);
            
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
