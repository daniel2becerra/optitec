<?php

namespace Optitec\Http\Controllers;

use Illuminate\Http\Request;

use Optitec\Http\Requests;
use Optitec\Http\Requests\SettlementRequest;
use Optitec\Http\Controllers\Controller;
use Optitec\Settlement;
use Optitec\Payment;
use Session;
use Redirect;
use DB;
use Auth;

class SettlementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settlements = Settlement::where('settlements.user_id', '=', Auth::User()->id)->paginate(15);
        return view('Settlement.index', compact('settlements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $payments = Payment::where('status_id', '=' , 0)
                    ->where('settlement_id', '=', 0)
                    ->where('user_id', '=', Auth::User()->id)->get();
        $Settlement = DB::table('settlements')
    					->where('settlements.user_id', '=', Auth::User()->id)
                    	->addSelect(DB::raw('COALESCE(SUM(settlements.entrega),0) AS TOTALENTREGAS'))
                    	->addSelect(DB::raw('COALESCE(SUM(settlements.recibe),0) AS TOTALRECIBE'))
                    	->addSelect(DB::raw('COALESCE(SUM(settlements.gastos),0) AS TOTALGASTOS'))
                    	->groupBy('settlements.user_id')->get();

        $paymentsSet = DB::table('payments')
    					->where('payments.status_id', '=', 1)
    					->where('payments.settlement_id', '<>', 0)
    					->where('payments.user_id', '=', Auth::User()->id)
                    	->addSelect(DB::raw('COALESCE(SUM(payments.price),0) AS TOTALABONOS'))
                    	->groupBy('payments.user_id')->get();
                    	
        if(count($Settlement) == 0)
        {
        	$TOTALENTREGAS = 0;
        	$TOTALRECIBE = 0;
        	$TOTALGASTOS = 0;
        }
        else
        {
        	$TOTALENTREGAS = $Settlement[0]->TOTALENTREGAS;
        	$TOTALRECIBE = $Settlement[0]->TOTALRECIBE;
        	$TOTALGASTOS = $Settlement[0]->TOTALGASTOS;
        }
        if(count($paymentsSet) == 0)
        {
        	$TOTALABONOS = 0;
        }
        else
        {
        	$TOTALABONOS = $paymentsSet[0]->TOTALABONOS;
        }
    	$pendiente = $TOTALENTREGAS - $TOTALRECIBE + $TOTALGASTOS - $TOTALABONOS;
        return view('Settlement.create', compact('payments', 'pendiente'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SettlementRequest $request)
    {
        $settlement = Settlement::create($request->all()+ ['user_id' => Auth::User()->id]);
        Payment::where('status_id', 0)
          ->where('settlement_id', 0)
          ->where('user_id', Auth::User()->id)
          ->update(['status_id' => 1, 'settlement_id' => $settlement->id]);
        Session::flash('message', 'Arqueo creado correctamente');
        return Redirect('/Settlements');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::User()->role_id == 1)
        {
            Settlement::where('id', $id)
            ->update(['revisado' => 1]);
        }

        $SettlementOld = DB::table('settlements')
                        ->where('settlements.user_id', '=', Auth::User()->id)
                        ->where('settlements.id', '<', $id)
                        ->addSelect(DB::raw('COALESCE(SUM(settlements.entrega),0) AS TOTALENTREGAS'))
                        ->addSelect(DB::raw('COALESCE(SUM(settlements.recibe),0) AS TOTALRECIBE'))
                        ->addSelect(DB::raw('COALESCE(SUM(settlements.gastos),0) AS TOTALGASTOS'))
                        ->groupBy('settlements.user_id')->get();


        $paymentsSet = DB::table('payments')
                        ->where('payments.status_id', '=', 1)
                        ->where('payments.settlement_id', '<>', 0)
                        ->where('payments.settlement_id', '<', $id)
                        ->where('payments.user_id', '=', Auth::User()->id)
                        ->addSelect(DB::raw('COALESCE(SUM(payments.price),0) AS TOTALABONOS'))
                        ->groupBy('payments.user_id')->get();
                        
        if(count($SettlementOld) == 0)
        {
            $TOTALENTREGAS = 0;
            $TOTALRECIBE = 0;
            $TOTALGASTOS = 0;
        }
        else
        {
            $TOTALENTREGAS = $SettlementOld[0]->TOTALENTREGAS;
            $TOTALRECIBE = $SettlementOld[0]->TOTALRECIBE;
            $TOTALGASTOS = $SettlementOld[0]->TOTALGASTOS;
        }
        if(count($paymentsSet) == 0)
        {
            $TOTALABONOS = 0;
        }
        else
        {
            $TOTALABONOS = $paymentsSet[0]->TOTALABONOS;
        }
        $pendiente = $TOTALENTREGAS - $TOTALRECIBE + $TOTALGASTOS - $TOTALABONOS;

        $Settlement = Settlement::find($id);
        $payments = Payment::where('settlement_id', '=', $id)->get();
        return view('Settlement.show', ['settlement'=>$Settlement], compact('payments', 'pendiente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Settlement::where('id', $id)
          ->update(['pendiente' => $request->pendiente]);
        return Redirect('/Settlements');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
