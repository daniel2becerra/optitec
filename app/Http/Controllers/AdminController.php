<?php

namespace Optitec\Http\Controllers;

use Illuminate\Http\Request;

use Optitec\Http\Requests;
use Optitec\Client;
use Optitec\Appointment;
use Carbon\Carbon;
use PDF;
use Optitec\Http\Controllers\Controller;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pdf()
    {
        $pdf = PDF::loadView('Facturas.recomendaciones')->setPaper('a3', 'landscape');
        return $pdf->stream('archivo.pdf');
    }

    public function index()
    {
        $Today = Carbon::now()->toDateString();
        $Tomorrow = Carbon::now()->addDays(1)->toDateString();
        $S = Carbon::now()->startOfWeek();
        $E = Carbon::now()->endOfWeek();
        $NW = Carbon::now()->weekOfYear+1;
        $TY = Carbon::now()->year;
        $SNW = Carbon::now()->setISODate($TY, $NW)->startOfWeek();
        $ENW = Carbon::now()->setISODate($TY, $NW)->endOfWeek();
        $Clients = Client::count();
        $TotalAppointment = Appointment::where('start', '<=', $Today)->count();
        $AppointmentToday = Appointment::where('start', '>', $Today)->where('start', '<', $Tomorrow)->count();        
        $AppointmentWeek = Appointment::where('start', '>=', $S)->where('start', '<=', $E)->count();
        $AppointmentNextWeek = Appointment::where('start', '>=', $SNW)->where('start', '<=', $ENW)->count();
        return view('Admin.index', compact('Clients', 'TotalAppointment', 'AppointmentToday', 'AppointmentWeek', 'AppointmentNextWeek'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
