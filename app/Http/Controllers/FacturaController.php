<?php

namespace Optitec\Http\Controllers;

use Illuminate\Http\Request;

use Optitec\Http\Requests;
use PDF;
use Excel;
use Input;
use DB;
use Optitec\Medicalhistory;
use Optitec\Order;
use Optitec\Payment;
use Optitec\Client;
use Optitec\Lab;
use Optitec\TypeLen;
use Optitec\Materiallen;
use Optitec\Marklen;
use Optitec\Filterlen;
use Optitec\Http\Controllers\Controller;

class FacturaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function abono()
    {
        $payment_id = Input::get('payment_id');
        $order_id = Input::get('order_id');
        $Order = DB::table('orders')
                ->join('clients', 'clients.id', '=', 'orders.client_id')
                ->where('orders.id', '=', $order_id)
                ->select('orders.*', 'clients.firstname', 'clients.lastname')
                ->first();
        $Payment = Payment::find($payment_id);
        $Payments = DB::select(DB::raw("
        SELECT SUM(payments.price) AS PAYMENTSTOTAL FROM orders
        JOIN payments
        ON payments.order_id = orders.id
        WHERE payments.id <= $payment_id AND orders.id = $order_id"));
        $Products = DB::select(DB::raw("
        SELECT SUM(orderproducts.price) AS PRODUCTSTOTAL FROM orders
        JOIN orderproducts
        ON orderproducts.order_id = orders.id
        WHERE orderproducts.created_at <= '$Payment->created_at' AND orders.id = $order_id"));
        $pdf = PDF::loadView('Facturas.abono', compact('Payment', 'Payments', 'Order', 'Products'))->setPaper('a5', 'landscape');
        return $pdf->stream('abono.pdf');
    }

    public function facturaventa()
    {
        $invoice_id = Input::get('id');
        $products = DB::table('invoices')
                        ->where('invoices.id', $invoice_id)
                        ->where('orderproducts.deleted_at', null)
                        ->join('orders', 'invoices.order_id', '=', 'orders.id')
                        ->join('orderproducts', 'orders.id', '=', 'orderproducts.order_id')
                        ->join('clients', 'orders.client_id', '=', 'clients.id')
                        ->select('invoices.id AS IDINVOICE', 'invoices.created_at AS DATEINVOICE', 'orderproducts.*', 'clients.identification', 'clients.firstname', 'clients.lastname', 'clients.phone')->get();
        $pdf = PDF::loadView('Facturas.facturaVenta', compact('products'))->setPaper('a3', 'landscape');
        return $pdf->stream('facturaVenta.pdf'); 
    }

    public function facturaventaold()
    {
        $history_id = Input::get('id');
        $pdf = PDF::loadView('Facturas.facturaventaold', compact('history_id'))->setPaper('a3', 'landscape');
        return $pdf->stream('facturaventaold.pdf'); 
    }

    public function historiaclinica()
    {
        $history_id = Input::get('id');
        $pdf = PDF::loadView('Facturas.historiaclinica', compact('history_id'))->setPaper('a3', 'landscape');
        return $pdf->stream('historiaclinica.pdf');
    }

    public function lentes()
    {
        $history_id = Input::get('id');
        $History = Medicalhistory::find($history_id);
        $Client = CLient::find($History->client_id);
        $pdf = PDF::loadView('Facturas.lentes', compact('History', 'Client'))->setPaper('a4', 'landscape');
        return $pdf->stream('lentes.pdf');
    }

    public function pedirlentes()
    {
        $order_id = Input::get('order_id');
        $Order = DB::table('orders')
                ->join('labs', 'labs.id', '=', 'orders.lab_id')
                ->join('typelens', 'typelens.id', '=', 'orders.typelen_id')
                ->join('materiallens', 'materiallens.id', '=', 'orders.materiallen_id')
                ->join('filterlens', 'filterlens.id', '=', 'orders.filterlen_id')
                ->join('orderproducts', 'orderproducts.order_id', '=', 'orders.id')
                ->join('products', 'products.id', '=', 'orderproducts.product_id')
                ->where('orders.id', '=', $order_id)
                ->select('orders.*', 'labs.name AS NAMELAB', 'typelens.name AS NAMETYPE', 'materiallens.name AS NAMEMATERIAL', 'filterlens.name AS NAMEFILTER', 'products.name AS NAMEPRODUCT')
                ->get();
        $History = Medicalhistory::find($Order[0]->medicalhistory_id);
        $pdf = PDF::loadView('Facturas.pedirLentes', compact('Order', 'History'))->setPaper('a3', 'landscape');
        return $pdf->stream('pedirlentes.pdf');
    }

    public function recomendaciones()
    {
        $history_id = Input::get('id');
        $History = Medicalhistory::find($history_id);
        $Client = CLient::find($History->client_id);
        $pdf = PDF::loadView('Facturas.recomendaciones', compact('History', 'Client'))->setPaper('a3', 'landscape');
        return $pdf->stream('recomendaciones.pdf');
    }
}
