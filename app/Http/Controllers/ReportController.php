<?php

namespace Optitec\Http\Controllers;

use Illuminate\Http\Request;

use Optitec\Http\Requests;
use Optitec\Http\Controllers\Controller;
use DB;
use Input;
use Response;
use Carbon\Carbon;

class ReportController extends Controller
{
	public function jsonFacturaIdentification()
	{
		$identification = Input::get('identification');
        $Facturas = DB::table('invoices')
		->join('orders', 'invoices.order_id', '=', 'orders.id')
		->join('clients', 'clients.id', '=', 'orders.client_id')
		->where('clients.identification', '=', $identification)
		->select('invoices.id', 'invoices.created_at', 'clients.identification', 'clients.firstname', 'clients.lastname')
		->get();

        return Response::json($Facturas);
	}

	public function jsonFacturaName()
	{
		$name = Input::get('name');
        if($name != "" || $name != null)
        {
	        $Facturas = DB::table('invoices')
            ->join('orders', 'invoices.order_id', '=', 'orders.id')
            ->join('clients', 'clients.id', '=', 'orders.client_id')
            ->where('clients.firstname', 'like', '%'.$name.'%')
            ->Orwhere('clients.lastname', 'like', '%'.$name.'%')
            ->select('invoices.id', 'invoices.created_at', 'clients.identification', 'clients.firstname', 'clients.lastname')
            ->get();

            return Response::json($Facturas);
	    }

        
	}

	public function jsonFacturaNumber()
	{
		$number = Input::get('number');
		$Facturas = DB::table('invoices')
        ->join('orders', 'invoices.order_id', '=', 'orders.id')
        ->join('clients', 'clients.id', '=', 'orders.client_id')
        ->where('invoices.id', '=', $number)
        ->select('invoices.id', 'invoices.created_at', 'clients.identification', 'clients.firstname', 'clients.lastname')
        ->get();

        return Response::json($Facturas);
	}

	public function jsonFacturaDate()
	{
		$FI = Input::get('FI');
        $FF = Carbon::parse(Input::get('FF'))->addDay(1);
		$Facturas = DB::table('invoices')
        ->join('orders', 'invoices.order_id', '=', 'orders.id')
        ->join('clients', 'clients.id', '=', 'orders.client_id')
        ->where('invoices.created_at', '>', $FI)
        ->where('invoices.created_at', '<=', $FF)
        ->select('invoices.id', 'invoices.created_at', 'clients.identification', 'clients.firstname', 'clients.lastname')
        ->get();

        return Response::json($Facturas);
	}

    public function factura()
    {
        return view('Reports.Factura');
    }
}
