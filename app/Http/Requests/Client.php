<?php

namespace Optitec\Http\Requests;

use Optitec\Http\Requests\Request;

class Client extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'identificacion' => 'required|numeric|digits_between:7,12|unique:users',
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'email|unique:users',
            'phone' => 'required',
            'birtday' => 'required',
            'profession' => 'required',
        ];
    }
}
