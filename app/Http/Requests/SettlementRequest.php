<?php

namespace Optitec\Http\Requests;

use Optitec\Http\Requests\Request;

class SettlementRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'entrega' => 'required',
            'recibe' => 'required',
            'gastos' => 'required',
            'notas' => 'required',
        ];
    }
}
