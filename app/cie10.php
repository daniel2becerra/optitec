<?php

namespace Optitec;

use Illuminate\Database\Eloquent\Model;

class cie10 extends Model
{
    protected $table = 'cie10s';
	protected $fillable = ['id10', 'dec10', 'grp10'];
	protected $dates = ['deleted_at'];
}
