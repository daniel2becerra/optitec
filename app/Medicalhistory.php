<?php

namespace Optitec;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Medicalhistory extends Model
{
	use SoftDeletes;
    protected $table = 'medicalhistories';
	protected $fillable = ['client_id', 'reasonconsultation', 'odwithoutcorrection', 'oiwithoutcorrection', 'aowithoutcorrection', 'odRx', 'oiRx', 'addRx', 'typeLen', 'odEE', 'oiEE', 'covertest', 'ppm', 'ductions', 'versions', 'odfondo', 'oifondo', 'odQ', 'oiQ', 'odR', 'oiR', 'odS', 'oiS', 'prescription', 'OdEsferaRxF', 'OdCilindroRxF', 'OdEjeRxF', 'OdAvRxF', 'OdAddRxF', 'OiEsferaRxF', 'OiCilindroRxF', 'OiEjeRxF', 'OiAvRxF', 'OiAddRxF','DpRxF', 'UsoRxF', 'observationRxF'];
	protected $dates = ['deleted_at'];
}
