<?php

namespace Optitec;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Typelen extends Model
{
    use SoftDeletes;


    protected $table = 'typelens';
	protected $fillable = ['name', 'lab_id'];
    protected $dates = ['deleted_at'];

	public function materiallens()
    {
        return $this->hasMany('Optitec\Materiallen');
    }

    public function marklens()
    {
        return $this->hasMany('Optitec\Marklen');
    }

    public function lab()
    {
        return $this->belongsTo('Optitec\Lab');
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function($typelen)
        {   
            
            $typelen->marklens->each(
                function($marklens)
                {
                    $marklens->delete();
                });
            $typelen->materiallens->each(
                function($materiallens)
                {
                    $materiallens->delete();
                });
        });
    }
}
