<?php

namespace Optitec;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Optitec\Http\Requests\ProductRequest;

class Product extends Model
{
	use SoftDeletes;
    
    protected $table = 'products';
	protected $fillable = ['name', 'reference', 'color'];
	protected $dates = ['deleted_at'];

	public function purchaseproducts()
    {
        return $this->hasMany('Optitec\Purchaseproduct');
    }
}
