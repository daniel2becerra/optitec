<?php

namespace Optitec;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    protected $table = 'orders';
	protected $fillable = ['typeorder_id', 'client_id', 'medicalhistory_id', 'lab_id', 'materiallen_id', 'marklen_id', 'typelen_id', 'filterlen_id', 'alt', 'observation'];
    protected $dates = ['deleted_at'];
}
