@extends('layouts.admin')
  @section('content')
    @include('alerts.AlertsRequest')
    @include('alerts.SuccessRequest')
    @include('alerts.ErrorsRequest')
<!doctype html>
<html lang="en">
<head>
  <link href="../assets/css/paper-bootstrap-wizard.css" rel="stylesheet" />
  <!-- Fonts and Icons -->
  <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
  <link href="../assets/css/themify-icons.css" rel="stylesheet">
</head>
<body>
  <div>
    <!--   Big container   -->
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
            <!--      Wizard container        -->
          <div class="wizard-container">
            <div class="card wizard-card" data-color="blue" id="wizard">
              {!!Form::open(['route'=>'Clients.store', 'method'=>'POST', 'autocomplete'=>'off', 'class'=>'form-horizontal form-label-left', 'id'=>'frm'])!!}
              <!--        You can switch " data-color="green" "  with one of the next bright colors: "blue", "azure", "orange", "red"       -->
                <div class="wizard-header">
                    <h3 class="wizard-title">Creación de pacientes</h3>
                    <p class="category">Debe ingresar todos los campos requeridos</p>
                </div>
                @include('Forms.Clients')
              {!!Form::close()!!}
            </div>
          </div> <!-- wizard container -->          
        </div>
      </div> <!-- row -->
      <div class="col-sm-12" hidden="true" id="history">
            <!--      Wizard container        -->
        <div class="wizard-container">
          <div class="card wizard-card" data-color="blue">
            <div class="wizard-header">
              <h3 class="wizard-title">Historia clinica del paciente</h3>
              <p class="category">historias</p>
            </div>            
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">          
            </div>
          </div>
        </div> <!-- wizard container -->          
        </div>
      </div> <!-- row -->
    </div> <!--  big container -->
  </div>
  </body>

  <!--   Core JS Files   -->
  <script src="../assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>

  <!--  Plugin for the Wizard -->
  <script src="../assets/js/paper-bootstrap-wizard.js" type="text/javascript"></script>

  <!--  More information about jquery.validate here: http://jqueryvalidation.org/  -->
  <script src="../assets/js/jquery.validate.min.js" type="text/javascript"></script>

</html>

@stop
