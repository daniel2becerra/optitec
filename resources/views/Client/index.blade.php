@extends('layouts.admin')
@section('content')	
@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Listado de clientes </h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
    			</ul>
    			<div class="clearfix"></div>
      		</div>
    		<div class="x_content">
			<table id="datatable" class="table table-hover">
				<thead>
					<th>Nombres</th>
					<th>Apellidos</th>
					<th>Correo</th>
					<th>Teléfono</th>
					<th>Profesión</th>
					<th>Operación</th>
				</thead>
				@foreach($clients as $client)
				<tbody>
					<td>{{$client->firstname}}</td>
					<td>{{$client->lastname}}</td>
					<td>{{$client->email}}</td>
					<td>{{$client->phone}}</td>
					<td>{{$client->profession}}</td>
					<td>
						{!!link_to_route('Clients.edit', $title = 'Editar', $parameters =$client->id, $attributes = ['class'=>'btn btn-primary'])!!}
					</td>
				</tbody>
				@endforeach		
			</table>
			{!! $clients->render() !!}
			</div>
		</div>
	</div>
</div>
@stop