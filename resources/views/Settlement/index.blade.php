@extends('layouts.admin')
@section('content')	
@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Listado de Arqueos </h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
    			</ul>
    			<div class="clearfix"></div>
      		</div>
    		<div class="x_content">
			<table id="datatable" class="table table-hover">
				<thead>
					<th>Fecha</th>
					<th>Entrega</th>
					<th>Recibe</th>
					<th>Gastos</th>
					<th>Visto</th>
					<th>Pendiente</th>
					<th>Operación</th>
				</thead>
				<tbody>
					@foreach($settlements as $settlement)
					<tr>
						<?php $visto = $settlement->revisado == 0 ? "NO" : "SI"; 
						$pendiente = $settlement->pendiente == 0 ? "NO" : "SI"; ?>
						<td>{{$settlement->created_at}}</td>
						<td>{{$settlement->entrega}}</td>
						<td>{{$settlement->recibe}}</td>
						<td>{{$settlement->gastos}}</td>
						<td>{{$visto}}</td>
						<td>{{$pendiente}}</td>
						<td>
							{!!link_to_route('Settlements.show', $title = 'Ver', $parameters =$settlement->id, $attributes = ['class'=>'btn btn-primary'])!!}
						</td>
					</tr>					
					@endforeach	
				</tbody>	
				{!! $settlements->render() !!}				
			</table>
			</div>
		</div>
	</div>
</div>
@stop