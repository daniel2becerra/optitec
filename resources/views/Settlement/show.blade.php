@extends('layouts.admin')
@section('content')	
@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Listado de Arqueos </h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
    			</ul>
    			<div class="clearfix"></div>
      		</div>
    		<div class="x_content">
			<table id="datatable" class="table table-hover">
				<thead>
					<th>N° Abono</th>
					<th>Fecha</th>
					<th>Valor</th>
				</thead>
				<tbody>
					<?php $total = 0; ?>
					@foreach($payments as $payment)
					<tr>
						<td>{{$payment->id}}</td>
						<td>{{$payment->created_at}}</td>
						<td>{{$payment->price}}</td>
					</tr>
					<?php $total +=  $payment->price?>					
					@endforeach	
					<tr>
						<td colspan="2" style="text-align: right;">TOTAL: </td>
						<td><b>{{$total}}</b></td>
					</tr>
				</tbody>				
			</table>
			<div class="row">
				  <div class="col-sm-3 col-sm-offset-1">                            
				    <div class="form-group">
				      {!!Form::label('Pendiente *: ', null, ['for'=>'pendiente'])!!}
				      {!!Form::text('pendiente',$pendiente, ['id'=>'pendiente', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'pendiente ', 'disabled'])!!}
				    </div>
				  </div>
				  <div class="col-sm-3">                            
				    <div class="form-group">
				      {!!Form::label('Por liquidar *: ', null, ['for'=>'porLiquidar'])!!}
				      {!!Form::text('porLiquidar',$total, ['id'=>'porLiquidar', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'porLiquidar ', 'disabled'])!!}
				    </div>
				  </div>
				  <div class="col-sm-3">                            
				    <div class="form-group">
				      {!!Form::label('Entrega *: ', null, ['for'=>'entrega'])!!}
				      {!!Form::number('entrega',$settlement->entrega, ['id'=>'entrega', 'class'=>'saldoFinal form-control col-md-7 col-xs-12', 'placeholder'=>'Entrega ', "onkeypress"=>"return isNumberKey(event)", 'disabled'])!!}
				    </div>
				  </div>
				</div>
				<div class="row">
				  <div class="col-sm-3 col-sm-offset-1">                            
				    <div class="form-group">
				      {!!Form::label('Recibe *: ', null, ['for'=>'recibe'])!!}
				      {!!Form::number('recibe',$settlement->recibe, ['id'=>'recibe', 'class'=>'saldoFinal form-control col-md-7 col-xs-12', 'placeholder'=>'Recibe ', "onkeypress"=>"return isNumberKey(event)", 'disabled'])!!}
				    </div>
				  </div>
				  <div class="col-sm-3">                            
				    <div class="form-group">
				      {!!Form::label('Gastos*: ', null, ['for'=>'gastos'])!!}
				      {!!Form::number('gastos',$settlement->gastos, ['id'=>'gastos', 'class'=>'saldoFinal form-control col-md-7 col-xs-12', 'placeholder'=>'Gastos ', "onkeypress"=>"return isNumberKey(event)", 'disabled'])!!}
				    </div>
				  </div>
				  <div class="col-sm-3">                            
				    <div class="form-group">
				      {!!Form::label('Saldo final *: ', null, ['for'=>'saldoFinal'])!!}
				      {!!Form::text('saldoFinal', 0, ['id'=>'saldoFinal', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'saldoFinal ', 'disabled'])!!}
				    </div>
				  </div>
				</div>
				<div class="row">
				  <div class="col-sm-9 col-sm-offset-1">                            
				    <div class="form-group">
				      {!!Form::label('Notas *: ', null, ['for'=>'notas'])!!}
				      {!!Form::textarea('notas',$settlement->notas, ['id'=>'notas', 'class'=>'form-control col-md-7 col-xs-12', 'rows'=>'3', 'placeholder'=>'notas', 'disabled'])!!}
				    </div>
				  </div>         
				</div>

				<div class="ln_solid"></div>
			@if(Auth::User()->role_id == 1 || Auth::User()->role_id == 2)
			{!!Form::model(null, ['route'=>['Settlements.update',$settlement->id],  'method'=>'PUT'])!!}
			<div class="form-group">
				<div class="dropdown">
					{!!form::select('pendiente', ['0'=>'No revisar', '1'=>'Revisar'], $settlement->pendiente, ['id'=>'typeproduct', 'class'=> 'btn btn-default'])!!}      
				</div>
			</div>
				{!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!}
			{!!Form::close()!!}
			@endif
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function() {
    var pendiente = $('#pendiente').val();
    var porLiquidar = $('#porLiquidar').val();
    var entrega = $('#entrega').val();
    var recibe = $('#recibe').val();
    var gastos = $('#gastos').val();
    if(entrega == null || entrega == '')
    {
      entrega = 0;
    }
    if(recibe == null || recibe == '')
    {
      recibe = 0;
    }
    if(gastos == null || gastos == '')
    {
      gastos = 0;
    }
    $('#saldoFinal').val(parseInt(pendiente)-parseInt(porLiquidar)+parseInt(entrega)-parseInt(recibe)+parseInt(gastos));
  });
  function isNumberKey(evt){
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;
      return true;
  }
</script>
@stop