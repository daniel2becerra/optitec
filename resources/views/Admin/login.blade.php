<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    @include('alerts.AlertsRequest')
    @include('alerts.SuccessRequest')
    @include('alerts.ErrorsRequest')
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            {!!Form::open(['route'=>'login.store', 'method'=>'POST', 'class'=>'form-signin', 'autocomplete'=>'off'])!!}
              <h1>Inicio de sesión</h1>
              <div>
                {!!Form::text('user',null, ['class'=>'form-control', 'placeholder'=>'Ingresa tu usuario'])!!}
              </div>
              <div>
                {!!Form::password('password', ['class'=>'form-control', 'placeholder'=>'Ingresa tu contrase&ntilde;a'])!!}
              </div>
              <div>
                <button class="btn btn-default submit" type="submit" name="iniciosesion">Iniciar Sesi&oacute;n</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Tienes problemas?
                  <a href="#signup" class="to_register"> Crea un ticket </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-eye"></i> Optica Visión Control</h1>
                  <p>©2018 Todos los derechos reservados. Optitec, software paa opticas INTERSOFT</p>
                </div>
              </div>
            {!!Form::close()!!} 
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form>
              <h1>Crea un ticket</h1>
              <div>
                <input type="text" class="form-control" placeholder="Nombre" required="" />
              </div>
              <div>
                <input type="email" class="form-control" placeholder="Correo electrónico" required="" />
              </div>
              <div>
                <input type="text" class="form-control" placeholder="Título" required="" />
              </div>
              <div>
                <textarea class="form-control" placeholder='Descripción' rows="3"></textarea>
              </div>
              <div>
                <a class="btn btn-default submit" href="index.html">Enviar</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Listo para ingresar?
                  <a href="#signin" class="to_register"> Inicia sesión </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-eye"></i> Optica Visión Control</h1>
                  <p>©2018 Todos los derechos reservados. Optitec, software paa opticas INTERSOFT</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
