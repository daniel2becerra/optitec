@extends('layouts.admin')
  @section('content')
    @include('alerts.AlertsRequest')
    @include('alerts.SuccessRequest')
    @include('alerts.ErrorsRequest')
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Facturas</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Consulta de facturas <small>Consulta por fechas</small></h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <div class="row">
                <div class="col-md-12">
                  <label class="radio-inline">
                    <input type="radio" name="typesearch" id="searchidentification" value="1"> Busqueda por identificación
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="typesearch" id="searchname" value="2"> Busqueda por nombre
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="typesearch" id="searchdate" value="3"> Busqueda por fecha
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="typesearch" id="searchnumber" value="3"> Busqueda por número
                  </label>
                </div>
              </div>
              <br>
              <div class="row" id="dividentification" hidden="true">
                <div class="col-md-6">
                  <div class="form-group">
                    {!!Form::label('Identificación *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12', 'for'=>'identification'])!!}
                    <div class="col-md-8">
                      {!!Form::text('identification',null, ['id'=>'identification', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Identificación'])!!}
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div>
                      <button class="btn btn-info" id="searchi">Consultar</button>
                  </div>
                </div>
              </div>
              <div class="row" id="divdate" hidden="true">
                <div class="col-md-4">
                  <div class='input-group date' id='FechaInicial'>
                      {!!Form::text('FechaInicial', null, ['id'=>'FI', 'class'=>'form-control', 'placeholder'=>'YYYY-MM-DD'])!!}
                      <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class='input-group date' id='FechaFinal'>
                      {!!Form::text('FechaFinal', null, ['id'=>'FF', 'class'=>'form-control', 'placeholder'=>'YYYY-MM-DD'])!!}
                      <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>
                <div class="col-md-4">
                  <div>
                      <button class="btn btn-info" id="searchd">Consultar</button>
                  </div>
                </div>
              </div>
              <div class="row" id="divname" hidden="true">
                <div class="col-md-6">
                  <div class="form-group">
                    {!!Form::label('Nombre *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12', 'for'=>'name'])!!}
                    <div class="col-md-8">
                      {!!Form::text('name',null, ['id'=>'name', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Nombre'])!!}
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div>
                      <button class="btn btn-info" id="searchna">Consultar</button>
                  </div>
                </div>
              </div>
              <div class="row" id="divnumber" hidden="true">
                <div class="col-md-6">
                  <div class="form-group">
                    {!!Form::label('Número *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12', 'for'=>'number'])!!}
                    <div class="col-md-8">
                      {!!Form::text('number',null, ['id'=>'number', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Número'])!!}
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div>
                      <button class="btn btn-info" id="searchnu">Consultar</button>
                  </div>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>Listado de facturas </h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        </ul>
                        <div class="clearfix"></div>
                        </div>
                      <div class="x_content">
                    <table id="datatable" class="table table-hover">
                      <thead>
                        <th>Identificación</th>
                        <th>Paciente</th>
                        <th>Factura</th>
                        <th>Fecha de factura</th>
                        <th>Operación</th>
                      </thead>
                      <tbody>                        
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>  
    </div>
<script type="text/javascript">
  $('#searchidentification').change (function()
    {
        if($(this).is(':checked')) 
        {
          $('#dividentification').prop('hidden', false);
          $('#divdate').prop('hidden', true);          
          $('#divname').prop('hidden', true);
          $('#divnumber').prop('hidden', true);
        }
    });

  $('#searchdate').change (function()
    {
        if($(this).is(':checked')) 
        {
          $('#divdate').prop('hidden', false);
          $('#dividentification').prop('hidden', true);
          $('#divname').prop('hidden', true);
          $('#divnumber').prop('hidden', true);
        }
    });

  $('#searchname').change (function()
    {
        if($(this).is(':checked')) 
        {
          $('#divname').prop('hidden', false);
          $('#dividentification').prop('hidden', true);
          $('#divdate').prop('hidden', true);
          $('#divnumber').prop('hidden', true);
        }
    });

  $('#searchnumber').change (function()
    {
        if($(this).is(':checked')) 
        {
          $('#divnumber').prop('hidden', false);
          $('#divname').prop('hidden', true);
          $('#dividentification').prop('hidden', true);
          $('#divdate').prop('hidden', true);
        }
    });

  $('#searchi').click(function(e) 
    {
      var identification = $('#identification').val();
      var trs=$("#datatable tr").length;
        for (var i=1; i<trs; i++) 
        {
            $("#datatable tr:last").remove();                               
        }
      $.get('/JsonFacturaIdentification?identification='+identification, function(data)
        {
          $.each(data, function(index, facturaObj)
          {
            $('#datatable > tbody').append('<tr><td>'+facturaObj.identification+'</td><td>'+facturaObj.firstname+' '+facturaObj.lastname+'</td><td>'+facturaObj.id+'</td><td>'+facturaObj.created_at+'</td><td><a class="btn btn-primary" href="/Factura/facturaventa?id='+facturaObj.id+'" target="_blank" role="button"><i class="fa fa-file-pdf-o"></i></a></td></tr>');  
          });
        });
    });

 $('#searchna').click(function(e) 
    {
      var name = $('#name').val();
      var trs=$("#datatable tr").length;
        for (var i=1; i<trs; i++) 
        {
            $("#datatable tr:last").remove();                               
        }
      $.get('/JsonFacturaName?name='+name, function(data)
        {
          $.each(data, function(index, facturaObj)
          {
            $('#datatable > tbody').append('<tr><td>'+facturaObj.identification+'</td><td>'+facturaObj.firstname+' '+facturaObj.lastname+'</td><td>'+facturaObj.id+'</td><td>'+facturaObj.created_at+'</td><td><a class="btn btn-primary" href="/Factura/facturaventa?id='+facturaObj.id+'" target="_blank" role="button"><i class="fa fa-file-pdf-o"></i></a></td></tr>');  
          });
        });
    });

 $('#searchd').click(function(e) 
    {
      var FI = $('#FI').val();
      var FF = $('#FF').val();
      var trs=$("#datatable tr").length;
        for (var i=1; i<trs; i++) 
        {
            $("#datatable tr:last").remove();                               
        }
      $.get('/JsonFacturaDate?FI='+FI+'&FF='+FF, function(data)
        {
          $.each(data, function(index, facturaObj)
          {
            $('#datatable > tbody').append('<tr><td>'+facturaObj.identification+'</td><td>'+facturaObj.firstname+' '+facturaObj.lastname+'</td><td>'+facturaObj.id+'</td><td>'+facturaObj.created_at+'</td><td><a class="btn btn-primary" href="/Factura/facturaventa?id='+facturaObj.id+'" target="_blank" role="button"><i class="fa fa-file-pdf-o"></i></a></td></tr>');     
          });
        });
    });

 $('#searchnu').click(function(e) 
    {
      var number = $('#number').val();
      var trs=$("#datatable tr").length;
        for (var i=1; i<trs; i++) 
        {
            $("#datatable tr:last").remove();                               
        }
      $.get('/JsonFacturaNumber?number='+number, function(data)
        {
          $.each(data, function(index, facturaObj)
          {
            $('#datatable > tbody').append('<tr><td>'+facturaObj.identification+'</td><td>'+facturaObj.firstname+' '+facturaObj.lastname+'</td><td>'+facturaObj.id+'</td><td>'+facturaObj.created_at+'</td><td><a class="btn btn-primary" href="/Factura/facturaventa?id='+facturaObj.id+'" target="_blank" role="button"><i class="fa fa-file-pdf-o"></i></a></td></tr>');     
          });
        });
    });
  $(function ()
  {
    $('#FechaInicial').datetimepicker({
        viewMode: 'years',
      format: 'YYYY-MM-DD',
    });
    $('#FechaFinal').datetimepicker({
        viewMode: 'years',
      format: 'YYYY-MM-DD',
    });
  });
</script>
@stop