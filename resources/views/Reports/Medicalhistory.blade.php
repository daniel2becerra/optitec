@extends('layouts.admin')
  @section('content')
    @include('alerts.AlertsRequest')
    @include('alerts.SuccessRequest')
    @include('alerts.ErrorsRequest')
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Historias médicas</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Consulta de historias médicas <small>Consulta por fechas</small></h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <div class="row">
                <div class="col-md-12">
                  <label class="radio-inline">
                    <input type="radio" name="typesearch" id="searchidentification" value="1"> Busqueda por paciente
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="typesearch" id="searchdate" value="2"> Busqueda por fecha
                  </label>
                </div>
              </div>
              <br>
              <div class="row" id="divdate" hidden="true">
                <div class="col-md-4">
                  <div class='input-group date' id='FechaInicial'>
                      {!!Form::text('FechaInicial', null, ['id'=>'FI', 'class'=>'form-control', 'placeholder'=>'YYYY-MM-DD'])!!}
                      <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class='input-group date' id='FechaFinal'>
                      {!!Form::text('FechaFinal', null, ['id'=>'FF', 'class'=>'form-control', 'placeholder'=>'YYYY-MM-DD'])!!}
                      <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class='input-group date' id='FechaFinal'>
                      <button class="btn btn-info" id="searchd">Consultar</button>
                  </div>
                </div>
              </div>
              <div class="row" id="dividentification" hidden="true">
                <div class="col-md-6">
                  <div class="form-group">
                    {!!Form::label('Identificación *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12', 'for'=>'identification'])!!}
                    <div class="col-md-8">
                      {!!Form::text('identification',null, ['id'=>'identification', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Identificación'])!!}
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class='input-group date' id='FechaFinal'>
                      <button class="btn btn-info" id="searchi">Consultar</button>
                  </div>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>Listado de perfiles </h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        </ul>
                        <div class="clearfix"></div>
                        </div>
                      <div class="x_content">
                    <table id="datatable" class="table table-hover">
                      <thead>
                        <th>Identificación</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Fecha de consulta</th>
                      </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>  
    </div>
<script type="text/javascript">
  $('#searchidentification').change (function()
    {
        if($(this).is(':checked')) 
        {
          $('#divdate').prop('hidden', true);
            $('#dividentification').prop('hidden', false);
        }
    });

  $('#searchdate').change (function()
    {
        if($(this).is(':checked')) 
        {
          $('#divdate').prop('hidden', false);
            $('#dividentification').prop('hidden', true);
        }
    });


  $('#searchd').click(function(e) 
    {
      var FI = $('#FI').val();
      var FF = $('#FF').val();
      var trs=$("#datatable tr").length;
        for (var i=1; i<trs; i++) 
        {
            $("#datatable tr:last").remove();                               
        }
      $.get('/JsonMedicalHistoryDate?FechaInicial='+FI+'&FechaFinal='+FF, function(data)
        {
          console.log(data);
          $.each(data, function(index, mhObj)
          {
            $('#datatable > tbody').append('<tr><td>'+mhObj.identification+'</td><td>'+mhObj.firstname+'</td><td>'+mhObj.lastname+'</td><td>'+mhObj.created_at+'</td></tr>');  
          });
        });
    });
  $(function ()
  {
    $('#FechaInicial').datetimepicker({
        viewMode: 'years',
      format: 'YYYY-MM-DD',
    });
    $('#FechaFinal').datetimepicker({
        viewMode: 'years',
      format: 'YYYY-MM-DD',
    });
  });
</script>
@stop