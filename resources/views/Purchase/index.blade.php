@extends('layouts.admin')
@section('content')	
@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Listado de compras </h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
    			</ul>
    			<div class="clearfix"></div>
      		</div>
    		<div class="x_content">
    		{!! $Purchases->render() !!}
			<table id="datatable" class="table table-hover">
				<thead>
					<th>Número de factura</th>
					<th>Vendedor</th>
					<th>Total</th>
					<th>Operación</th>
				</thead>
				<tbody>
					@foreach($Purchases as $purchase)
					@if($purchase->invoicepurchase == null)
						<?php continue; ?>
					@endif
					<tr>
						<td>{{$purchase->invoicepurchase}}</td>
						<td>{{$purchase->seller}}</td>
						<td>{{$purchase->TOTAL}}</td>
						<td>
							{!!link_to_route('Purchases.edit', $title = 'Editar', $parameters =$purchase->id, $attributes = ['class'=>'btn btn-primary'])!!}
						</td>						
					</tr>
				@endforeach	
				</tbody>					
			</table>
			{!! $Purchases->render() !!}
			</div>
		</div>
	</div>
</div>
@stop