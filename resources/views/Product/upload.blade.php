@extends('layouts.admin')
	@section('content')
		@include('alerts.AlertsRequest')
		@include('alerts.SuccessRequest')
		@include('alerts.ErrorsRequest')
		<div class="">
			<div class="page-title">
		    	<div class="title_left">
		      		<h3>Productos</h3>
		    	</div>
		  	</div>
		  	<div class="clearfix"></div>
		  	<div class="row">
			    <div class="col-md-12 col-sm-12 col-xs-12">
			    	<div class="x_panel">
			    		<div class="x_title">
							<h2>Crear producto <small>Plantilla de creación de productos</small></h2>
			    			<div class="clearfix"></div>
			        	</div>
			        	<div class="x_content">
			        		<br />
			        		<div class="col-md-12">
								<div class="box-header">
									<h3 class="box-title">Cargar Datos de los productos</h3>
								</div><!-- /.box-header -->
								<a class="btn btn-success" href="/downloadfile?fileName=archivoproductos.xlsx" role="button"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Descargar plantilla</a>					     
					      		<form  id="uploadfiles" name="uploadfiles" method="post"  action="/uploadfile" enctype="multipart/form-data" files="true" onsubmit="return validacion()">
					      			<input type="hidden" name="_token" id="_token"  value="<?= csrf_token(); ?>">             
					    			<div class="box-body">	     
					    				<div class="form-group col-xs-12"  >
					            			<label>Agregar Archivo de Excel </label>
					            			<input name="archivo" id="archivo" type="file" class="archivo form-control" required/ accept=".xls,.xlsx"><br /><br />
					        			</div>					     
					    				<div class="box-footer">
					                    	{!!Form::submit('Cargar datos',['id'=>'crear', 'class'=>'btn btn-primary'])!!}
					      				</div>		       
					    			</div>
								</form>
							</div>
						</div>
			      	</div>
			    </div>
			</div>  
		</div>
		<script>
			function validacion()
	        {
	            $('#crear').prop('value', 'enviando...');
	            $('#crear').prop('disabled', true);
	            return(true);           
        	} 
		</script>
	@stop	