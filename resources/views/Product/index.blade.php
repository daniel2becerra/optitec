@extends('layouts.admin')
@section('content')	
@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Listado de productos </h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
    			</ul>
    			<div class="clearfix"></div>
      		</div>
    		<div class="x_content">
    		{!! $products->render() !!}	
			<table id="datatable" class="table table-hover">
				<thead>
					<th>Nombre</th>
					<th>Referencia</th>
					<th>Color</th>
					<th>Compras</th>
					<th>Ventas</th>
					<th>Saldo</th>
					<th>Operación</th>
				</thead>
				<tbody>
					@foreach($products as $product)
					<tr>
						<td>{{$product->name}}</td>
						<td>{{$product->reference}}</td>
						<td>{{$product->color}}</td>
						<td>{{$product->COMPRAS}}</td>
						<td>{{$product->VENTAS}}</td>
						<td>{{$product->COMPRAS - $product->VENTAS}}</td>
						<td>
							{!!link_to_route('Products.edit', $title = 'Editar', $parameters =$product->id, $attributes = ['class'=>'btn btn-primary'])!!}
						</td>
					</tr>					
					@endforeach
				</tbody>	
			</table>
			{!! $products->render() !!}	
			</div>
		</div>
	</div>
</div>
@stop