<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Abonos</title>
        <SPAN style="position: absolute; top: 40px; left: 300px;width: 103px; height: 61px">
        <IMG SRC="images/optica_peque.jpg" >
        </SPAN>
        <link rel="stylesheet" href="assets/css/pedir_lente_style.css" />
        <label id="lbl6" name="lbl6"><h3>Formato para pedir Lentes</h3></label>
        <label id="lbl7" name="lbl7"><h6>TELÉFONO: 339 4178</h6></label>
    </head>
    <body>
      <label id="lbl1" name="lbl1">ORDEN</label> 
      {!!Form::label(null,$Order[0]->id, ['id'=>'id'])!!} 
      <label id="lbl2" name="lbl2">____________</label>
      <label id="lbl3" name="lbl3">FECHA PEDIDO</label>
      {!!Form::label(null,$Order[0]->created_at, ['id'=>'created_at'])!!} 
      <label id="lbl4" name="lbl4">_________________</label>
      <label id="lbl5" name="lbl5">N°        <?php echo $Order[0]->id ?></label>
      <table id="tabla"  name="tabla">
        <tr>
          <td>RX FINAL</td>
          <td>ESFERA</td>
          <td>CILINDRO</td>
          <td>EJE</td>
          <td>ADD</td>
        </tr>
        <tr>
          <td>OD</td>
          <td>_____________</td>
          {!!Form::label(null,$History->OdEsferaRxF, ['id'=>'OdEsferaRxF'])!!}
          <td>_____________</td>
          {!!Form::label(null,$History->OdCilindroRxF, ['id'=>'OdCilindroRxF'])!!}
          <td>_____________</td>
          {!!Form::label(null,$History->OdEjeRxF, ['id'=>'OdEjeRxF'])!!}
          <td>_____________</td>
          {!!Form::label(null,$History->OdAddRxF, ['id'=>'OdAddRxF'])!!}
        </tr> 
        <tr>
          <td>OI</td>
          <td>_____________</td>
          {!!Form::label(null,$History->OiEsferaRxF, ['id'=>'OiEsferaRxF'])!!}
          <td>_____________</td>
          {!!Form::label(null,$History->OiCilindroRxF, ['id'=>'OiCilindroRxF'])!!}
          <td>_____________</td>
          {!!Form::label(null,$History->OiEjeRxF, ['id'=>'OiEjeRxF'])!!}
          <td>_____________</td>
          {!!Form::label(null,$History->OiAddRxF, ['id'=>'OiAddRxF'])!!}
        </tr>
      </table>
      <label id="lbl16" name="lbl16">DP</label>
      {!!Form::label(null,$History->DpRxF, ['id'=>'DpRxF'])!!} 
      <label id="lbl17" name="lbl17">_______________________________</label>
      <label id="lbl18" name="lbl18">ALT</label>
      {!!Form::label(null,$Order[0]->alt, ['id'=>'alt'])!!}
      <label id="lbl19" name="lbl19">_________________________________________</label>
      <label id="lbl20" name="lbl20">LABORATORIO:</label>
      {!!Form::label(null,$Order[0]->NAMELAB, ['id'=>'NAMELAB'])!!} 
      <label id="lbl21" name="lbl21">________________</label>
      <label id="lbl22" name="lbl22">TIPO:</label>
      {!!Form::label(null,$Order[0]->NAMETYPE, ['id'=>'NAMETYPE'])!!}
      <label id="lbl23" name="lbl23">_____________________________________________</label>
      <label id="lbl24" name="lbl24">MATERIAL:</label>
      {!!Form::label(null,$Order[0]->NAMEMATERIAL, ['id'=>'NAMEMATERIAL'])!!}
      <label id="lbl25" name="lbl25">_________________________</label>
      <label id="lbl28" name="lbl28">FILTRO:</label>
      {!!Form::label(null,$Order[0]->NAMEFILTER, ['id'=>'NAMEFILTER'])!!}
      <label id="lbl29" name="lbl29">____________________________________</label>
      <label id="lbl30" name="lbl30">OBSERVACIONES:</label>
      {!!Form::label(null,$Order[0]->observation, ['id'=>'observation'])!!}
      <label id="lbl31" name="lbl31">_________________________________________________________________                                                 
      </label>                    
    </body>
</html>