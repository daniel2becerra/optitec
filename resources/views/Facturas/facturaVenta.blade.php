<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Abonos</title>

        <SPAN style="position: absolute; top: 10 px; left: 10 px;width: 103px; height: 61px">
        <IMG SRC="images/optica3.jpg" >
        </SPAN>


        <link rel="stylesheet" href="assets/css/factura_stylenew.css">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    </head>
    <body>
        <label id="lbl6">FACTURA DE VENTA</label>
        <table id="tabla2"  name="tabla2"><tr><td>No <?php echo $products[0]->IDINVOICE; ?></td></tr></table> 
        <label id="lbl7">FECHA: </label>
        <label id="lbl8">______________________________________________________</label>
        {!!Form::label(null,$products[0]->DATEINVOICE, ['id'=>'lbl15'])!!} 
        <label id="lbl9">NOMBRE:</label>
        <label id="lbl10">_____________________________________________________</label>
        {!!Form::label(null,$products[0]->firstname." ".$products[0]->lastname, ['id'=>'lbl16'])!!} 
        <label id="lbl11">C.C. No.</label>
        <label id="lbl12">______________________</label>
        {!!Form::label(null,$products[0]->identification, ['id'=>'lbl17'])!!} 
        <label id="lbl13">TEL:</label>
        <label id="lbl14">_________________________</label>
        {!!Form::label(null,$products[0]->phone, ['id'=>'lbl18'])!!} 
        <div class="col-md-6">
             <table id="table" class="table table-bordered">
                <thead>
                    <tr>
                        <th colspan="2" >Descripción</th>
                        <th colspan="2" >Valor</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $total = 0; ?>
                    @foreach($products as $product)
                        <?php  $total += $product->price;?>
                        <tr>
                            <td colspan="2" >{{$product->productname}}</td>
                            <td colspan="2" >{{$product->price}}</td> 
                        </tr>
                    @endforeach                    
                    <tr>
                        <td  colspan="2" align="right">TOTAL</td>
                        <td colspan="2" ><?php echo $total; ?></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left">ESTA FACTURA DE VENTA ES UN TITULO VALOR, CONFORME AL ART.772, MODIFICADO EN LA LEY 1231 DE NOV. DE 2008 DEL C.C.</td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">FECHA DE ENTREGA: </td>
                        <td colspan="2" align="left">VENDEDOR: </td>
                    </tr>
                </tbody>
            </table>
         </div>
    </body>
</html>