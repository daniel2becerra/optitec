@extends('layouts.admin')
	@section('content')
		@include('alerts.AlertsRequest')
		@include('alerts.SuccessRequest')
		@include('alerts.ErrorsRequest')
		<div class="">
		  <div class="page-title">
		    <div class="title_left">
		      <h3>Venta rápida</h3>
		    </div>
		  </div>
		  <div class="clearfix"></div>
		  <div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		      <div class="x_panel">
		        <div class="x_title">
		          <h2>Venta nueva <small>Venta rápida sin consulta</small></h2>
		          <div class="clearfix"></div>
		        </div>
		        <div class="x_content">
					@include('Forms.Quicksale')
				</div>
		      </div>
		    </div>
		  </div>  
		</div>
		<script>
			$(document).ready(function()
			  {
			      $('.selectClass').select2();
			  });
		</script>
	@stop	