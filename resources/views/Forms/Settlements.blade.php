<div class="row">
  <div class="col-sm-3 col-sm-offset-1">                            
    <div class="form-group">
      {!!Form::label('Pendiente *: ', null, ['for'=>'pendiente'])!!}
      {!!Form::text('pendiente',$pendiente, ['id'=>'pendiente', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'pendiente ', 'disabled'])!!}
    </div>
  </div>
  <div class="col-sm-3">                            
    <div class="form-group">
      {!!Form::label('Por liquidar *: ', null, ['for'=>'porLiquidar'])!!}
      {!!Form::text('porLiquidar',$total, ['id'=>'porLiquidar', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'porLiquidar ', 'disabled'])!!}
    </div>
  </div>
  <div class="col-sm-3">                            
    <div class="form-group">
      {!!Form::label('Entrega *: ', null, ['for'=>'entrega'])!!}
      {!!Form::number('entrega',null, ['id'=>'entrega', 'class'=>'saldoFinal form-control col-md-7 col-xs-12', 'placeholder'=>'Entrega ', "onkeypress"=>"return isNumberKey(event)"])!!}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-3 col-sm-offset-1">                            
    <div class="form-group">
      {!!Form::label('Recibe *: ', null, ['for'=>'recibe'])!!}
      {!!Form::number('recibe',null, ['id'=>'recibe', 'class'=>'saldoFinal form-control col-md-7 col-xs-12', 'placeholder'=>'Recibe ', "onkeypress"=>"return isNumberKey(event)"])!!}
    </div>
  </div>
  <div class="col-sm-3">                            
    <div class="form-group">
      {!!Form::label('Gastos*: ', null, ['for'=>'gastos'])!!}
      {!!Form::number('gastos',null, ['id'=>'gastos', 'class'=>'saldoFinal form-control col-md-7 col-xs-12', 'placeholder'=>'Gastos ', "onkeypress"=>"return isNumberKey(event)"])!!}
    </div>
  </div>
  <div class="col-sm-3">                            
    <div class="form-group">
      {!!Form::label('Saldo final *: ', null, ['for'=>'saldoFinal'])!!}
      {!!Form::text('saldoFinal', 0, ['id'=>'saldoFinal', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'saldoFinal ', 'disabled'])!!}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-10 col-sm-offset-1">                            
    <div class="form-group">
      {!!Form::label('Notas *: ', null, ['for'=>'notas'])!!}
      {!!Form::textarea('notas',null, ['id'=>'notas', 'class'=>'form-control col-md-7 col-xs-12', 'rows'=>'3', 'placeholder'=>'notas'])!!}
    </div>
  </div>         
</div>

<div class="ln_solid"></div>
<script type="text/javascript">
  $('.saldoFinal').keyup(function(e)
  {
    var pendiente = $('#pendiente').val();
    var porLiquidar = $('#porLiquidar').val();
    var entrega = $('#entrega').val();
    var recibe = $('#recibe').val();
    var gastos = $('#gastos').val();
    if(entrega == null || entrega == '')
    {
      entrega = 0;
    }
    if(recibe == null || recibe == '')
    {
      recibe = 0;
    }
    if(gastos == null || gastos == '')
    {
      gastos = 0;
    }
    $('#saldoFinal').val(parseInt(pendiente)-parseInt(porLiquidar)+parseInt(entrega)-parseInt(recibe)+parseInt(gastos));
  });
  function isNumberKey(evt){
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;
      return true;
  }
</script>