<div class="row">
    <div class='col-sm-5 col-sm-offset-1'>
        {!!Form::label('Identificación *: ', null, ['for'=>'identification'])!!}
        <div class="input-group">
            {!!Form::text('identification',null, ['id'=>'identification', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Identificación'])!!}
            <span class="input-group-btn">
                <button type="button" class="btn btn-success"><span class="fa fa-search" id="search"></span></button>
            </span>
        </div>
    </div> 
    <div class="col-md-5">
        <div class="form-group">
            {!!Form::label('Nombre Completo *: ', null, ['for'=>'name'])!!}
            {!!Form::text('name',null, ['id'=>'name', 'class'=>'form-control', 'placeholder'=>'Nombres completos', 'readonly'=>'true'])!!}
        </div>
    </div>
</div>
<div class="row">
  <div class="col-sm-2 col-sm-offset-1">                            
    <div class="form-group">
      {!!Form::label('Od Esfera*: ', null, ['for'=>'OdEsferaRxF'])!!}
      {!!Form::text('OdEsferaRxF',null, ['id'=>'OdEsferaRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Od Esfera ', 'readonly'=>'true'])!!}
    </div>
  </div> 
  <div class="col-sm-2">                            
    <div class="form-group">
      {!!Form::label('Od Cilindro*: ', null, ['for'=>'OdCilindroRxF'])!!}
      {!!Form::text('OdCilindroRxF',null, ['id'=>'OdCilindroRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Od Cilindro ', 'readonly'=>'true'])!!}
    </div>
  </div> 
  <div class="col-sm-2">                            
    <div class="form-group">
      {!!Form::label('Od Eje*: ', null, ['for'=>'OdEjeRxF'])!!}
      {!!Form::text('OdEjeRxF',null, ['id'=>'OdEjeRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Od Eje ', 'readonly'=>'true'])!!}
    </div>
  </div> 
  <div class="col-sm-2">                            
    <div class="form-group">
      {!!Form::label('Od av.*: ', null, ['for'=>'OdAvRxF'])!!}
      {!!Form::text('OdAvRxF',null, ['id'=> 'OdAvRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'av. Od ', 'readonly'=>'true'])!!}
    </div>
  </div> 
  <div class="col-sm-2">                            
    <div class="form-group">
      {!!Form::label('Od add *: ', null, ['for'=>'OdAddRxF'])!!}
      {!!Form::text('OdAddRxF',null, ['id'=>'OdAddRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Od add ', 'readonly'=>'true'])!!}
    </div>
  </div>
  <div class="col-sm-2 col-sm-offset-1">                            
    <div class="form-group">
      {!!Form::label('Oi Esfera*: ', null, ['for'=>'OiEsferaRxF'])!!}
      {!!Form::text('OiEsferaRxF',null, ['id'=>'OiEsferaRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Oi Esfera ', 'readonly'=>'true'])!!}
    </div>
  </div>      
  <div class="col-sm-2">                            
    <div class="form-group">
      {!!Form::label('Oi Cilindro*: ', null, ['for'=>'OiCilindroRxF'])!!}
      {!!Form::text('OiCilindroRxF',null, ['id'=>'OiCilindroRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Oi Cilindro ', 'readonly'=>'true'])!!}
    </div>
  </div>      
  <div class="col-sm-2">                            
    <div class="form-group">
      {!!Form::label('Oi Eje*: ', null, ['for'=>'OiEjeRxF'])!!}
      {!!Form::text('OiEjeRxF',null, ['id'=>'OiEjeRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Oi Eje ', 'readonly'=>'true'])!!}
    </div>
  </div>      
  <div class="col-sm-2">                            
    <div class="form-group">
      {!!Form::label('Oi av.*: ', null, ['for'=>'OiAvRxF'])!!}
      {!!Form::text('OiAvRxF',null, ['id'=>'OiAvRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Oi av.', 'readonly'=>'true'])!!}
    </div>
  </div> 
  <div class="col-sm-2">                            
    <div class="form-group">
      {!!Form::label('Oi add *: ', null, ['for'=>'OiAddRxF'])!!}
      {!!Form::text('OiAddRxF',null, ['id'=>'OiAddRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Oi add ', 'readonly'=>'true'])!!}
    </div>
  </div>
  <div class="col-sm-3 col-sm-offset-1">                            
    <div class="form-group">
      {!!Form::label('DP *: ', null, ['for'=>'DpRxF'])!!}
      {!!Form::text('DpRxF',null, ['id'=>'DpRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'DP ', 'readonly'=>'true'])!!}
    </div>
  </div>
  <div class="col-sm-3">                            
    <div class="form-group">
      {!!Form::label('Alt *: ', null, ['for'=>'alt'])!!}
      {!!Form::text('alt',null, ['id'=>'alt', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Alt ', 'readonly'=>'true'])!!}
    </div>
  </div>
  <div class="col-sm-4"> 
    <div class="form-group">
      {!!Form::label('Montura *: ', null, ['for'=>'product_id'])!!}
        {!!form::select('product_id', $products, null, ['id'=>'product_id', 'class'=> 'form-control selectClass', 'placeholder'=>'Seleccione una opción', 'disabled'])!!}  
    </div>
  </div>
  <div class="col-sm-5 col-sm-offset-1"> 
    <div class="form-group">
      <label>Laboratorio <span class="required">*</span></label>
        {!!form::select('lab_id', $labs, null, ['id'=>'lab_id', 'class'=> 'form-control selectClass', 'placeholder'=>'Seleccione una opción', 'disabled'])!!}  
    </div>
  </div>
  <div class="col-sm-5"> 
    <div class="form-group">
      <label>Tipo de lente <span class="required">*</span></label>
        {!!form::select('typelen_id', [], null, ['id'=>'typelen_id', 'class'=> 'form-control selectClass', 'placeholder'=>'Seleccione una opción', 'disabled'])!!}  
    </div>
  </div>
  <div class="col-sm-5 col-sm-offset-1"> 
    <div class="form-group">
      <label>Material del lente <span class="required">*</span></label>
        {!!form::select('materiallen_id', [], null, ['id'=>'materiallen_id', 'class'=> 'form-control selectClass', 'placeholder'=>'Seleccione una opción', 'disabled'])!!}  
    </div>
  </div>
  <div class="col-sm-5"> 
    <div class="form-group">
      <label>Filtro del lente <span class="required">*</span></label>
        {!!form::select('filterlen_id', [], null, ['id'=>'filterlen_id', 'class'=> 'form-control selectClass', 'placeholder'=>'Seleccione una opción', 'disabled'])!!}  
    </div>
  </div>
  <div class="col-sm-3 col-sm-offset-1">                            
    <div class="form-group">
      {!!Form::label('Valor de la orden*: ', null, ['for'=>'price'])!!}
      {!!Form::text('price',null, ['id'=>'price', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Precio ', 'readonly'=>'true'])!!}
    </div>
  </div>
  <div class="col-sm-3">                            
    <div class="form-group">
      {!!Form::label('Abono*: ', null, ['for'=>'paymentprice'])!!}
      {!!Form::text('paymentprice',null, ['id'=>'paymentprice', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Abono ', 'readonly'=>'true'])!!}
    </div>
  </div>
  <div class="col-sm-4">                            
    <div class="form-group">
      {!!Form::label('Saldo pendiente*: ', null, ['for'=>'pendiente'])!!}
      {!!Form::text('pendiente',null, ['id'=>'pendiente', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Saldo pendiente ', 'readonly'=>'true'])!!}
    </div>
  </div>
  <div class="col-sm-10 col-sm-offset-1">                            
    <div class="form-group">
      {!!Form::label('Observación *: ', null, ['for'=>'observation'])!!}
      {!!Form::textarea('observation',null, ['id'=>'observation', 'class'=>'form-control col-md-7 col-xs-12', 'rows'=>'2', 'placeholder'=>'Observación', 'readonly'=>'true'])!!}
    </div>
  </div>         
</div>
{!!Form::hidden('client_id', null, ['id'=>'client_id'])!!}
{!!Form::hidden('typeorder_id', 1, ['id'=>'typeorder_id'])!!}
{!!Form::hidden('medicalhistory_id', null, ['id'=>'medicalhistory_id'])!!}
{!!Form::hidden('materialprice_id', null, ['id'=>'materialprice_id'])!!}
{!!Form::hidden('priceproduct', null, ['id'=>'priceproduct'])!!}
{!!Form::hidden('pricelen', null, ['id'=>'pricelen'])!!}
{!!Form::hidden('pricefilter', null, ['id'=>'pricefilter'])!!}
<div class="ln_solid"></div>
<script type="text/javascript">
  var priceProduct = 0.0;
  var priceMarkMaterial = 0.0;
  var priceFilter = 0.0;
  $('#price').keyup(function(e)
  {
    var price = $('#price').val();
    var paymentprice = $('#paymentprice').val();
    if(price == null || price == '')
    {
      price = 0;
    }
    if(paymentprice == null || paymentprice == '')
    {
      paymentprice = 0;
    }
    $('#pendiente').val(parseInt(price)-parseInt(paymentprice));
  });

  $('#paymentprice').keyup(function(e)
  {
    var price = $('#price').val();
    var paymentprice = $('#paymentprice').val();
    if(price == null || price == '')
    {
      price = 0;
    }
    if(paymentprice == null || paymentprice == '')
    {
      paymentprice = 0;
    }
    $('#pendiente').val(parseInt(price)-parseInt(paymentprice));
  });


  $('#lab_id').on('change', function(e)
  {
    $('#typelen_id').empty();
    $('#materiallen_id').empty();
    $('#filterlen_id').empty();
    var lab_id = e.target.value;
    $.get('/JsonTypelen?lab_id='+lab_id, function(data)
    {
      $('#typelen_id').append($('<option>', {
            text: "Seleccione una opción"
        }));
      $('#materiallen_id').append($('<option>', {
            text: "Seleccione una opción"
        }));
      $('#filterlen_id').append($('<option>', {
            text: "Seleccione una opción"
        }));
      $.each(data, function(index, tlObj)  
      {
        $('#typelen_id').append($('<option>', {
            value: index,
            text: tlObj
        }));
      });
    });
    $.get('/JsonFilterlenLab?lab_id='+lab_id, function(data)
    {
      $.each(data, function(index, flObj)  
      {
        $('#filterlen_id').append($('<option>', {
            value: index,
            text: flObj
        }));
      });
    });
  });

  $('#typelen_id').on('change', function(e)
  {
    $('#materiallen_id').empty();
    var typelen_id = e.target.value;
    $.get('/JsonMateriallen?typelen_id='+typelen_id, function(dataMat)
    {
      $('#materiallen_id').append($('<option>', {
            text: "Seleccione una opción"
        }));
      $.each(dataMat, function(index, mtlObj)  
      {
        $('#materiallen_id').append($('<option>', {
            value: index,
            text: mtlObj
        }));
      });
    });
  });

  $('#product_id').on('change', function(e)
  {
    var product_id = e.target.value;
    $.get('/JsonProduct?product_id='+product_id, function(dataProduct)
    {
      priceProduct = parseFloat(dataProduct.PRECIOVENTA);
      $('#priceproduct').val(priceProduct);
      var paymentprice = $('#paymentprice').val();
      if(paymentprice == null || paymentprice == '')
      {
        paymentprice = 0.0;
      }
      var total = parseFloat(priceProduct) + parseFloat(priceMarkMaterial) + parseFloat(priceFilter);
      var abono = $('#paymentprice').val();
      $('#pendiente').val(total-abono);
      $('#price').val(total);
    });
  });

  $('#materiallen_id').on('change', function(e)
  {
    var materiallen_id = e.target.value;
    var marklen_id = $('#marklen_id').val();
    $.get('/JsonMaterialPrice?materiallen_id='+materiallen_id, function(dataPrice)
    {
      priceMaterial = parseFloat(dataPrice.price);
      $('#pricelen').val(priceMaterial);
      $('#materialprice_id').val(dataPrice.id);
      if(priceMaterial == null || priceMaterial == '')
      {
        priceMaterial = 0.0;
      }
      var paymentprice = $('#paymentprice').val();
      if(paymentprice == null || paymentprice == '')
      {
        paymentprice = 0.0;
      }
      var total = parseFloat(priceProduct) + parseFloat(priceMaterial) + parseFloat(priceFilter);
      var abono = $('#paymentprice').val();
      $('#pendiente').val(total-abono);
      $('#price').val(total);
    });
  });

  $('#filterlen_id').on('change', function(e)
  {

    var filterlen_id = e.target.value;
    $.get('/JsonFilterlenId?id='+filterlen_id, function(dataFilter)
    {
      priceFilter = parseFloat(dataFilter.price);
      $('#pricefilter').val(priceFilter);
      if(priceFilter == null || priceFilter == '')
      {
        priceFilter = 0.0;
      }
      var paymentprice = $('#paymentprice').val();
      if(paymentprice == null || paymentprice == '')
      {
        paymentprice = 0.0;
      }
      var total = parseFloat(priceProduct) + parseFloat(priceMaterial) + parseFloat(priceFilter);
      var abono = $('#paymentprice').val();
      $('#pendiente').val(total-abono);
      $('#price').val(total);
    });
  });

  $("#frm").submit(function (event)
  {
      event.preventDefault();
      $('.loading').show();
      var form = $(this);
      var data = new FormData($(this)[0]);
      var url = form.attr("action");
      $.ajax({
          type: "POST",
          url: url,
          data: data,
          async: false,
          cache: false,
          contentType: false,
          processData: false,
          success: function (data) {
              if (data.fail) {
                  $('#frm input.required, #frm textarea.required').each(function () {
                      index = $(this).attr('name');
                      if (index in data.errors) {
                          $("#form-" + index + "-error").addClass("has-error");
                          $("#" + index + "-error").html(data.errors[index]);
                      }
                      else {
                          $("#form-" + index + "-error").removeClass("has-error");
                          $("#" + index + "-error").empty();
                      }
                  });
                  $('#focus').focus().select();
              } else {
                  $(".has-error").removeClass("has-error");
                  $(".help-block").empty();
                  $('.loading').hide();
                  swal({
                  title: '<i>Orden de pedido guardada</i><br> Ahora puede imprimir: ',
                  type: 'info',
                  html: '<a href="/Factura/pedirlentes?order_id='+data.order_id+'" class="btn btn-info" target="_blank"><i class="fa fa-file-pdf-o"></i> Orden</a>' +
                        '<a href="/Factura/abono?payment_id='+data.payment_id+'&order_id='+data.order_id+'" class="btn btn-info" target="_blank"><i class="fa fa-file-pdf-o"></i> Abono</a>',
                  allowOutsideClick: false,
                  confirmButtonText:
                    '<i class="fa fa-thumbs-up"></i> OK',
                }).then(function (result) {
                  if (result.value) {
                    location.reload();
                  }
                })
              }
          },
          error: function (xhr, textStatus, errorThrown) {
              alert(errorThrown);
          }
      });
      return false;
  });

  $(document).ready(function()
  {
      $('.selectClass').select2();
  });
  $('#search').click(function(e) 
    {
      var id = $('#identification').val();
      if(id.length > 0)
      {
        $.get('/JsonCliente?identification='+id, function(dataCliente)
        {
          if(dataCliente.length == 0)
          {
            
          }
          else
          {
            $.each(dataCliente, function(index, clientObj)
            {
              var firstname = clientObj.firstname;
              var lastname = clientObj.lastname;
              var client_id = clientObj.id;
              $('#client_id').val(client_id);
              $('#name').val(firstname+' '+lastname);
              $('#identification').prop('readonly', true);
              $('#alt').prop('readonly', false);
              $('#product_id').prop('disabled', false);
              $('#lab_id').prop('disabled', false);
              $('#typelen_id').prop('disabled', false);
              $('#materiallen_id').prop('disabled', false);
              $('#marklen_id').prop('disabled', false);
              $('#filterlen_id').prop('disabled', false);
              $('#observation').prop('readonly', false);
              $('#paymentprice').prop('readonly', false);
              $('#crear').prop('disabled', false);
              $.get('/JsonMedicalHistory?client_id='+clientObj.id, function(dataHistory)
              {
                $('#medicalhistory_id').val(dataHistory[0].id);
                $('#OdEsferaRxF').val(dataHistory[0].OdEsferaRxF);
                $('#OdEsferaRxF').val(dataHistory[0].OdEsferaRxF);
                $('#OdCilindroRxF').val(dataHistory[0].OdCilindroRxF);
                $('#OdEjeRxF').val(dataHistory[0].OdEjeRxF);
                $('#OdAvRxF').val(dataHistory[0].OdAvRxF);
                $('#OdAddRxF').val(dataHistory[0].OdAddRxF);  
                $('#OiEsferaRxF').val(dataHistory[0].OiEsferaRxF);
                $('#OiCilindroRxF').val(dataHistory[0].OiCilindroRxF);
                $('#OiEjeRxF').val(dataHistory[0].OiEjeRxF);
                $('#OiAvRxF').val(dataHistory[0].OiAvRxF);
                $('#OiAddRxF').val(dataHistory[0].OiAddRxF);
                $('#DpRxF').val(dataHistory[0].DpRxF);                
              });
            });
          }
        });
      }
    });
</script>