{!!Html::style('assets/css/select2.min.css')!!}
<div class="wizard-navigation">
  <div class="progress-with-circle">
      <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4" style="width: 15%;"></div>
  </div>
  <ul >
    <li>
      <a id="db" href="#datosBasicos" data-toggle="tab" >
        <div class="icon-circle">
          <i class="ti-map"></i>
        </div>
        Información personal
      </a>
    </li>
    <li>
      <a id="h6" href="#historia6" data-toggle="tab">
        <div class="icon-circle">
          <i class="ti-comments"></i>
        </div>
        Diagnóstico
      </a>
    </li>
    <li>
      <a id="h7" href="#historia7" data-toggle="tab">
        <div class="icon-circle">
          <i class="ti-comments"></i>
        </div>
        RX Final
      </a>
    </li>
  </ul>
</div>
<div class="tab-content">
  <div class="tab-pane" id="datosBasicos">
    <div class="row">
        <div class="col-sm-12">
            <h5 class="info-text"> Información personal del paciente</h5>
        </div>
        <input id="_token" type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="row">
        <div class='col-sm-4'>
          {!!Form::label('Documento *: ', null, ['for'=>'identification'])!!}
          <div class="input-group">
            {!!Form::text('identification', null, ['id'=>'identification', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Documento'])!!}
            <span class="input-group-btn">
                <button type="button" class="btn btn-primary"><span class="fa fa-search" id="search"></span></button>
            </span>
          </div>
        </div>      
        <div class="col-sm-4">                            
          <div class="form-group">
            {!!Form::label('Nombres *: ', null, ['for'=>'firstname'])!!}
            {!!Form::text('firstname',null, ['id'=>'firstname', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Nombres', 'disabled'])!!}
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            {!!Form::label('Apellidos *: ', null, ['for'=>'lastname'])!!}
            {!!Form::text('lastname',null, ['id'=>'lastname', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Apellidos', 'disabled'])!!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">                            
          <div class="form-group">
            {!!Form::label('Correo *: ', null, [ 'for'=>'email'])!!}
            {!!Form::text('email',null, ['id'=>'email', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Correo', 'disabled'])!!}
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            {!!Form::label('Teléfono *: ', null, [ 'for'=>'phone'])!!}
            {!!Form::text('phone',null, ['id'=>'phone', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Teléfono', 'disabled'])!!}
          </div>
        </div>
        <div class='col-sm-4'>
            {!!Form::label('F. de nacimiento *: ', null, [ 'for'=>'birthday'])!!}
            <div class='input-group date' id='birthdayDiv'>
              {!!Form::text('birthday', null, ['id'=>'birthday', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'YYYY-MM-DD', 'disabled'])!!}
              <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            {!!Form::label('Profesión *: ', null, [ 'for'=>'profession'])!!}
            {!!Form::text('profession',null, ['id'=>'profession', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Profesión', 'disabled'])!!}
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="tab-pane" id="historia6">
    <div class="row">
      <div class="col-sm-12">
          <h5 class="info-text"> Diagnóstico</h5>
      </div>
      <div class="col-md-12">                            
        <div class="form-group">
          {!!Form::label('Diagnostico *: ', null, ['for'=>'diagnostico'])!!}
          {!!Form::select('diagnosticos[]', $cie10s, null, ['id'=>'diagnostico', 'class'=> 'col-md-12 diag selectClass', 'required', 'multiple'=>'multiple'])!!}
        </div>
      </div>              
    </div>
  </div>
  <div class="tab-pane" id="historia7">
    <div class="row">
      <div class="col-sm-12">
          <h5 class="info-text"> RX Final</h5>
      </div>
      <div class="col-sm-2 col-sm-offset-1">                            
        <div class="form-group">
          {!!Form::label('Od Esfera*: ', null, ['for'=>'odEsferaRxF'])!!}
          {!!Form::text('OdEsferaRxF',null, ['id'=>'odEsferaRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Od Esfera '])!!}
        </div>
      </div> 
      <div class="col-sm-2">                            
        <div class="form-group">
          {!!Form::label('Od Cilindro*: ', null, ['for'=>'OdCilindroRxF'])!!}
          {!!Form::text('OdCilindroRxF',null, ['id'=>'OdCilindroRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Od Cilindro '])!!}
        </div>
      </div> 
      <div class="col-sm-2">                            
        <div class="form-group">
          {!!Form::label('Od Eje*: ', null, ['for'=>'OdEjeRxF'])!!}
          {!!Form::text('OdEjeRxF',null, ['id'=>'OdEjeRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Od Eje '])!!}
        </div>
      </div> 
      <div class="col-sm-2">                            
        <div class="form-group">
          {!!Form::label('Od av.*: ', null, ['for'=>'OdAvRxF'])!!}
          {!!Form::text('OdAvRxF',null, ['id'=> 'OdAvRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'av. Od '])!!}
        </div>
      </div> 
      <div class="col-sm-2">                            
        <div class="form-group">
          {!!Form::label('Od add *: ', null, ['for'=>'OdAddRxF'])!!}
          {!!Form::text('OdAddRxF',null, ['id'=>'OdAddRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Od add '])!!}
        </div>
      </div>
      <div class="col-sm-2 col-sm-offset-1">                            
        <div class="form-group">
          {!!Form::label('Oi Esfera*: ', null, ['for'=>'OiEsferaRxF'])!!}
          {!!Form::text('OiEsferaRxF',null, ['id'=>'OiEsferaRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Oi Esfera '])!!}
        </div>
      </div>      
      <div class="col-sm-2">                            
        <div class="form-group">
          {!!Form::label('Oi Cilindro*: ', null, ['for'=>'OiCilindroRxF'])!!}
          {!!Form::text('OiCilindroRxF',null, ['id'=>'OiCilindroRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Oi Cilindro '])!!}
        </div>
      </div>      
      <div class="col-sm-2">                            
        <div class="form-group">
          {!!Form::label('Oi Eje*: ', null, ['for'=>'OiEjeRxF'])!!}
          {!!Form::text('OiEjeRxF',null, ['id'=>'OiEjeRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Oi Eje '])!!}
        </div>
      </div>      
      <div class="col-sm-2">                            
        <div class="form-group">
          {!!Form::label('Oi av.*: ', null, ['for'=>'OiAvRxF'])!!}
          {!!Form::text('OiAvRxF',null, ['id'=>'OiAvRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Oi av.'])!!}
        </div>
      </div> 
      <div class="col-sm-2">                            
        <div class="form-group">
          {!!Form::label('Oi add *: ', null, ['for'=>'OiAddRxF'])!!}
          {!!Form::text('OiAddRxF',null, ['id'=>'OiAddRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Oi add '])!!}
        </div>
      </div>
      <div class="col-sm-5 col-sm-offset-1">                            
        <div class="form-group">
          {!!Form::label('DP *: ', null, ['for'=>'DpRxF'])!!}
          {!!Form::text('DpRxF',null, ['id'=>'DpRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'DP '])!!}
        </div>
      </div>
      <div class="col-sm-5">                            
        <div class="form-group">
          {!!Form::label('Uso *: ', null, ['for'=>'UsoRxF'])!!}
          {!!Form::text('UsoRxF',null, ['id'=>'UsoRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Uso '])!!}
        </div>
      </div>
      <div class="col-sm-10 col-sm-offset-1">                            
        <div class="form-group">
          {!!Form::label('Observación *: ', null, ['for'=>'observationRxF'])!!}
          {!!Form::textarea('observationRxF',null, ['id'=>'observationRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'rows'=>'2', 'placeholder'=>'Observación'])!!}
        </div>
      </div>         
    </div>
  </div>
  <div class="tab-pane" id="facilities">
    <h5 class="info-text">Tell us more about facilities. </h5>
    <div class="row">
      <div class="col-sm-5 col-sm-offset-1">
        <div class="form-group">
          <label>Your place is good for</label>
          <select class="form-control">
            <option disabled="" selected="">- type -</option>
            <option>Business</option>
            <option>Vacation </option>
            <option>Work</option>
          </select>
        </div>
      </div>
      <div class="col-sm-5">
        <div class="form-group">
            <label>Is air conditioning included ?</label>
            <select class="form-control">
                <option disabled="" selected="">- response -</option>
                <option>Yes</option>
                <option>No </option>
            </select>
        </div>
      </div>
      <div class="col-sm-5 col-sm-offset-1">
        <div class="form-group">
            <label>Does your place have wi-fi?</label>
            <select class="form-control">
                <option disabled="" selected="">- response -</option>
                <option>Yes</option>
                <option>No </option>
            </select>
        </div>
      </div>
      <div class="col-sm-5">
        <div class="form-group">
            <label>Is breakfast included?</label>
            <select class="form-control">
                <option disabled="" selected="">- response -</option>
                <option>Yes</option>
                <option>No </option>
            </select>
        </div>
      </div>
    </div>
  </div>
  <div class="tab-pane" id="type">
    <h5 class="info-text">What type of location do you have? </h5>
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2">
          <div class="col-sm-4 col-sm-offset-2">
            <div class="choice" data-toggle="wizard-checkbox">
              <input type="checkbox" name="jobb" value="Design">
              <div class="card card-checkboxes card-hover-effect">
                <i class="ti-home"></i>
                <p>Home</p>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="choice" data-toggle="wizard-checkbox">
              <input type="checkbox" name="jobb" value="Design">
              <div class="card card-checkboxes card-hover-effect">
                <i class="ti-package"></i>
                <p>Apartment</p>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
<div class="wizard-footer">
  <div class="pull-right">
    <input id="next" type='button' class='btn btn-next btn-fill btn-primary btn-wd' name='next' value='Siguiente' disabled />
    {!!Form::submit('Finalizar',['id'=>'finish', 'class'=>'btn btn-finish btn-fill btn-success btn-wd'])!!}
  </div>
  <div class="pull-left">
      <input type='button' class='btn btn-previous btn-default btn-wd' name='previous' value='Atrás' />
  </div>
  <div class="clearfix"></div>
</div>
{!!Html::script('assets/js/select2.min.js')!!}
<script>
$("#frm").submit(function (event) {
    event.preventDefault();
    $('.loading').show();
    var form = $(this);
    var data = new FormData($(this)[0]);
    var url = form.attr("action");
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.fail) {
                $('#frm input.required, #frm textarea.required').each(function () {
                    index = $(this).attr('name');
                    if (index in data.errors) {
                        $("#form-" + index + "-error").addClass("has-error");
                        $("#" + index + "-error").html(data.errors[index]);
                    }
                    else {
                        $("#form-" + index + "-error").removeClass("has-error");
                        $("#" + index + "-error").empty();
                    }
                });
                $('#focus').focus().select();
            } else {
                $(".has-error").removeClass("has-error");
                $(".help-block").empty();
                $('.loading').hide();
                console.log(data);
                swal({
                title: '<i>Historia clínica guardada</i><br> Ahora puede imprimir: ',
                type: 'info',
                html: '<a href="/Factura/lentes?id='+data.id+'" class="btn btn-info" target="_blank"><i class="fa fa-file-pdf-o"></i> RX Final</a>',
                allowOutsideClick: false,
                confirmButtonText:
                  '<i class="fa fa-thumbs-up"></i> OK',
              }).then(function (result) {
                if (result.value) {
                  location.reload();
                }
              })
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
    return false;
});
  $(document).ready(function()
  {
      $('.selectClass').select2();
  });

  $(function ()
  {
    $('#birthdayDiv').datetimepicker({
        viewMode: 'years',
      format: 'YYYY-MM-DD',
    });
  });
  function pulsar(e) { 
  tecla = (document.all) ? e.keyCode :e.which; 
  return (tecla!=13); 
  } 
  $('#search').click(function(e) 
    {
      $("#accordion").empty();
      var id = $('#identification').val();
      if(id.length > 0)
      {
        $('#identification').prop('readonly', true);
        $.get('/JsonCliente?identification='+id, function(data)
        {
          if(data.length == 0)
          {
            $('#firstname').prop('disabled', false);
            $('#lastname').prop('disabled', false);
            $('#email').prop('disabled', false);
            $('#phone').prop('disabled', false);
            $('#birthday').prop('disabled', false);
            $('#profession').prop('disabled', false);
            $('#next').prop('disabled', false);
          }
          else
          {
            $.each(data, function(index, clientObj)
            {
              $('#next').prop('disabled', false);
              $('#email').prop('disabled', false);
              $('#phone').prop('disabled', false);
              $('#firstname').val(clientObj.firstname);
              $('#lastname').val(clientObj.firstname);
              $('#email').val(clientObj.email);
              $('#phone').val(clientObj.phone);
              $('#birthday').val(clientObj.birthday);
              $('#profession').val(clientObj.profession);
              $('#identification').prop('readonly', true);
              $('#history').prop('hidden', false);

              $.get('/JsonMedicalHistory?client_id='+clientObj.id, function(data)
              {
                $.each(data, function(index, medicalHistoryObj)
                {
                  if (index == 0)
                    {
                      var collapsein = "in";
                    }
                  else
                    {
                      var collapsein = "";
                    }
                  $('#accordion').append("<div class='panel panel-default'><div class='panel-heading' role='tab' id='heading"+index+"'><h4 class='panel-title'><a role='button' data-toggle='collapse' data-parent='#accordion' href='#collapse"+index+"' aria-expanded='true' aria-controls='collapse"+index+"'>"+medicalHistoryObj.created_at+"</a></h4></div><div id='collapse"+index+"' class='panel-collapse collapse' role='tabpanel' aria-labelledby='heading"+index+"'><div class='panel-body'><div class='row'><div class='col-md-12'><label>Razón de la consulta: </label><em>"+medicalHistoryObj.reasonconsultation+"</em></div></div><div class='row'><div class='col-md-12'><h6>Agudeza visual</h6></div><div class='col-md-4'><label>Od con corrección</label><em>"+medicalHistoryObj.odwithcorrection+"</em></div><div class='col-md-4'><label>Oi con corrección</label><em>"+medicalHistoryObj.oiwithcorrection+"</em></div><div class='col-md-4'><label>Ao con corrección</label><em>"+medicalHistoryObj.aowithcorrection+"</em></div><div class='col-md-4'><label>Od sin corrección</label><em>"+medicalHistoryObj.odwithoutcorrection+"</em></div><div class='col-md-4'><label>Oi sin corrección</label><em>"+medicalHistoryObj.oiwithoutcorrection+"</em></div><div class='col-md-4'><label>Ao sin corrección</label><em>"+medicalHistoryObj.aowithoutcorrection+"</em></div></div><div class='row'><div class='col-md-12'><h6>RX en uso</h6></div><div class='col-md-3'><label>Od</label><em>"+medicalHistoryObj.odRx+"</em></div><div class='col-md-3'><label>Oi</label><em>"+medicalHistoryObj.oiRx+"</em></div><div class='col-md-3'><label>Add</label><em>"+medicalHistoryObj.addRx+"</em></div><div class='col-md-3'><label>Tipo de lente</label><em>"+medicalHistoryObj.typeLen+"</em></div></div><div class='row'><div class='col-md-12'><h6>Examen externo</h6></div><div class='col-md-6'><label>Od</label><em>"+medicalHistoryObj.odEE+"</em></div><div class='col-md-6'><label>Oi</label><em>"+medicalHistoryObj.oiEE+"</em></div></div><div class='row'><div class='col-md-12'><h6>Motilidad ocular</h6></div><div class='col-md-3'><label>Cover test</label><em>"+medicalHistoryObj.covertest+"</em></div><div class='col-md-3'><label>PPM</label><em>"+medicalHistoryObj.ppm+"</em></div><div class='col-md-3'><label>Ducciones</label><em>"+medicalHistoryObj.ductions+"</em></div><div class='col-md-3'><label>Versiones</label><em>"+medicalHistoryObj.versions+"</em></div></div><div class='row'><div class='col-md-12'><h6>Fondo de ojo</h6></div><div class='col-md-6'><label>Od</label><em>"+medicalHistoryObj.odfondo+"</em></div><div class='col-md-6'><label>Oi</label><em>"+medicalHistoryObj.oifondo+"</em></div></div><div class='row'><div class='col-md-12'><h6>Queratometría</h6></div><div class='col-md-6'><label>Od</label><em>"+medicalHistoryObj.odQ+"</em></div><div class='col-md-6'><label>Oi</label><em>"+medicalHistoryObj.oiQ+"</em></div></div><div class='row'><div class='col-md-12'><h6>Refracción</h6></div><div class='col-md-6'><label>Od</label><em>"+medicalHistoryObj.odR+"</em></div><div class='col-md-6'><label>Oi</label><em>"+medicalHistoryObj.oiR+"</em></div></div><div class='row'><div class='col-md-12'><h6>Subjetivo</h6></div><div class='col-md-6'><label>Od</label><em>"+medicalHistoryObj.odS+"</em></div><div class='col-md-6'><label>Oi</label><em>"+medicalHistoryObj.oiS+"</em></div></div><div class='row'><div class='col-md-12'><label>Razón de la consulta: </label><em>"+medicalHistoryObj.prescription+"</em></div></div><div class='row'><div class='col-md-12'><h6>RX final</h6></div><div class='col-md-3'><label>Od Esfera</label><em>"+medicalHistoryObj.OdEsferaRxF+"</em></div><div class='col-md-2'><label>Od Cilindro</label><em>"+medicalHistoryObj.OdCilindroRxF+"</em></div><div class='col-md-2'><label>Od Eje</label><em>"+medicalHistoryObj.OdEjeRxF+"</em></div><div class='col-md-2'><label>Od Av</label><em>"+medicalHistoryObj.OdAvRxF+"</em></div><div class='col-md-3'><label>Od Add</label><em>"+medicalHistoryObj.OdAddRxF+"</em></div><div class='col-md-3'><label>Oi Esfera</label><em>"+medicalHistoryObj.OiEsferaRxF+"</em></div><div class='col-md-2'><label>Oi Cilindro</label><em>"+medicalHistoryObj.OiCilindroRxF+"</em></div><div class='col-md-2'><label>Oi Eje</label><em>"+medicalHistoryObj.OiEjeRxF+"</em></div><div class='col-md-2'><label>Oi Av</label><em>"+medicalHistoryObj.OiAvRxF+"</em></div><div class='col-md-3'><label>Oi Add</label><em>"+medicalHistoryObj.OiAddRxF+"</em></div><div class='col-md-6'><div class='col-md-12'><label>Observación</label><em>"+medicalHistoryObj.observationRxF+"</em></div></div></div></div></div>")
                });
              });
            });
          }
        });
      }
    });

  $('#db').prop('disabled', true);
  $('#mc').prop('disabled', true);
  $('#h1').prop('disabled', true);
  $('#h2').prop('disabled', true);
  $('#h3').prop('disabled', true);
  $('#h4').prop('disabled', true);
  $('#h5').prop('disabled', true);
  $('#h6').prop('disabled', true);
  $('#h7').prop('disabled', true)
</script>