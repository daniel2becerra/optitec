{!!Html::style('assets/css/select2.min.css')!!}
<div class="wizard-navigation">
  <div class="progress-with-circle">
      <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4" style="width: 15%;"></div>
  </div>
  <ul >
    <li>
      <a id="db" href="#datosBasicos" data-toggle="tab" >
        <div class="icon-circle">
          <i class="ti-map"></i>
        </div>
        Información personal
      </a>
    </li>
    <li>
      <a id="mc" href="#motivoConsulta" data-toggle="tab">
        <div class="icon-circle">
          <i class="ti-direction-alt"></i>
        </div>
        Motivo consulta
      </a>
    </li>
    <li>
      <a id="h2" href="#historia2" data-toggle="tab">
        <div class="icon-circle">
          <i class="ti-panel"></i>
        </div>
        RX uso, exámen externo
      </a>
    </li>
    <li>
      <a id="h1" href="#historia1" data-toggle="tab">
        <div class="icon-circle">
          <i class="ti-panel"></i>
        </div>
        Agudeza visual
      </a>
    </li>
    <li>
      <a id="h3" href="#historia3" data-toggle="tab">
        <div class="icon-circle">
          <i class="ti-panel"></i>
        </div>
        Motilidad y fondo ocular
      </a>
    </li>
    <li>
      <a id="h4" href="#historia4" data-toggle="tab">
        <div class="icon-circle">
          <i class="ti-panel"></i>
        </div>
        Queratometría, refracción y Subjetivo
      </a>
    </li>
    <li>
      <a id="h5" href="#historia5" data-toggle="tab">
        <div class="icon-circle">
          <i class="ti-comments"></i>
        </div>
        Fórmula médica
      </a>
    </li>
    <li>
      <a id="h6" href="#historia6" data-toggle="tab">
        <div class="icon-circle">
          <i class="ti-comments"></i>
        </div>
        Diagnóstico
      </a>
    </li>
    <li>
      <a id="h7" href="#historia7" data-toggle="tab">
        <div class="icon-circle">
          <i class="ti-comments"></i>
        </div>
        RX Final
      </a>
    </li>
  </ul>
</div>
<div class="tab-content">
  <div class="tab-pane" id="datosBasicos">
    <div class="row">
        <div class="col-sm-12">
            <h5 class="info-text"> Información personal del paciente</h5>
        </div>
        <input id="_token" type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="row">
        <div class='col-sm-4'>
          {!!Form::label('Documento *: ', null, ['for'=>'identification'])!!}
          <div class="input-group">
            {!!Form::text('identification', null, ['id'=>'identification', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Documento'])!!}
            <span class="input-group-btn">
                <button type="button" id="searchbutton" class="btn btn-primary"><span class="fa fa-search" id="search"></span></button>
            </span>
          </div>
        </div>      
        <div class="col-sm-4">                            
          <div class="form-group">
            {!!Form::label('Nombres *: ', null, ['for'=>'firstname'])!!}
            {!!Form::text('firstname',null, ['id'=>'firstname', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Nombres', 'disabled'])!!}
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            {!!Form::label('Apellidos *: ', null, ['for'=>'lastname'])!!}
            {!!Form::text('lastname',null, ['id'=>'lastname', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Apellidos', 'disabled'])!!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">                            
          <div class="form-group">
            {!!Form::label('Correo *: ', null, [ 'for'=>'email'])!!}
            {!!Form::text('email',null, ['id'=>'email', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Correo', 'disabled'])!!}
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            {!!Form::label('Teléfono *: ', null, [ 'for'=>'phone'])!!}
            {!!Form::text('phone',null, ['id'=>'phone', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Teléfono', 'disabled'])!!}
          </div>
        </div>
        <div class='col-sm-4'>
            {!!Form::label('F. de nacimiento *: ', null, [ 'for'=>'birthday'])!!}
            <div class='input-group date' id='birthdayDiv'>
              {!!Form::text('birthday', null, ['id'=>'birthday', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'DD-MM-YYYY', 'disabled'])!!}
              <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            {!!Form::label('Profesión *: ', null, [ 'for'=>'profession'])!!}
            {!!Form::text('profession',null, ['id'=>'profession', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Profesión', 'disabled'])!!}
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="tab-pane" id="motivoConsulta">
    <div class="row">
        <h5 class="info-text"> Motivo de consulta. </h5>
        <div class="col-sm-10 col-sm-offset-1">
            <div class="form-group">
                <label>Describa el motivo de la consulta </label>
                {!!Form::textarea('reasonconsultation', null, ['class'=>'form-control', 'rows'=>'9', 'required'])!!}
            </div>
        </div>
    </div>
  </div>
  <div class="tab-pane" id="historia1">
    <div class="row">
        <div class="col-sm-12">
            <h5 class="info-text"> Agudeza visual</h5>
      </div>
      <div class="col-sm-3 col-sm-offset-1">                            
        <div class="form-group">
          {!!Form::label('Od sin corrección *: ', null, ['for'=>'odwithoutcorrection'])!!}
          {!!Form::text('odwithoutcorrection',null, ['id'=>'odwithoutcorrection', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Od sin corrección '])!!}
        </div>
      </div> 
      <div class="col-sm-3">                            
        <div class="form-group">
          {!!Form::label('Oi sin corrección *: ', null, ['for'=>'oiwithoutcorrection'])!!}
          {!!Form::text('oiwithoutcorrection',null, ['id'=>'oiwithoutcorrection', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Oi sin corrección '])!!}
        </div>
      </div> 
      <div class="col-sm-3">                            
        <div class="form-group">
          {!!Form::label('Ao sin corrección *: ', null, ['for'=>'aowithoutcorrection'])!!}
          {!!Form::text('aowithoutcorrection',null, ['id'=>'aowithoutcorrection', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'ao sin corrección '])!!}
        </div>
      </div>             
    </div>
  </div>
  <div class="tab-pane" id="historia2">
    <div class="row">
      <div class="col-sm-12">
          <h5 class="info-text"> RX en uso</h5>
      </div>
      <div class="col-sm-5 col-sm-offset-1">                            
        <div class="form-group">
          {!!Form::label('Od *: ', null, ['for'=>'odRx'])!!}
          {!!Form::text('odRx',null, ['id'=>'odRx', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Od'])!!}
        </div>
      </div> 
      <div class="col-sm-5">                            
        <div class="form-group">
          {!!Form::label('Oi*: ', null, ['for'=>'oiRx'])!!}
          {!!Form::text('oiRx',null, ['id'=>'oiRx', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Oi'])!!}
        </div>
      </div> 
      <div class="col-sm-5 col-sm-offset-1">                            
        <div class="form-group">
          {!!Form::label('Add*: ', null, ['for'=>'addRx'])!!}
          {!!Form::text('addRx',null, ['id'=>'addRx', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'add'])!!}
        </div>
      </div>
      <div class="col-sm-5">                            
        <div class="form-group">
          {!!Form::label('Tipo de lente*: ', null, ['for'=>'typeLen'])!!}
          {!!Form::text('typeLen',null, ['id'=>'typeLen', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Tipo de lente'])!!}
        </div>
      </div>
      <div class="col-sm-12">
        <h5 class="info-text"> Examen externo</h5>
      </div>
      <div class="col-sm-5 col-sm-offset-1">                            
        <div class="form-group">
          {!!Form::label('Od *: ', null, ['for'=>'odRx'])!!}
          {!!Form::text('odEE',null, ['id'=>'odEE', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Od'])!!}
        </div>
      </div> 
      <div class="col-sm-5">                            
        <div class="form-group">
          {!!Form::label('Oi*: ', null, ['for'=>'oiRx'])!!}
          {!!Form::text('oiEE',null, ['id'=>'oiEE', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Oi'])!!}
        </div>
      </div>             
    </div>
  </div>
  <div class="tab-pane" id="historia3">
    <div class="row">
      <div class="col-sm-12">
          <h5 class="info-text"> Motilidad Ocular</h5>
      </div>
      <div class="col-sm-5 col-sm-offset-1">                            
        <div class="form-group">
          {!!Form::label('Cover Test *: ', null, ['for'=>'covertest'])!!}
          {!!Form::text('covertest',null, ['id'=>'covertest', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Cover Test '])!!}
        </div>
      </div> 
      <div class="col-sm-5">                            
        <div class="form-group">
          {!!Form::label('PPM *: ', null, ['for'=>'ppm'])!!}
          {!!Form::text('ppm',null, ['id'=>'ppm', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'PPM '])!!}
        </div>
      </div> 
      <div class="col-sm-5 col-sm-offset-1">                            
        <div class="form-group">
          {!!Form::label('Ducciones *: ', null, ['for'=>'ductions'])!!}
          {!!Form::text('ductions',null, ['id'=>'ductions', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Ducciones '])!!}
        </div>
      </div> 
      <div class="col-sm-5">                            
        <div class="form-group">
          {!!Form::label('Versiones *: ', null, ['for'=>'versions'])!!}
          {!!Form::text('versions',null, ['id'=>'versions', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Versiones '])!!}
        </div>
      </div>
      <div class="col-sm-12">
          <h5 class="info-text"> Fondo del ojo</h5>
      </div>
      <div class="col-sm-5 col-sm-offset-1">                            
        <div class="form-group">
          {!!Form::label('Od *: ', null, ['for'=>'odfondo'])!!}
          {!!Form::text('odfondo',null, ['id'=>'odfondo', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Odfondo '])!!}
        </div>
      </div> 
      <div class="col-sm-5">                            
        <div class="form-group">
          {!!Form::label('Oi *: ', null, ['for'=>'oifondo'])!!}
          {!!Form::text('oifondo', null, ['id'=>'oifondo', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Oi '])!!}
        </div>
      </div>              
    </div>
  </div>
  <div class="tab-pane" id="historia4">
    <div class="row">
      <div class="col-sm-12">
        <h5 class="info-text"> Queratometría</h5>
      </div>
      <div class="col-sm-5 col-sm-offset-1">                            
        <div class="form-group">
          {!!Form::label('Od *: ', null, ['for'=>'odQ'])!!}
          {!!Form::text('odQ',null, ['id'=>'odQ', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Od'])!!}
        </div>
      </div> 
      <div class="col-sm-5">                            
        <div class="form-group">
          {!!Form::label('Oi *: ', null, ['for'=>'oiQ'])!!}
          {!!Form::text('oiQ',null, ['id'=>'oiQ', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Oi '])!!}
        </div>
      </div>
      <div class="col-sm-12">
        <h5 class="info-text"> Refracción</h5>
      </div>
      <div class="col-sm-5 col-sm-offset-1">                            
        <div class="form-group">
          {!!Form::label('Od *: ', null, ['for'=>'odR'])!!}
          {!!Form::text('odR',null, ['id'=>'odR', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Od'])!!}
        </div>
      </div> 
      <div class="col-sm-5">                            
        <div class="form-group">
          {!!Form::label('Oi *: ', null, ['for'=>'oiR'])!!}
          {!!Form::text('oiR',null, ['id'=>'oiR', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Oi '])!!}
        </div>
      </div>
      <div class="col-sm-12">
        <h5 class="info-text"> Subjetivo</h5>
      </div>
      <div class="col-sm-5 col-sm-offset-1">                            
        <div class="form-group">
          {!!Form::label('Od *: ', null, ['for'=>'odS'])!!}
          {!!Form::text('odS',null, ['id'=>'odS', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Od'])!!}
        </div>
      </div> 
      <div class="col-sm-5">                            
        <div class="form-group">
          {!!Form::label('Oi *: ', null, ['for'=>'oiS'])!!}
          {!!Form::text('oiS',null, ['id'=>'oiS', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Oi '])!!}
        </div>
      </div>                    
    </div>
  </div>
  <div class="tab-pane" id="historia5">
    <div class="row">
        <h5 class="info-text"> Fórmula médica. </h5>
        <div class="col-sm-10 col-sm-offset-1">
            <div class="form-group">
                <label>Describa la fórmula médica </label>
                {!!Form::textarea('prescription', null, ['class'=>'form-control', 'rows'=>'9'])!!}
            </div>
        </div>
    </div>
  </div>
  <div class="tab-pane" id="historia6">
    <div class="row">
      <div class="col-md-2">
          <h5 class="info-text"> Diagnóstico</h5>
      </div>
      <div class="col-md-12">                            
        <div class="form-group">
          {!!Form::label('Diagnostico *: ', null, ['for'=>'diagnostico'])!!}
          {!!Form::select('diagnosticos[]', $cie10s, null, ['id'=>'diagnostico', 'class'=> 'col-md-12 diag selectClass', 'required', 'multiple'=>'multiple'])!!}
        </div>
      </div>              
    </div>
  </div>
  <div class="tab-pane" id="historia7">
    <div class="row">
      <div class="col-sm-12">
          <h5 class="info-text"> RX Final</h5>
      </div>
      <div class="col-sm-2 col-sm-offset-1">                            
        <div class="form-group">
          {!!Form::label('Od Esfera*: ', null, ['for'=>'odEsferaRxF'])!!}
          {!!Form::text('OdEsferaRxF',null, ['id'=>'odEsferaRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Od Esfera '])!!}
        </div>
      </div> 
      <div class="col-sm-2">                            
        <div class="form-group">
          {!!Form::label('Od Cilindro*: ', null, ['for'=>'OdCilindroRxF'])!!}
          {!!Form::text('OdCilindroRxF',null, ['id'=>'OdCilindroRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Od Cilindro '])!!}
        </div>
      </div> 
      <div class="col-sm-2">                            
        <div class="form-group">
          {!!Form::label('Od Eje*: ', null, ['for'=>'OdEjeRxF'])!!}
          {!!Form::text('OdEjeRxF',null, ['id'=>'OdEjeRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Od Eje '])!!}
        </div>
      </div> 
      <div class="col-sm-2">                            
        <div class="form-group">
          {!!Form::label('Od av.*: ', null, ['for'=>'OdAvRxF'])!!}
          {!!Form::text('OdAvRxF',null, ['id'=> 'OdAvRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'av. Od '])!!}
        </div>
      </div> 
      <div class="col-sm-2">                            
        <div class="form-group">
          {!!Form::label('Od add *: ', null, ['for'=>'OdAddRxF'])!!}
          {!!Form::text('OdAddRxF',null, ['id'=>'OdAddRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Od add '])!!}
        </div>
      </div>
      <div class="col-sm-2 col-sm-offset-1">                            
        <div class="form-group">
          {!!Form::label('Oi Esfera*: ', null, ['for'=>'OiEsferaRxF'])!!}
          {!!Form::text('OiEsferaRxF',null, ['id'=>'OiEsferaRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Oi Esfera '])!!}
        </div>
      </div>      
      <div class="col-sm-2">                            
        <div class="form-group">
          {!!Form::label('Oi Cilindro*: ', null, ['for'=>'OiCilindroRxF'])!!}
          {!!Form::text('OiCilindroRxF',null, ['id'=>'OiCilindroRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Oi Cilindro '])!!}
        </div>
      </div>      
      <div class="col-sm-2">                            
        <div class="form-group">
          {!!Form::label('Oi Eje*: ', null, ['for'=>'OiEjeRxF'])!!}
          {!!Form::text('OiEjeRxF',null, ['id'=>'OiEjeRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Oi Eje '])!!}
        </div>
      </div>      
      <div class="col-sm-2">                            
        <div class="form-group">
          {!!Form::label('Oi av.*: ', null, ['for'=>'OiAvRxF'])!!}
          {!!Form::text('OiAvRxF',null, ['id'=>'OiAvRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Oi av.'])!!}
        </div>
      </div> 
      <div class="col-sm-2">                            
        <div class="form-group">
          {!!Form::label('Oi add *: ', null, ['for'=>'OiAddRxF'])!!}
          {!!Form::text('OiAddRxF',null, ['id'=>'OiAddRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Oi add '])!!}
        </div>
      </div>
      <div class="col-sm-5 col-sm-offset-1">                            
        <div class="form-group">
          {!!Form::label('DP *: ', null, ['for'=>'DpRxF'])!!}
          {!!Form::text('DpRxF',null, ['id'=>'DpRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'DP '])!!}
        </div>
      </div>
      <div class="col-sm-5">                            
        <div class="form-group">
          {!!Form::label('Uso *: ', null, ['for'=>'UsoRxF'])!!}
          {!!Form::text('UsoRxF',null, ['id'=>'UsoRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Uso '])!!}
        </div>
      </div>
      <div class="col-sm-10 col-sm-offset-1">                            
        <div class="form-group">
          {!!Form::label('Observación *: ', null, ['for'=>'observationRxF'])!!}
          {!!Form::textarea('observationRxF',null, ['id'=>'observationRxF', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'rows'=>'2', 'placeholder'=>'Observación'])!!}
        </div>
      </div>         
    </div>
  </div>
  <div class="tab-pane" id="facilities">
    <h5 class="info-text">Tell us more about facilities. </h5>
    <div class="row">
      <div class="col-sm-5 col-sm-offset-1">
        <div class="form-group">
          <label>Your place is good for</label>
          <select class="form-control">
            <option disabled="" selected="">- type -</option>
            <option>Business</option>
            <option>Vacation </option>
            <option>Work</option>
          </select>
        </div>
      </div>
      <div class="col-sm-5">
        <div class="form-group">
            <label>Is air conditioning included ?</label>
            <select class="form-control">
                <option disabled="" selected="">- response -</option>
                <option>Yes</option>
                <option>No </option>
            </select>
        </div>
      </div>
      <div class="col-sm-5 col-sm-offset-1">
        <div class="form-group">
            <label>Does your place have wi-fi?</label>
            <select class="form-control">
                <option disabled="" selected="">- response -</option>
                <option>Yes</option>
                <option>No </option>
            </select>
        </div>
      </div>
      <div class="col-sm-5">
        <div class="form-group">
            <label>Is breakfast included?</label>
            <select class="form-control">
                <option disabled="" selected="">- response -</option>
                <option>Yes</option>
                <option>No </option>
            </select>
        </div>
      </div>
    </div>
  </div>
  <div class="tab-pane" id="type">
    <h5 class="info-text">What type of location do you have? </h5>
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2">
          <div class="col-sm-4 col-sm-offset-2">
            <div class="choice" data-toggle="wizard-checkbox">
              <input type="checkbox" name="jobb" value="Design">
              <div class="card card-checkboxes card-hover-effect">
                <i class="ti-home"></i>
                <p>Home</p>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="choice" data-toggle="wizard-checkbox">
              <input type="checkbox" name="jobb" value="Design">
              <div class="card card-checkboxes card-hover-effect">
                <i class="ti-package"></i>
                <p>Apartment</p>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
<div class="wizard-footer">
  <div class="pull-right">
    <input id="next" type='button' class='btn btn-next btn-fill btn-primary btn-wd' name='next' value='Siguiente' disabled />
    {!!Form::submit('Finalizar',['id'=>'finish', 'class'=>'btn btn-finish btn-fill btn-success btn-wd'])!!}
  </div>
  <div class="pull-left">
      <input type='button' class='btn btn-previous btn-default btn-wd' name='previous' value='Atrás' />
  </div>
  <div class="clearfix"></div>
</div>
{!!Html::script('assets/js/select2.min.js')!!}
<script type="text/javascript">
  $('#lab_id').on('change', function(e)
    {
      $('#typelen_id').empty();
      $('#materiallen_id').empty();
      $('#marklen_id').empty();
      $('#filterlen_id').empty();
      var lab_id = e.target.value;
      $.get('/JsonTypelen?lab_id='+lab_id, function(data)
      {
        $('#typelen_id').append($('<option>', {
              text: "Seleccione una opción"
          }));
        $('#materiallen_id').append($('<option>', {
              text: "Seleccione una opción"
          }));
        $('#marklen_id').append($('<option>', {
              text: "Seleccione una opción"
          }));
        $('#filterlen_id').append($('<option>', {
              text: "Seleccione una opción"
          }));
        $.each(data, function(index, tlObj)  
        {
          $('#typelen_id').append($('<option>', {
              value: index,
              text: tlObj
          }));
        });
      });
      $.get('/JsonFilterlenLab?lab_id='+lab_id, function(data)
      {
        $.each(data, function(index, flObj)  
        {
          $('#filterlen_id').append($('<option>', {
              value: index,
              text: flObj
          }));
        });
      });
    });

  $('#typelen_id').on('change', function(e)
    {
      $('#materiallen_id').empty();
      $('#marklen_id').empty();
      var typelen_id = e.target.value;
      $.get('/JsonMateriallen?typelen_id='+typelen_id, function(data)
      {
        $('#materiallen_id').append($('<option>', {
              text: "Seleccione una opción"
          }));
        $.each(data, function(index, mtlObj)  
        {
          $('#materiallen_id').append($('<option>', {
              value: index,
              text: mtlObj
          }));
        });
      });

      $.get('/JsonMarklen?typelen_id='+typelen_id, function(data)
      {
        $('#marklen_id').append($('<option>', {
              text: "Seleccione una opción"
          }));
        $.each(data, function(index, mclObj)  
        {
          $('#marklen_id').append($('<option>', {
              value: index,
              text: mclObj
          }));
        });
      });
    });

  $(document).on('click', 'form button[type=submit]', function(e) {
  
  });

$("#frm").submit(function (event) {
    event.preventDefault();
    $('.loading').show();
    var form = $(this);
    var data = new FormData($(this)[0]);
    var url = form.attr("action");
    var form = document.getElementById('frm');
      
      // Si el form no es valido
      if (!form.checkValidity()) {
        console.log('Nop');
        return false;
      }
      
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          async: false,
          cache: false,
          contentType: false,
          processData: false,
          success: function (data) {
              if (data.fail) {
                  $('#frm input.required, #frm textarea.required').each(function () {
                      index = $(this).attr('name');
                      if (index in data.errors) {
                          $("#form-" + index + "-error").addClass("has-error");
                          $("#" + index + "-error").html(data.errors[index]);
                      }
                      else {
                          $("#form-" + index + "-error").removeClass("has-error");
                          $("#" + index + "-error").empty();
                      }
                  });
                  $('#focus').focus().select();
              } else {
                  $(".has-error").removeClass("has-error");
                  $(".help-block").empty();
                  $('.loading').hide();
                  swal({
                  title: '<i>Historia clínica guardada</i><br> Ahora puede imprimir: ',
                  type: 'info',
                  html: '<a href="/Factura/lentes?id='+data.id+'" class="btn btn-info" target="_blank"><i class="fa fa-file-pdf-o"></i> RX Final</a>' +
                        '<a href="/Factura/recomendaciones?id='+data.id+'" class="btn btn-info" target="_blank"><i class="fa fa-file-pdf-o"></i> Recomendaciones</a>',
                  allowOutsideClick: false,
                  confirmButtonText:
                    '<i class="fa fa-thumbs-up"></i> OK',
                }).then(function (result) {
                  if (result.value) {
                    location.reload();
                  }
                })
              }
          },
          error: function (xhr, textStatus, errorThrown) {
              alert(errorThrown);
          }
      });
    
    return false;
});
  $(document).ready(function()
  {
      $('.selectClass').select2();
  });

  $(function ()
  {
    $('#birthdayDiv').datetimepicker({
        viewMode: 'years',
      format: 'DD-MM-YYYY',
    });
  });
  function pulsar(e) { 
  tecla = (document.all) ? e.keyCode :e.which; 
  return (tecla!=13); 
  } 
  $('#searchbutton').click(function(e) 
    {
      $('#searchbutton').prop('disabled', true);
      $("#accordion").empty();
      var id = $('#identification').val();
      if(id.length > 0)
      {
        $('#identification').prop('readonly', true);
        $.get('/JsonCliente?identification='+id, function(data)
        {
          if(data.length == 0)
          {
            $('#firstname').prop('disabled', false);
            $('#lastname').prop('disabled', false);
            $('#email').prop('disabled', false);
            $('#phone').prop('disabled', false);
            $('#birthday').prop('disabled', false);
            $('#profession').prop('disabled', false);
            $('#next').prop('disabled', false);
          }
          else
          {
            $.each(data, function(index, clientObj)
            {
              $('#next').prop('disabled', false);
              $('#email').prop('disabled', false);
              $('#phone').prop('disabled', false);
              $('#firstname').val(clientObj.firstname);
              $('#lastname').val(clientObj.lastname);
              $('#email').val(clientObj.email);
              $('#phone').val(clientObj.phone);
              $('#birthday').val(clientObj.birthday);
              $('#profession').val(clientObj.profession);
              $('#identification').prop('readonly', true);
              $('#history').prop('hidden', false);

              $.get('/JsonMedicalHistory?client_id='+clientObj.id, function(data)
              {
                $.each(data, function(index, medicalHistoryObj)
                {
                  if (index == 0)
                    {
                      var collapsein = "in";
                    }
                  else
                    {
                      var collapsein = "";
                    }
                  $('#accordion').append("<div class='panel panel-default'><div class='panel-heading' role='tab' id='heading"+index+"'><h4 class='panel-title'><a role='button' data-toggle='collapse' data-parent='#accordion' href='#collapse"+index+"' aria-expanded='true' aria-controls='collapse"+index+"'>"+medicalHistoryObj.created_at+"</a></h4></div><div id='collapse"+index+"' class='panel-collapse collapse' role='tabpanel' aria-labelledby='heading"+index+"'><div class='panel-body'><div class='row'><div class='col-md-12'><label>Razón de la consulta: </label><em>"+medicalHistoryObj.reasonconsultation+"</em></div></div><div class='row'><div class='col-md-12'><h6>Agudeza visual</h6></div><div class='col-md-4'><label>Od sin corrección</label><em>"+medicalHistoryObj.odwithoutcorrection+"</em></div><div class='col-md-4'><label>Oi sin corrección</label><em>"+medicalHistoryObj.oiwithoutcorrection+"</em></div><div class='col-md-4'><label>Ao sin corrección</label><em>"+medicalHistoryObj.aowithoutcorrection+"</em></div></div><div class='row'><div class='col-md-12'><h6>RX en uso</h6></div><div class='col-md-3'><label>Od</label><em>"+medicalHistoryObj.odRx+"</em></div><div class='col-md-3'><label>Oi</label><em>"+medicalHistoryObj.oiRx+"</em></div><div class='col-md-3'><label>Add</label><em>"+medicalHistoryObj.addRx+"</em></div><div class='col-md-3'><label>Tipo de lente</label><em>"+medicalHistoryObj.typeLen+"</em></div></div><div class='row'><div class='col-md-12'><h6>Examen externo</h6></div><div class='col-md-6'><label>Od</label><em>"+medicalHistoryObj.odEE+"</em></div><div class='col-md-6'><label>Oi</label><em>"+medicalHistoryObj.oiEE+"</em></div></div><div class='row'><div class='col-md-12'><h6>Motilidad ocular</h6></div><div class='col-md-3'><label>Cover test</label><em>"+medicalHistoryObj.covertest+"</em></div><div class='col-md-3'><label>PPM</label><em>"+medicalHistoryObj.ppm+"</em></div><div class='col-md-3'><label>Ducciones</label><em>"+medicalHistoryObj.ductions+"</em></div><div class='col-md-3'><label>Versiones</label><em>"+medicalHistoryObj.versions+"</em></div></div><div class='row'><div class='col-md-12'><h6>Fondo de ojo</h6></div><div class='col-md-6'><label>Od</label><em>"+medicalHistoryObj.odfondo+"</em></div><div class='col-md-6'><label>Oi</label><em>"+medicalHistoryObj.oifondo+"</em></div></div><div class='row'><div class='col-md-12'><h6>Queratometría</h6></div><div class='col-md-6'><label>Od</label><em>"+medicalHistoryObj.odQ+"</em></div><div class='col-md-6'><label>Oi</label><em>"+medicalHistoryObj.oiQ+"</em></div></div><div class='row'><div class='col-md-12'><h6>Refracción</h6></div><div class='col-md-6'><label>Od</label><em>"+medicalHistoryObj.odR+"</em></div><div class='col-md-6'><label>Oi</label><em>"+medicalHistoryObj.oiR+"</em></div></div><div class='row'><div class='col-md-12'><h6>Subjetivo</h6></div><div class='col-md-6'><label>Od</label><em>"+medicalHistoryObj.odS+"</em></div><div class='col-md-6'><label>Oi</label><em>"+medicalHistoryObj.oiS+"</em></div></div><div class='row'><div class='col-md-12'><label>Razón de la consulta: </label><em>"+medicalHistoryObj.prescription+"</em></div></div><div class='row'><div class='col-md-12'><h6>RX final</h6></div><div class='col-md-3'><label>Od Esfera</label><em>"+medicalHistoryObj.OdEsferaRxF+"</em></div><div class='col-md-2'><label>Od Cilindro</label><em>"+medicalHistoryObj.OdCilindroRxF+"</em></div><div class='col-md-2'><label>Od Eje</label><em>"+medicalHistoryObj.OdEjeRxF+"</em></div><div class='col-md-2'><label>Od Av</label><em>"+medicalHistoryObj.OdAvRxF+"</em></div><div class='col-md-3'><label>Od Add</label><em>"+medicalHistoryObj.OdAddRxF+"</em></div><div class='col-md-3'><label>Oi Esfera</label><em>"+medicalHistoryObj.OiEsferaRxF+"</em></div><div class='col-md-2'><label>Oi Cilindro</label><em>"+medicalHistoryObj.OiCilindroRxF+"</em></div><div class='col-md-2'><label>Oi Eje</label><em>"+medicalHistoryObj.OiEjeRxF+"</em></div><div class='col-md-2'><label>Oi Av</label><em>"+medicalHistoryObj.OiAvRxF+"</em></div><div class='col-md-3'><label>Oi Add</label><em>"+medicalHistoryObj.OiAddRxF+"</em></div><div class='col-md-6'><div class='col-md-12'><label>Observación</label><em>"+medicalHistoryObj.observationRxF+"</em></div></div></div></div></div>")
                });
              });
            });
          }
        });
      }
    });

  $('#db').prop('disabled', true);
  $('#mc').prop('disabled', true);
  $('#h1').prop('disabled', true);
  $('#h2').prop('disabled', true);
  $('#h3').prop('disabled', true);
  $('#h4').prop('disabled', true);
  $('#h5').prop('disabled', true);
  $('#h6').prop('disabled', true);
  $('#h7').prop('disabled', true)
</script>