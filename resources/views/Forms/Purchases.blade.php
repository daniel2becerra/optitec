<div class="row">     
  <div class="col-sm-4">                            
    <div class="form-group">
      {!!Form::label('Número de factura *: ', null, ['for'=>'invoicepurchase'])!!}
      {!!Form::text('invoicepurchase',null, ['id'=>'invoicepurchase', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Número de factura'])!!}
    </div>
  </div>
  <div class="col-sm-4">
    <div class="form-group">
      {!!Form::label('Vendedor *: ', null, ['for'=>'seller'])!!}
      {!!Form::text('seller',null, ['id'=>'seller', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Vendedor'])!!}
    </div>
  </div>
</div>
<div class="ln_solid"></div>

<div class="row">
  <div class="form-group">
    <div class="col-md-4">
      {!!Form::select('product_idInput', $products, null, ['id'=>'product_idInput', 'class'=> 'selectClass', 'placeholder'=>'Seleccione una opción'])!!}
    </div>
    <div class="col-md-2">
      {!!Form::number("purchasePriceInput",null, ["id"=>"purchasePriceInput", "class"=>"form-control", "placeholder"=>"Precio de compra", "onkeypress"=>"return isNumberKey(event)"])!!}
    </div>
    <div class="col-md-2">
      {!!Form::number("salePriceInput",null, ["id"=>"salePriceInput", "class"=>"form-control", "placeholder"=>"Precio de venta", "onkeypress"=>"return isNumberKey(event)"])!!}
    </div>
    <div class="col-md-2">
      {!!Form::number("quantityInput",null, ["id"=>"quantityInput", "class"=>"form-control", "placeholder"=>"Cantidad", "onkeypress"=>"return isNumberKey(event)"])!!}
    </div>
    <button id="addproduct" type="button" class="btn btn-primary">Agregar</button>
  </div>
</div>
<div class="row">
  <div class="x_content">
    <table id="datatable" class="table table-hover">
      <thead>
        <th></th>
        <th>Producto</th>
        <th></th>
        <th>Precio de compra</th>
        <th></th>
        <th>Precio de venta</th>
        <th></th>
        <th>Cantidad</th>
        <th>Total</th>
        <th>Operación</th>
      </thead>
      <tbody>
        <?php $totalfinal = 0; ?>
        @foreach($purchaseproducts as $purchaseproduct)
          <tr>
            <td><input type="hidden" name="product_id[]" id="product_id" value="{{$purchaseproduct->product_id}}"></td>
            <td>{{$purchaseproduct->product->name.'-'.$purchaseproduct->product->reference.'-'.$purchaseproduct->product->color}}</td>
            <td><input type="hidden" name="purchasePrice[]" value="{{$purchaseproduct->purchasePrice}}"></td>
            <td>{{$purchaseproduct->purchasePrice}}</td>
            <td><input type="hidden" name="salePrice[]" value="{{$purchaseproduct->salePrice}}"></td>
            <td>{{$purchaseproduct->salePrice}}</td>
            <td><input type="hidden" name="quantity[]" value="{{$purchaseproduct->quantity}}"></td>
            <td>{{$purchaseproduct->quantity}}</td>
            <td>{{$purchaseproduct->purchasePrice * $purchaseproduct->quantity}}</td>
            <td><input type="button" class="btn btn-primary borrar" value="Eliminar" /></td>
          </tr>
          <?php $totalfinal += $purchaseproduct->purchasePrice * $purchaseproduct->quantity ?>         
          @endforeach 
      </tbody>          
    </table>
     <div align="right">
      {!!Form::label('Total : ', null)!!}
       {!!Form::label('0', $totalfinal, ['id' => 'total'])!!}
     </div>
  </div>
</div>
<div class="ln_solid"></div>
<script type="text/javascript">
  function isNumberKey(evt){
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;
      return true;
  }
  $(document).on('click', '.borrar', function (event) {
    event.preventDefault();
    $(this).closest('tr').remove();
    var totalproducto = $(this).parents("tr").find("td").eq(8).html();
    totalfinal = $('#total').text();
    totalfinal -= totalproducto;
    $('#total').text(totalfinal);
  });
  $('#addproduct').click(function(e) 
  {
    var product_idInput = $('#product_idInput').val();
    var product_name = $( "#product_idInput option:selected" ).text();
    var purchasePriceInput = $('#purchasePriceInput').val();
    var salePriceInput = $('#salePriceInput').val();
    var quantityInput = $('#quantityInput').val();
    if(product_name == null || product_name == "" || purchasePriceInput == null || purchasePriceInput == "" || salePriceInput == null || salePriceInput == "" || quantityInput == null || quantityInput == "" )
    {
      swal({
        type: 'error',
        title: 'Oops...',
        text: 'Algunos campos estan vacíos, revísalos!',
      })
    }
    else
    {
      var total = purchasePriceInput*quantityInput;
      $('#purchasePriceInput').val("");
      $('#salePriceInput').val("");
      $('#quantityInput').val("");
      totalfinal = parseInt($('#total').text());
      totalfinal += total;
      $('#total').text(totalfinal);
      $('#datatable > tbody').append('<tr><td><input type="hidden" name="product_id[]" id="product_id" value="'+product_idInput+'"></td><td>'+product_name+'</td><td><input type="hidden" name="purchasePrice[]" value="'+purchasePriceInput+'"></td><td>'+purchasePriceInput+'</td><td><input type="hidden" name="salePrice[]" value="'+salePriceInput+'"></td><td>'+salePriceInput+'</td><td><input type="hidden" name="quantity[]" value="'+quantityInput+'"></td><td>'+quantityInput+'</td><td>'+total+'</td><td><input type="button" class="btn btn-primary borrar" value="Eliminar" /></td><tr>');
    }
  });
</script>
