<div class="tab-pane" id="datosBasicos">
  {!!Form::open(['route'=>'Clients.quickstore', 'method'=>'POST', 'autocomplete'=>'off', 'class'=>'form-horizontal form-label-left', 'id'=>'quickstore'])!!}
    <div class="row">
      <div class="row">
        <div class='col-sm-4'>
          {!!Form::label('Documento *: ', null, ['for'=>'identification'])!!}
          <div class="input-group">
            {!!Form::text('identificationinit', $Client->identification, ['id'=>'identificationinit', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Documento'])!!}
            <span class="input-group-btn">
                <button type="button" id="searchbutton" class="btn btn-primary"><span class="fa fa-search" id="search"></span></button>
            </span>
          </div>
        </div>      
        <div class="col-sm-4">                            
          <div class="form-group">
            {!!Form::label('Nombres *: ', null, ['for'=>'firstname'])!!}
            {!!Form::text('firstname',null, ['id'=>'firstname', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Nombres', 'required', 'disabled'])!!}
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            {!!Form::label('Apellidos *: ', null, ['for'=>'lastname'])!!}
            {!!Form::text('lastname', null, ['id'=>'lastname', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Apellidos', 'required', 'disabled'])!!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">                            
          <div class="form-group">
            {!!Form::label('Correo *: ', null, [ 'for'=>'email'])!!}
            {!!Form::text('email', null, ['id'=>'email', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Correo', 'disabled'])!!}
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            {!!Form::label('Teléfono *: ', null, [ 'for'=>'phone'])!!}
            {!!Form::text('phone',null, ['id'=>'phone', 'class'=>'form-control col-md-7 col-xs-12', 'required', 'placeholder'=>'Teléfono', 'required', 'disabled'])!!}
          </div>
        </div>
        <div class='col-sm-4'>
            {!!Form::label('F. de nacimiento *: ', null, [ 'for'=>'birthday'])!!}
            <div class='input-group date' id='birthdayDiv'>
              {!!Form::text('birthday', null, ['id'=>'birthday', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'YYYY-MM-DD', 'disabled'])!!}
              <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            {!!Form::label('Profesión *: ', null, [ 'for'=>'profession'])!!}
            {!!Form::text('profession',null, ['id'=>'profession', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Profesión', 'disabled'])!!}
          </div>
        </div>
      </div>
    </div>
  {!!Form::close()!!}
  {!!Form::open(['route'=>'Invoices.store', 'method'=>'POST', 'autocomplete'=>'off', 'class'=>'form-horizontal form-label-left', 'id'=>'Facturar'])!!}
    {!!Form::hidden('identification', null, ['id'=>'identification'])!!}
    {!!Form::hidden('order_id', $Order->id, ['id'=>'order_idinvoice'])!!}
  {!!Form::hidden('value', null, ['id'=>'value'])!!}
  {!!Form::close()!!}
</div>
<div class="">
  <div class="page-title">
    <div class="title_left">
      <h3>Pedidos</h3>
    </div>
  </div>
    <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Pedido <small>Plantilla de creación de pedidos</small></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">Productos</div>
                <div class="panel-body">
                  {!!Form::open(['route'=>'Orderproducts.store', 'method'=>'POST', 'autocomplete'=>'off', 'class'=>'form-horizontal form-label-left', 'id'=>'frmproducts'])!!}
                      <div class="form-group">
                          <div class="col-md-6">
                              <?php $dis = ($Order->order_id == null) ? '' : 'disabled';?>
                              {!!Form::select('product_id', $products, null, ['id'=>'product_id', 'class'=> 'selectClass', 'placeholder'=>'Seleccione una opción' , $dis])!!}
                              {!!Form::hidden('order_id', $Order->id, ['id'=>'order_idproduct'])!!}
                              {!!Form::hidden('typeproduct_id', 3, ['id'=>'typeproduct_id'])!!}
                          </div>
                          <div class="col-md-2">
                              {!!Form::submit('Agregar',['id'=>'submitproduct', 'class'=>'btn btn-finish btn-fill btn-primary btn-wd', $dis])!!}
                          </div>
                  {!!Form::close()!!}
                  {!!Form::open(['route'=>'Orderproducts.store', 'method'=>'POST', 'autocomplete'=>'off', 'class'=>'form-horizontal form-label-left', 'id'=>'frmdiscount'])!!}
                          <div class="col-md-2">
                              {!!Form::text('price', null, ['id'=>'pricediscount', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Descuento', $dis])!!}
                              {!!Form::hidden('product_id', 1, ['id'=>'product_id'])!!}
                              {!!Form::hidden('order_id', $Order->id, ['id'=>'order_iddiscount'])!!}
                              {!!Form::hidden('typeproduct_id', 4, ['id'=>'typeproduct_id'])!!}
                          </div>
                          <div class="col-md-2">
                              {!!Form::submit('Agregar',['id'=>'submitdiscount', 'class'=>'btn btn-finish btn-fill btn-primary btn-wd', $dis])!!}
                          </div>
                      </div>
                  {!!Form::close()!!}
              </div>
                <table id="tableproducts" class="table table-bordered table-hover">
                  <thead>
                    <th>Fecha</th>
                    <th>Nombre</th>
                    <th>Valor</th>  
                    <th>Borrar</th>
                  </thead>
                  <?php $totalproductos = 0; ?>
                  <tbody>
                    @foreach($Orderproducts as $Orderproduct)
                      <tr id="product{{$Orderproduct->id}}">
                        <td>{{substr($Orderproduct->created_at, 0, 10)}}</td>
                        <td>{{$Orderproduct->productname}}</td>
                        <td>{{$Orderproduct->price}}</td>
                        <td><input type="button" value="Eliminar" class="btn btn-danger" onclick="return dltrw({{$Orderproduct->id}}, '/Orderproducts/', 'product')" ;=""></td>
                      </tr>
                      <?php $totalproductos += $Orderproduct->price; ?>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <div class="form-group">
                {!!Form::label('TOTAL PRODUCTOS: ', null, ['class'=>'control-label col-md-4 col-sm-3 col-xs-12', 'for'=>'name'])!!}
                <div>
                  {!!Form::label($totalproductos, $totalproductos, ['id'=>'totalproductos'])!!}
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">Abonos</div>
                <div class="panel-body">
                  {!!Form::open(['route'=>'Payments.store', 'method'=>'POST', 'autocomplete'=>'off', 'class'=>'form-horizontal form-label-left', 'id'=>'frmpayments'])!!}
                  <div class="form-group">
                    <div class="col-md-4">
                      {!!Form::text('price',null, ['id'=>'pricepayment', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Abono', $dis])!!}
                      {!!Form::hidden('order_id', $Order->id, ['id'=>'order_idpayment'])!!}
                    </div>
                    {!!Form::submit('Agregar',['id'=>'submitpayment', 'class'=>'btn btn-finish btn-fill btn-primary btn-wd', $dis])!!}
                  {!!Form::close()!!}
                  </div>
                </div>
                <table id="tablepayments" class="table table-bordered table-hover">
                  <?php $totalabonos = 0; ?>
                  <thead>
                    <th>Fecha</th>
                    <th>Valor</th>  
                    <th>Ver</th>
                    <th>Eliminar</th>
                  </thead>
                  <tbody>
                    @foreach($Payments as $Payment)
                      <tr id="payment{{$Payment->id}}">
                        <td>{{substr($Payment->created_at, 0, 10)}}</td>
                        <td>{{$Payment->price}}</td>
                        <td>
                          <a class="btn btn-primary" href="/Factura/abono?payment_id={!!$Payment->id!!}&order_id={!!$Payment->order_id!!}" role="button" target="_blank"><i class="fa fa-eye">
                        </td>
                        <td><input type="button" value="Eliminar" class="btn btn-danger" onclick="return dltrw({{$Payment->id}}, '/Payments/', 'payment')" ;=""></td>
                      </tr>
                      <?php $totalabonos += $Payment->price; ?>
                    @endforeach
                  </tbody>  
                </table>
              </div>
              <div class="form-group">
                {!!Form::label('TOTAL ABONOS: ', null, ['class'=>'control-label col-md-4 col-sm-3 col-xs-12', 'for'=>'name'])!!}
                <div>
                  {!!Form::label($totalabonos, $totalabonos, ['id'=>'totalabonos'])!!}
                </div>
              </div>
              <div class="form-group">
                {!!Form::label('SALDO PENDIENTE: ', null, ['class'=>'control-label col-md-4 col-sm-3 col-xs-12', 'for'=>'name'])!!}
                <div>
                  {!!Form::label( $totalproductos - $totalabonos, null, ['id'=>'pendiente'])!!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <button id="btnFacturar" class="btn btn-primary">Facturar</button>
  </div> 
</div>


<div class="ln_solid"></div>
<script>
  $(function() 
    {
      $('#searchbutton').trigger('click');
    });
  $( "#btnFacturar" ).click(function() {
    $( "#Facturar" ).submit();
  });
  $('#searchbutton').click(function(e) 
    {
      $('#searchbutton').prop('disabled', true);
      var id = $('#identificationinit').val();
      if(id.length > 0)
      {
        $('#identificationinit').prop('readonly', true);
        $.get('/JsonCliente?identification='+id, function(data)
        {
          if(data.length == 0)
          {
            $('#firstname').prop('disabled', false);
            $('#lastname').prop('disabled', false);
            $('#email').prop('disabled', false);
            $('#phone').prop('disabled', false);
            $('#birthday').prop('disabled', false);
            $('#profession').prop('disabled', false);
            $('#identification').val($('#identificationinit').val());
          }
          else
          {
            $('#client_identification').val(id);
            $.each(data, function(index, clientObj)
            {
              $('#email').prop('disabled', true);
              $('#phone').prop('disabled', true);
              $('#firstname').val(clientObj.firstname);
              $('#lastname').val(clientObj.lastname);
              $('#email').val(clientObj.email);
              $('#phone').val(clientObj.phone);
              $('#birthday').val(clientObj.birthday);
              $('#profession').val(clientObj.profession);
              $('#identificationinit').prop('readonly', true);
            });
          }
        });
      }
    });


  $(document).ready(function()
    {
        $('.selectClass').select2();
        $('#value').val(parseInt($('#totalproductos').text()));
    });
      $("#Facturar").submit(function (event){
            event.preventDefault();
          $('.loading').show();
          var form = $(this);
          var data = new FormData($(this)[0]);
          var url = form.attr("action");
          var tp = parseInt($('#totalproductos').text());
          var ta = parseInt($('#totalabonos').text());

          var firstname = $('#firstname').val();
          var lastname = $('#lastname').val();
          var phone = $('#phone').val();
          var identification = $('#identification').val();
          if(tp==0)
          {
            swal({
            type: 'error',
            title: 'Oops...',
            text: 'No tiene artículos!',
          })
          }
          else if ((tp-ta)==0)
          {
            $.ajax({
            type: "POST",
              url: url,
              data: data,
              async: false,
              cache: false,
              contentType: false,
              processData: false,
              success: function (data) {
                  if (data.fail) {
                      $('#frm input.required, #frm textarea.required').each(function () {
                          index = $(this).attr('name');
                          if (index in data.errors) {
                              $("#form-" + index + "-error").addClass("has-error");
                              $("#" + index + "-error").html(data.errors[index]);
                          }
                          else {
                              $("#form-" + index + "-error").removeClass("has-error");
                              $("#" + index + "-error").empty();
                          }
                      });
                      $('#focus').focus().select();
                  } else {
                      $(".has-error").removeClass("has-error");
                      $(".help-block").empty();
                      $('.loading').hide();
                  }
                  swal({
                    title: '<i>Factura generada</i><br> Ahora puede imprimir: ',
                    type: 'info',
                    html: '<a href="/Factura/facturaventa?id='+data.invoice_id+'" class="btn btn-info" target="_blank"><i class="fa fa-file-pdf-o"></i>Factura</a>',
                    allowOutsideClick: false,
                    confirmButtonText:
                      '<i class="fa fa-thumbs-up"></i> OK',
                  }).then(function (result) {
                    if (result.value) {
                      location.reload();
                    }
                  })
              },
              error: function (xhr, textStatus, errorThrown) {
                  alert(errorThrown);
              }
              
            });
            return false;
          }
          else if((tp-ta)<0)
          {
            swal({
            type: 'error',
            title: 'Oops...',
            text: 'Tiene saldo a favor, debe realizar la devolución!',
          })
          }
          else
          {
            swal({
            type: 'error',
            title: 'Oops...',
            text: 'Aún debe, no es posible facturar!',
          })
          }
          }); 
      $("#frmpayments").submit(function (event) {
        event.preventDefault();
        $('.loading').show();
        var form = $(this);
        var data = new FormData($(this)[0]);
        var url = form.attr("action");
        var tp = parseInt($('#totalproductos').text());
        var ta = parseInt($('#totalabonos').text());
        var pricepayment = parseInt($('#pricepayment').val());
        $.ajax({
        type: "POST",
          url: url,
          data: data,
          async: false,
          cache: false,
          contentType: false,
          processData: false,
          success: function (data) {
              if (data.fail) {
                  $('#frm input.required, #frm textarea.required').each(function () {
                      index = $(this).attr('name');
                      if (index in data.errors) {
                          $("#form-" + index + "-error").addClass("has-error");
                          $("#" + index + "-error").html(data.errors[index]);
                      }
                      else {
                          $("#form-" + index + "-error").removeClass("has-error");
                          $("#" + index + "-error").empty();
                      }
                  });
                  $('#focus').focus().select();
              } else {
                  $(".has-error").removeClass("has-error");
                  $(".help-block").empty();
                  $('.loading').hide();
              }
              var date = data.created_at.date;
              var order_id = $('#order_idpayment').val();
              $('#tablepayments > tbody').append('<tr id=payment'+data.id+'><td>'+date.substring(0, 19)+'</td><td>'+data.price+'</td><td><a class="btn btn-primary" href="/Factura/abono?payment_id='+data.id+'&order_id='+order_id+'" role="button" target="_blank"><i class="fa fa-eye"></td><td><input type="button" value="Eliminar" class="btn btn-danger"  onclick="return dltrw(\''+data.id+'\', \'/Payments/\', \'payment\')"; /></td></tr>'); 
              $('#totalabonos').text(parseInt($('#totalabonos').text())+ parseInt(data.price));
              $('#pricepayment').val('');
              $('#pendiente').text(parseInt($('#totalproductos').text()) - parseInt($('#totalabonos').text()));
          },
          error: function (xhr, textStatus, errorThrown) {
              alert(errorThrown);
          }
          
        });
        return false;
       
    });

    $("#quickstore").submit(function (event) {
        event.preventDefault();
        $('.loading').show();
        var form = $(this);
        var data = new FormData($(this)[0]);
        var url = form.attr("action");
        $.ajax({
        type: "POST",
          url: url,
          data: data,
          async: false,
          cache: false,
          contentType: false,
          processData: false,
          success: function (data) {
              if (data.fail) {
                  $('#frm input.required, #frm textarea.required').each(function () {
                      index = $(this).attr('name');
                      if (index in data.errors) {
                          $("#form-" + index + "-error").addClass("has-error");
                          $("#" + index + "-error").html(data.errors[index]);
                      }
                      else {
                          $("#form-" + index + "-error").removeClass("has-error");
                          $("#" + index + "-error").empty();
                      }
                  });
                  $('#focus').focus().select();
              } else {
                  $(".has-error").removeClass("has-error");
                  $(".help-block").empty();
                  $('.loading').hide();
              }
              $('#identificationinit').prop('disabled', true);
              $('#firstname').prop('disabled', true);
              $('#lastname').prop('disabled', true);
              $('#email').prop('disabled', true);
              $('#phone').prop('disabled', true);
              $('#birthday').prop('disabled', true);
              $('#profession').prop('disabled', true);

              $('#product_id').prop('disabled', false);
              $('#submitproduct').prop('disabled', false);
              $('#submitdiscount').prop('disabled', false);
              $('#pricediscount').prop('disabled', false);
              $('#submitpayment').prop('disabled', false);
              $('#pricepayment').prop('disabled', false);
              $('#pricediscount').prop('disabled', false);
              $('#btnFacturar').prop('disabled', false);

              $('#order_idinvoice').val(data.order_id);
              $('#order_idproduct').val(data.order_id);           
              $('#order_iddiscount').val(data.order_id);    
              $('#order_idpayment').val(data.order_id);    
          },
          error: function (xhr, textStatus, errorThrown) {
              alert(errorThrown);
          }
          
        });
        return false;
       
    });  
      $("#frmproducts").submit(function (event) {
          event.preventDefault();
          $('.loading').show();
          var form = $(this);
          var data = new FormData($(this)[0]);
          var url = form.attr("action");
          $.ajax({
              type: "POST",
              url: url,
              data: data,
              async: false,
              cache: false,
              contentType: false,
              processData: false,
              success: function (data) {
                console.log(data);
                  if (data.fail) {
                      $('#frm input.required, #frm textarea.required').each(function () {
                          index = $(this).attr('name');
                          if (index in data.errors) {
                              $("#form-" + index + "-error").addClass("has-error");
                              $("#" + index + "-error").html(data.errors[index]);
                          }
                          else {
                              $("#form-" + index + "-error").removeClass("has-error");
                              $("#" + index + "-error").empty();
                          }
                      });
                      $('#focus').focus().select();
                  } else {
                      $(".has-error").removeClass("has-error");
                      $(".help-block").empty();
                      $('.loading').hide();
                  }
                  var date = data.created_at.date;
                  $('#tableproducts > tbody').append('<tr id=product'+data.id+'><td>'+date.substring(0, 19)+'</td><td>'+data.name+'</td><td>'+data.price+'</td><td><input type="button" value="Eliminar" class="btn btn-danger"  onclick="return dltrw(\''+data.id+'\', \'/Orderproducts/\', \'product\')"; /></td></tr>'); 
                  $('#totalproductos').text(parseInt($('#totalproductos').text())+ parseInt(data.price));
                  $('#value').val(parseInt($('#totalproductos').text())+ parseInt(data.price));
                  $('#pendiente').text(parseInt($('#totalproductos').text()) - parseInt($('#totalabonos').text()));
              },
              error: function (xhr, textStatus, errorThrown) {
                  alert(errorThrown);
              }
          });
          return false;
      });  
      $("#frmdiscount").submit(function (event) {
            event.preventDefault();
            $('.loading').show();
            var form = $(this);
            var data = new FormData($(this)[0]);
            var url = form.attr("action");
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.fail) {
                        $('#frm input.required, #frm textarea.required').each(function () {
                            index = $(this).attr('name');
                            if (index in data.errors) {
                                $("#form-" + index + "-error").addClass("has-error");
                                $("#" + index + "-error").html(data.errors[index]);
                            }
                            else {
                                $("#form-" + index + "-error").removeClass("has-error");
                                $("#" + index + "-error").empty();
                            }
                        });
                        $('#focus').focus().select();
                    } else {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").empty();
                        $('.loading').hide();
                    }
                    var date = data.created_at.date;
                    $('#tableproducts > tbody').append('<tr id=discount'+data.id+'><td>'+date.substring(0, 19)+'</td><td>'+data.name+'</td><td>'+data.price+'</td><td><input type="button" value="Eliminar" class="btn btn-danger" onclick="return dltrw(\''+data.id+'\', \'/Orderproducts/\', \'discount\')"; /></td></tr>'); 
                    $('#totalproductos').text(parseInt($('#totalproductos').text())+ parseInt(data.price));
                    $('#value').val(parseInt($('#totalproductos').text())+ parseInt(data.price));
                    $('#pricediscount').val('');
                    $('#pendiente').text(parseInt($('#totalproductos').text()) - parseInt($('#totalabonos').text()));
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
            return false;
        });  

      $(function ()
      {
        $('#birthdayDiv').datetimepicker({
            viewMode: 'years',
          format: 'YYYY-MM-DD',
        });
      }); 

      function dltrw(id, url, from)
        {
            $('.loading').show();
            var data = {
                    "id": id,
                    "_method": 'DELETE',
                    "_token": "{{ csrf_token() }}",
                };
            var url = url+id;
            $.ajax({
                type: "DELETE",
                url: url,
                data: data,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    if (data.fail) {
                        $('#frm input.required, #frm textarea.required').each(function () {
                            index = $(this).attr('name');
                            if (index in data.errors) {
                                $("#form-" + index + "-error").addClass("has-error");
                                $("#" + index + "-error").html(data.errors[index]);
                            }
                            else {
                                $("#form-" + index + "-error").removeClass("has-error");
                                $("#" + index + "-error").empty();
                            }
                        });
                        $('#focus').focus().select();
                    } else {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").empty();
                        $('.loading').hide();
                    }
                    $('#'+from+id).remove();
                    if(data.type == 1)
                    {
                        $('#totalproductos').text(parseInt($('#totalproductos').text())- parseInt(data.price));
                        $('#value').val(parseInt($('#totalproductos').text())- parseInt(data.price));
                        $('#pendiente').text(parseInt($('#totalproductos').text()) - parseInt($('#totalabonos').text()));
                    }
                    else if(data.type == 2)
                    {
                        $('#totalabonos').text(parseInt($('#totalabonos').text())- parseInt(data.price));           
                        $('#pendiente').text(parseInt($('#totalproductos').text()) + parseInt($('#totalabonos').text()));
                    }
                    
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
            return false;
        }
    </script>
