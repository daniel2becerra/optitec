<div class="form-group">
  {!!Form::label('Nombres *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12', 'for'=>'firstname'])!!}
  <div class="col-md-6 col-sm-6 col-xs-12">
    {!!Form::text('firstname',null, ['id'=>'firstname', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Nombres'])!!}
  </div>
</div>
<div class="form-group">
  {!!Form::label('Apellidos *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12', 'for'=>'lastname'])!!}
  <div class="col-md-6 col-sm-6 col-xs-12">
    {!!Form::text('lastname',null, ['id'=>'lastname', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Apellidos'])!!}
  </div>
</div>
<div class="form-group">
  {!!Form::label('Correo *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12', 'for'=>'email'])!!}
  <div class="col-md-6 col-sm-6 col-xs-12">
    {!!Form::text('email',null, ['email', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Correo'])!!}
  </div>
</div>
<div class="form-group">
  {!!Form::label('Teléfono *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12', 'for'=>'user'])!!}
  <div class="col-md-6 col-sm-6 col-xs-12">
    {!!Form::text('phone',null, ['id'=>'phone', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Teléfono'])!!}
  </div>
</div>
<div class="form-group">
  {!!Form::label('F. de nacimiento *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12', 'for'=>'birthday'])!!}
  <div class='col-md-6 col-sm-6 col-xs-12 input-group date' id='birthdayDiv'>
      {!!Form::text('birthday', null, ['id'=>'birthday', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'YYYY-MM-DD'])!!}
      <span class="input-group-addon">
          <span class="glyphicon glyphicon-calendar"></span>
      </span>
    </div>
</div>
<div class="form-group">
  {!!Form::label('Profesión *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12', 'for'=>'register'])!!}
  <div class="col-md-6 col-sm-6 col-xs-12">
    {!!Form::text('profession',null, ['id'=>'profession', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Profesión'])!!}
  </div>
</div>
<div class="ln_solid"></div>
<script>
  $(function ()
  {
    $('#birthdayDiv').datetimepicker({
        viewMode: 'years',
      format: 'YYYY-MM-DD',
    });
  });
  function pulsar(e) { 
  tecla = (document.all) ? e.keyCode :e.which; 
  return (tecla!=13); 
  } 
  </script>
