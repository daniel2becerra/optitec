<div class="form-group">
  {!!Form::label('Nombre *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12', 'for'=>'name'])!!}
  <div class="col-md-6 col-sm-6 col-xs-12">
    {!!Form::text('name',null, ['id'=>'name', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Nombre'])!!}
  </div>
</div>
<div class="form-group">
  {!!Form::label('Dirección *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12', 'for'=>'address '])!!}
  <div class="col-md-6 col-sm-6 col-xs-12">
    {!!Form::text('address',null, ['id'=>'address', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Dirección'])!!}
  </div>
</div>
<div class="form-group">
  {!!Form::label('Teléfono *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12', 'for'=>'phone'])!!}
  <div class="col-md-6 col-sm-6 col-xs-12">
    {!!Form::text('phone',null, ['phone', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Teléfono'])!!}
  </div>
</div>
<div class="form-group">
  {!!Form::label('Nombre del contacto *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12','for'=>'contactName'])!!}
  <div class="col-md-6 col-sm-6 col-xs-12">
    {!!Form::text('contactName', null, ['id'=>'contactName', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Nombre del contacto'])!!}
  </div>
</div>
<div class="form-group">
  {!!Form::label('Teléfono del contacto *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12', 'for'=>'contactPhone'])!!}
  <div class="col-md-6 col-sm-6 col-xs-12">
    {!!Form::text('contactPhone',null, ['id'=>'contactPhone', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Teléfono del contacto'])!!}
  </div>
</div>
<div class="ln_solid"></div>
<div id="full">
<?php
  $cuentaPrecios = 0;
  $cuentaTipos = 0;
?>
@foreach($tipos as $tipo)
<?php
  $cuentaMarcas = 0;
 ?>
  <div id="tipo<?php echo $cuentaTipos?>">
    <div class="form-group">
      {!!Form::label('Tipo del lente *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12'])!!}
      <div class="col-md-4 col-sm-4 col-xs-">
        {!!Form::text('nametypelens[]', $tipo->NAMETYPE, ['class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Tipo del lente', 'required' => 'true'])!!}
      </div>
      <?php 
        if($cuentaTipos==0){echo ("<a class='btn btn-success' id='tmml'><i class='fa fa-plus'></i></a>");}else{echo ("<a class='btn btn-danger' id='subtmml'><i class='fa fa-minus'></i></a>");};
       ?>
    </div>
    <button id="addMaterial<?php echo $cuentaTipos?>" type="button" class="btn btn-primary">Agregar material</button>  
    <button id="delMaterial<?php echo $cuentaTipos?>" type="button" class="btn btn-danger">Eliminar último material</button>    
    <table id="tabla<?php echo $cuentaTipos?>" class="table table-hover table-bordered">
      <thead>
        <tr>
          <th>Material\Marca</th>
          <th>Precio</th>
        </tr>
      </thead>
      <tbody>
          @foreach($materiales as $material)
            @if($tipo->IDTYPE == $material->IDTYPE)
              <tr>
              <td><input name="material[<?php echo $cuentaTipos?>][]" class="form-control" type="text" size="16" value="{{$material->NAMEMATERIAL}}" placeholder="Material" required="true"></td>
              <td><input name="precio[<?php echo $cuentaTipos?>][]" class="form-control" type="text" size="16" value="{{$material->PRICEMATERIAL}}" placeholder="Precio" required="true"></td>
               </tr>
            @endif
          @endforeach
      </tbody>
    </table>
    <?php 
    echo("<script>
          $('#addMaterial".($cuentaTipos)."').click(function()
          {
            var tds=$('#tabla".($cuentaTipos)." tr:first th').length;
            var nuevaFila='<tr>';
            nuevaFila+='<th><input name=\"material[".($cuentaTipos)."][]\" class=\"form-control\" type=\"text\" size=\"16\" placeholder=\"Material\"  required=\"true\"></th>';
            for(var i=1;i<tds;i++)
            {
                nuevaFila+='<td><input name=\"precio[".($cuentaTipos)."][]\" class=\"form-control\" type=\"text\" size=\"16\" placeholder=\"$$$\"  required=\"true\"></td>';
            }
            nuevaFila+='</tr>';
            $('#tabla".($cuentaTipos)."').append(nuevaFila);
          });

          $('#addMarca".($cuentaTipos)."').click(function()
          {
            var c = 0;
            $('#tabla".($cuentaTipos)." tr').each(function()
            {
              if(c==0)
              {
                $(this).append('<th scope=\"row\"><input name=\"marca[".($cuentaTipos)."][]\" class=\"form-control\" type=\"text\" size=\"16\" placeholder=\"Marca\"  required=\"true\"></th>');
              }
              else
              {
                $(this).append('<td><input name=\"precio[".($cuentaTipos)."][]\" class=\"form-control\" type=\"text\" size=\"16\" placeholder=\"$$$\"  required=\"true\"></td>');
              }
              c++;
            });
          });
          $('#delMaterial".($cuentaTipos)."').click(function()
          {
            var trs=$('#tabla".($cuentaTipos)." tr').length;
            if(trs>2)
            {
                $('#tabla".($cuentaTipos)." tr:last').remove();
            }
          });

          $('#delMarca".($cuentaTipos)."').click(function()
          {
            var tds=$('#tabla".($cuentaTipos)." tr:first th').length;
            if(tds>2)
            {
              $('#tabla".($cuentaTipos)." th:last-child, #tabla".($cuentaTipos)." td:last-child').remove();
            }
          });
        </script>");
    ?>
    <div class="ln_solid"></div>
  </div>
  <?php 
      $cuentaTipos += 1;
   ?>
@endforeach
</div>


<div id="fLens">
  <?php
    $flatFilter = 0;
  ?>
  @foreach($filtros as $filtro)
  <?php
    echo("<div id='fillen'><div class='form-group'><div class='col-md-1 '><label for='Filtro *: ' class='control-label'>Filtro *: </label></div><div class='col-md-5'><input id='filter0' class='form-control' placeholder='Filtro' name='filter[]' type='text' value='".$filtro->name."' required='true'></div><div class='col-md-1'><label for='Precio *: ' class='control-label'>Precio *: </label></div><div class='col-md-4'><input id='price' class='form-control' placeholder='Precio' name='price[]' type='text' value='".$filtro->price."' required='true'></div>");if($flatFilter==0){echo ("<a class='btn btn-success' id='fl'><i class='fa fa-plus'></i> </a></div></div>");}else{echo ("<a class='btn btn-danger' id='Subfl'><i class='fa fa-minus'></i></a></div></div>");};
    $flatFilter = $flatFilter + 1;
  ?>  
  @endforeach
</div>

<script>
  $('#fl').click(function(e) 
  {
    $('#fLens').append("<div id='fillen'><div class='form-group'><div class='col-md-1 '><label for='Filtro *: ' class='control-label'>Filtro *: </label></div><div class='col-md-5'><input id='filter0' class='form-control' placeholder='Filtro' name='filter[]' type='text' required='true'></div><div class='col-md-1'><label for='Precio *: ' class='control-label'>Precio *: </label></div><div class='col-md-4'><input id='price' class='form-control' placeholder='Precio' name='price[]' type='text' required='true'></div><a class='btn btn-danger' id='Subfl'><i class='fa fa-minus'></i> </a></div></div>");
  });
  $(document).on('click', '#Subfl', function()
  {
    $(this).parents('div').eq(1).remove();
  }); 

  $(document).on('click', '#subtmml', function(){
    $(this).parents('div').eq(1).remove();
  });

  $(document).on('click', '#tmml', function()
  {
    var counttipos = $("div[id*='tipo']").length;
    $('#full').append("<div id='tipo"+counttipos+"'><div class='form-group'><label class='control-label col-md-3 col-sm-3 col-xs-12'>Tipo del lente *:</label><div class='col-md-4 col-sm-4 col-xs-'><input class= 'form-control col-md-7 col-xs-12', placeholder='Tipo del lente' type='text' name='nametypelens[]' required='true'></div><a class='btn btn-danger' id='subtmml'><i class='fa fa-minus'></i></a></div><button id='addMaterial"+counttipos+"' type='button' class='btn btn-primary'>Agregar material</button><button id='delMaterial"+counttipos+"' type='button' class='btn btn-danger'>Eliminar último material</button><table id='tabla"+counttipos+"' class='table table-hover table-bordered'><thead><tr><th>Material\\Marca</th><th>Precio</th></tr></thead><tbody><tr><th scope='row'><input name='material["+counttipos+"][]' class='form-control' type='text' size='16' placeholder='Material' required='true'></th><td><input name='precio["+counttipos+"][]' class='form-control' type='text' size='16' placeholder='$$$' required='true'></td></tr></tbody></table><div class='ln_solid'></div><script> $('#addMaterial"+counttipos+"').click(function(){var tds=$('#tabla"+counttipos+" tr:first th').length;var nuevaFila='<tr>';nuevaFila+=\"<th><input name='material["+counttipos+"][]' class='form-control' type='text' size='16' placeholder='Material' required='true'></th>\";for(var i=1;i<tds;i++){nuevaFila+=\"<td><input name='precio["+counttipos+"][]' class='form-control' type='text' size='16'placeholder='$$$' required='true'></td>\";}nuevaFila+=\"</tr>\";$('#tabla"+counttipos+"').append(nuevaFila);});$('#addMarca"+counttipos+"').click(function(){var c = 0; $('#tabla"+counttipos+" tr').each(function(){if(c==0){$(this).append(\"<th scope='row'><input name='marca["+counttipos+"][]' class='form-control' type='text' size='16' placeholder='Marca' required='true'></th>\");} else{$(this).append(\"<td><input name='precio["+counttipos+"][]' class='form-control' type='text' size='16' placeholder='$$$' required='true'></td>\");} c++;});});$('#delMaterial"+counttipos+"').click(function(){var trs=$('#tabla"+counttipos+" tr').length;if(trs>2){$('#tabla"+counttipos+" tr:last').remove();}});$('#delMarca"+counttipos+"').click(function(){var tds=$('#tabla"+counttipos+" tr:first th').length;if(tds>2){$('#tabla"+counttipos+" th:last-child, #tabla"+counttipos+" td:last-child').remove();}});<\/script></div>");
  });
  
</script>