<div class="form-group">
  {!!Form::label('Nombre *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12', 'for'=>'name'])!!}
  <div class="col-md-6 col-sm-6 col-xs-12">
    {!!Form::text('name',null, ['id'=>'name', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Nombre'])!!}
  </div>
</div>
<div class="form-group">
  {!!Form::label('Referencia *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12', 'for'=>'reference '])!!}
  <div class="col-md-6 col-sm-6 col-xs-12">
    {!!Form::text('reference',null, ['id'=>'reference', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Referencia'])!!}
  </div>
</div>
<div class="form-group">
  {!!Form::label('Color *: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12', 'for'=>'color'])!!}
  <div class="col-md-6 col-sm-6 col-xs-12">
    {!!Form::text('color',null, ['color', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Color'])!!}
  </div>
</div>
<div class="ln_solid"></div>
