<div class="row">
    <div class='col-sm-5 col-sm-offset-1'>
        {!!Form::label('Identificación *: ', null, ['for'=>'identification'])!!}
        {!!Form::text('identification',$Client->identification, ['id'=>'identification', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Identificación', 'readonly'=>'true'])!!}
    </div> 
    <div class="col-md-5">
        <div class="form-group">
          {!!Form::label('Nombre Completo *: ', null, ['for'=>'name'])!!}
          {!!Form::text('name',$Client->firstname.' '.$Client->lastname, ['id'=>'name', 'class'=>'form-control', 'placeholder'=>'Nombres completos', 'readonly'=>'true'])!!}
        </div>
    </div>
</div>
<div class="row">
  <div class="col-sm-2 col-sm-offset-1">                            
    <div class="form-group">
      {!!Form::label('Od Esfera*: ', null, ['for'=>'OdEsferaRxF'])!!}
      {!!Form::text('OdEsferaRxF', $MedicalHistory->OdEsferaRxF, ['id'=>'OdEsferaRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Od Esfera ', 'readonly'=>'true'])!!}
    </div>
  </div> 
  <div class="col-sm-2">                            
    <div class="form-group">
      {!!Form::label('Od Cilindro*: ', null, ['for'=>'OdCilindroRxF'])!!}
      {!!Form::text('OdCilindroRxF', $MedicalHistory->OdCilindroRxF, ['id'=>'OdCilindroRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Od Cilindro ', 'readonly'=>'true'])!!}
    </div>
  </div> 
  <div class="col-sm-2">                            
    <div class="form-group">
      {!!Form::label('Od Eje*: ', null, ['for'=>'OdEjeRxF'])!!}
      {!!Form::text('OdEjeRxF', $MedicalHistory->OdEjeRxF, ['id'=>'OdEjeRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Od Eje ', 'readonly'=>'true'])!!}
    </div>
  </div> 
  <div class="col-sm-2">                            
    <div class="form-group">
      {!!Form::label('Od av.*: ', null, ['for'=>'OdAvRxF'])!!}
      {!!Form::text('OdAvRxF', $MedicalHistory->OdAvRxF, ['id'=> 'OdAvRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'av. Od ', 'readonly'=>'true'])!!}
    </div>
  </div> 
  <div class="col-sm-2">                            
    <div class="form-group">
      {!!Form::label('Od add *: ', null, ['for'=>'OdAddRxF'])!!}
      {!!Form::text('OdAddRxF', $MedicalHistory->OdAddRxF, ['id'=>'OdAddRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Od add ', 'readonly'=>'true'])!!}
    </div>
  </div>
  <div class="col-sm-2 col-sm-offset-1">                            
    <div class="form-group">
      {!!Form::label('Oi Esfera*: ', null, ['for'=>'OiEsferaRxF'])!!}
      {!!Form::text('OiEsferaRxF', $MedicalHistory->OiEsferaRxF, ['id'=>'OiEsferaRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Oi Esfera ', 'readonly'=>'true'])!!}
    </div>
  </div>      
  <div class="col-sm-2">                            
    <div class="form-group">
      {!!Form::label('Oi Cilindro*: ', null, ['for'=>'OiCilindroRxF'])!!}
      {!!Form::text('OiCilindroRxF', $MedicalHistory->OiCilindroRxF, ['id'=>'OiCilindroRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Oi Cilindro ', 'readonly'=>'true'])!!}
    </div>
  </div>      
  <div class="col-sm-2">                            
    <div class="form-group">
      {!!Form::label('Oi Eje*: ', null, ['for'=>'OiEjeRxF'])!!}
      {!!Form::text('OiEjeRxF', $MedicalHistory->OiEjeRxF, ['id'=>'OiEjeRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Oi Eje ', 'readonly'=>'true'])!!}
    </div>
  </div>      
  <div class="col-sm-2">                            
    <div class="form-group">
      {!!Form::label('Oi av.*: ', null, ['for'=>'OiAvRxF'])!!}
      {!!Form::text('OiAvRxF', $MedicalHistory->OiAvRxF, ['id'=>'OiAvRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Oi av.', 'readonly'=>'true'])!!}
    </div>
  </div> 
  <div class="col-sm-2">                            
    <div class="form-group">
      {!!Form::label('Oi add *: ', null, ['for'=>'OiAddRxF'])!!}
      {!!Form::text('OiAddRxF', $MedicalHistory->OiAddRxF, ['id'=>'OiAddRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Oi add ', 'readonly'=>'true'])!!}
    </div>
  </div>
  <div class="col-sm-3 col-sm-offset-1">                            
    <div class="form-group">
      {!!Form::label('DP *: ', null, ['for'=>'DpRxF'])!!}
      {!!Form::text('DpRxF', $MedicalHistory->DpRxF, ['id'=>'DpRxF', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'DP ', 'readonly'=>'true'])!!}
    </div>
  </div>
  <div class="col-sm-3">                            
    <div class="form-group">
      {!!Form::label('Alt *: ', null, ['for'=>'Alt'])!!}
      {!!Form::text('alt',$Order->alt, ['id'=>'alt', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Alt ', 'readonly'=>'true'])!!}
    </div>
  </div>
  <div class="col-sm-2">                            
    <div class="form-group">
      <label>Laboratorio <span class="required">*</span></label>
      {!!Form::text('lab_id',$Lab->name, ['id'=>'lab_id', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Alt ', 'readonly'=>'true'])!!}
    </div>
  </div>
  <div class="col-sm-2">                            
    <div class="form-group">
      <label>Tipo de lente <span class="required">*</span></label>
      {!!Form::text('Typelen_id',$Typelen->name, ['id'=>'Typelen_id', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Alt ', 'readonly'=>'true'])!!}
    </div>
  </div> 
  <div class="col-sm-3 col-sm-offset-1">                            
    <div class="form-group">
      <label>Material del lente <span class="required">*</span></label>
      {!!Form::text('Materiallen_id',$Materiallen->name, ['id'=>'Materiallen_id', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Alt ', 'readonly'=>'true'])!!}
    </div>
  </div> 
  <div class="col-sm-4">                            
    <div class="form-group">
      <label>filtro de lente <span class="required">*</span></label>
      {!!Form::text('Filterlen_id',$Filterlen->name, ['id'=>'Filterlen_id', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Alt ', 'readonly'=>'true'])!!}
    </div>
  </div>  
  <div class="col-sm-10 col-sm-offset-1">                            
    <div class="form-group">
      {!!Form::label('Observación *: ', null, ['for'=>'observation'])!!}
      {!!Form::textarea('observation',$Order->observation, ['id'=>'observation', 'class'=>'form-control col-md-7 col-xs-12', 'rows'=>'2', 'placeholder'=>'Observación', 'readonly'=>'true'])!!}
    </div>
  </div>         
</div>
<div class="ln_solid"></div>