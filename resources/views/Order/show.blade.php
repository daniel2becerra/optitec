@extends('layouts.admin')
	@section('content')
		@include('alerts.AlertsRequest')
		@include('alerts.SuccessRequest')
		@include('alerts.ErrorsRequest')
		<div class="">
			<div class="page-title">
				<div class="title_left">
					<h3>Pedidos</h3>
				</div>
			</div>
		  	<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2>Pedido <small>Plantilla de creación de pedidos</small></h2>
						  	<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<br />
							@include('Forms.OrdersShow')
							<div class="col-md-12">
								<div class="panel panel-default">
								  <!-- Default panel contents -->
									<div class="panel-heading">Productos</div>
									<div class="panel-body">
										@if(!$Factura)
											{!!Form::open(['route'=>'Orderproducts.store', 'method'=>'POST', 'autocomplete'=>'off', 'class'=>'form-horizontal form-label-left', 'id'=>'frmproducts'])!!}
												<div class="form-group">
													<div class="col-md-3">
														{!!Form::select('product_id', $products, null, ['id'=>'product_id', 'class'=> 'selectClass', 'placeholder'=>'Seleccione una opción'])!!}
														{!!Form::hidden('order_id', $Order->id, ['id'=>'order_id'])!!}
														{!!Form::hidden('typeproduct_id', 3, ['id'=>'typeproduct_id'])!!}
													</div>
													<div class="col-md-3">
														{!!Form::submit('Agregar',['id'=>'finish', 'class'=>'btn btn-finish btn-fill btn-primary btn-wd'])!!}
													</div>
								    		{!!Form::close()!!}
								    		{!!Form::open(['route'=>'Orderproducts.store', 'method'=>'POST', 'autocomplete'=>'off', 'class'=>'form-horizontal form-label-left', 'id'=>'frmdiscount'])!!}
													<div class="col-md-3">
														{!!Form::text('price', null, ['id'=>'price', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Descuento'])!!}
														{!!Form::hidden('product_id', 1, ['id'=>'product_id'])!!}
														{!!Form::hidden('order_id', $Order->id, ['id'=>'order_id'])!!}
														{!!Form::hidden('typeproduct_id', 4, ['id'=>'typeproduct_id'])!!}
													</div>
													<div class="col-md-3">
														{!!Form::submit('Agregar',['id'=>'finish', 'class'=>'btn btn-finish btn-fill btn-primary btn-wd'])!!}
													</div>
								    			</div>
								    		{!!Form::close()!!}
									    @endif
									</div>
									<table id="tableproducts" class="table table-bordered table-hover">
										<thead>
											<th>Fecha</th>
											<th>Nombre</th>
											<th>Valor</th>	
											<th>Borrar</th>
										</thead>
										<?php $totalproductos = 0; ?>
										<tbody>
										@foreach($Orderproducts as $Orderproduct)										
											<tr id="product{{$Orderproduct->id}}">
												<td>{{$Orderproduct->created_at}}</td>
												<td>{{$Orderproduct->productname}}</td>
												<td>{{$Orderproduct->price}}</td>
												<td><input type="button" value="Eliminar" class="btn btn-danger" onclick="return dltrw({{$Orderproduct->id}}, '/Orderproducts/', 'product')" ;=""></td>
											</tr>
											<?php $totalproductos += $Orderproduct->price; ?>										
										@endforeach	
										</tbody>
									</table>
								</div>
								<div class="form-group">
								  {!!Form::label('TOTAL PRODUCTOS: ', null, ['class'=>'control-label col-md-4 col-sm-3 col-xs-12', 'for'=>'name'])!!}
								  <div>
								    {!!Form::label($totalproductos, $totalproductos, ['id'=>'totalproductos'])!!}
								  </div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="panel panel-default">
								  <!-- Default panel contents -->
									<div class="panel-heading">Abonos</div>
									<div class="panel-body">
										@if(!$Factura)
										{!!Form::open(['route'=>'Payments.store', 'method'=>'POST', 'autocomplete'=>'off', 'class'=>'form-horizontal form-label-left', 'id'=>'frmpayments'])!!}
										<div class="form-group">
									      <div class="col-md-4">
									        {!!Form::text('price',null, ['id'=>'price', 'class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'Abono'])!!}
									        {!!Form::hidden('order_id', $Order->id, ['id'=>'order_id'])!!}
									      </div>
									      {!!Form::submit('Agregar',['id'=>'finish', 'class'=>'btn btn-finish btn-fill btn-primary btn-wd'])!!}
									    {!!Form::close()!!}
									    </div>
									    @endif
									</div>
									<?php $totalabonos = 0; ?>
									<table id="tablepayments" class="table table-bordered table-hover">
										<?php $totalabonos = 0; ?>
										<thead>
											<th>Fecha</th>
											<th>Valor</th>	
											<th>Ver</th>
											<th>Eliminar</th>
										</thead>
										<tbody>
											@foreach($Payments as $Payment)
											<tr id="payment{{$Payment->id}}">
												<td>{{$Payment->created_at}}</td>
												<td>{{$Payment->price}}</td>
												<td><a class="btn btn-primary" href="/Factura/abono?payment_id={{$Payment->id}}&order_id={{$Order->id}}" role="button" target="_blank"><i class="fa fa-eye"></i></a></td>
												<td><input type="button" value="Eliminar" class="btn btn-danger" onclick="return dltrw({{$Payment->id}}, '/Payments/', 'payment')" ;=""></td>
											</tr>
											<?php $totalabonos += $Payment->price; ?>
											@endforeach	
										</tbody>	
									</table>
								</div>
								<div class="form-group">
								  {!!Form::label('TOTAL ABONOS: ', null, ['class'=>'control-label col-md-4 col-sm-3 col-xs-12', 'for'=>'name'])!!}
								  <div>
								    {!!Form::label($totalabonos, $totalabonos, ['id'=>'totalabonos'])!!}
								  </div>
								</div>
								<div class="form-group">
					                {!!Form::label('SALDO PENDIENTE: ', null, ['class'=>'control-label col-md-4 col-sm-3 col-xs-12', 'for'=>'name'])!!}
					                <div>
					                  {!!Form::label( $totalproductos - $totalabonos, null, ['id'=>'pendiente'])!!}
					                </div>
					              </div>
								<div class="x_content">
						        	<br />
						        	{!!Form::open(['route'=>'Invoices.store', 'method'=>'POST', 'autocomplete'=>'off', 'class'=>'form-horizontal form-label-left', 'id'=>'Facturar'])!!}
						        		{!!Form::hidden('order_id', $Order->id, ['id'=>'order_id'])!!}{!!Form::hidden('order_id', $Order->id, ['id'=>'order_id'])!!}
					        			{!!Form::hidden('value', null, ['id'=>'value'])!!}
										{!!Form::submit('Facturar',['class'=>'btn btn-primary'])!!}
									{!!Form::close()!!}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> 
		</div>
		<script type="text/javascript">
			$(document).ready(function()
			  {
			      $('.selectClass').select2();
			      $('#value').val(parseInt($('#totalproductos').text()));
			  });
			$("#Facturar").submit(function (event){
        		event.preventDefault();
			    $('.loading').show();
			    var form = $(this);
			    var data = new FormData($(this)[0]);
			    var url = form.attr("action");
			    var tp = parseInt($('#totalproductos').text());
			    var ta = parseInt($('#totalabonos').text());
			    if ((tp-ta)==0)
			    {
			    	$.ajax({
		    		type: "POST",
			        url: url,
			        data: data,
			        async: false,
			        cache: false,
			        contentType: false,
			        processData: false,
			        success: function (data) {
			            if (data.fail) {
			                $('#frm input.required, #frm textarea.required').each(function () {
			                    index = $(this).attr('name');
			                    if (index in data.errors) {
			                        $("#form-" + index + "-error").addClass("has-error");
			                        $("#" + index + "-error").html(data.errors[index]);
			                    }
			                    else {
			                        $("#form-" + index + "-error").removeClass("has-error");
			                        $("#" + index + "-error").empty();
			                    }
			                });
			                $('#focus').focus().select();
			            } else {
			                $(".has-error").removeClass("has-error");
			                $(".help-block").empty();
			                $('.loading').hide();
			            }
		            	swal({
		                title: '<i>Factura generada</i><br> Ahora puede imprimir: ',
		                type: 'info',
		                html: '<a href="/Factura/facturaventa?id='+data.invoice_id+'" class="btn btn-info" target="_blank"><i class="fa fa-file-pdf-o"></i>Factura</a>',
		                allowOutsideClick: false,
		                confirmButtonText:
		                  '<i class="fa fa-thumbs-up"></i> OK',
		              }).then(function (result) {
		                if (result.value) {
		                  location.reload();
		                }
		              })
			        },
			        error: function (xhr, textStatus, errorThrown) {
			            alert(errorThrown);
			        }
			        
			    	});
			    	return false;
			    }
			    else if((tp-ta)<0)
			    {
			    	swal({
					  type: 'error',
					  title: 'Oops...',
					  text: 'Tiene saldo a favor, debe realizar la devolución!',
					})
			    }
			    else
			    {
			    	swal({
					  type: 'error',
					  title: 'Oops...',
					  text: 'Aún debe, no es posible facturar!',
					})
			    }
        	});	
		  $("#frmpayments").submit(function (event) {
		    event.preventDefault();
		    $('.loading').show();
		    var form = $(this);
		    var data = new FormData($(this)[0]);
		    var url = form.attr("action");
		    var tp = parseInt($('#totalproductos').text());
		    var ta = parseInt($('#totalabonos').text());
		    var pricepayment = parseInt($('#price').val());
	    	$.ajax({
    		type: "POST",
	        url: url,
	        data: data,
	        async: false,
	        cache: false,
	        contentType: false,
	        processData: false,
	        success: function (data) {
	            if (data.fail) {
	                $('#frm input.required, #frm textarea.required').each(function () {
	                    index = $(this).attr('name');
	                    if (index in data.errors) {
	                        $("#form-" + index + "-error").addClass("has-error");
	                        $("#" + index + "-error").html(data.errors[index]);
	                    }
	                    else {
	                        $("#form-" + index + "-error").removeClass("has-error");
	                        $("#" + index + "-error").empty();
	                    }
	                });
	                $('#focus').focus().select();
	            } else {
	                $(".has-error").removeClass("has-error");
	                $(".help-block").empty();
	                $('.loading').hide();
	            }
	            var date = data.created_at.date;
	            $('#tablepayments > tbody').append('<tr id=payment'+data.id+'><td>'+date.substring(0, 19)+'</td><td>'+data.price+'</td><td><a class="btn btn-primary" href="/Factura/abono?payment_id='+data.id+'&order_id={{$Order->id}}" role="button" target="_blank"><i class="fa fa-eye"></td><td><input type="button" value="Eliminar" class="btn btn-danger"  onclick="return dltrw(\''+data.id+'\', \'/Payments/\', \'payment\')"; /></td></tr>'); 
	            $('#totalabonos').text(parseInt($('#totalabonos').text())+ parseInt(data.price));
	            $('#pendiente').text(parseInt($('#totalproductos').text()) - parseInt($('#totalabonos').text()));
	        },
	        error: function (xhr, textStatus, errorThrown) {
	            alert(errorThrown);
	        }
	        
	    	});
	    	return false;
		   
		}); 
		$("#frmproducts").submit(function (event) {
		    event.preventDefault();
		    $('.loading').show();
		    var form = $(this);
		    var data = new FormData($(this)[0]);
		    var url = form.attr("action");
		    $.ajax({
		        type: "POST",
		        url: url,
		        data: data,
		        async: false,
		        cache: false,
		        contentType: false,
		        processData: false,
		        success: function (data) {
		            if (data.fail) {
		                $('#frm input.required, #frm textarea.required').each(function () {
		                    index = $(this).attr('name');
		                    if (index in data.errors) {
		                        $("#form-" + index + "-error").addClass("has-error");
		                        $("#" + index + "-error").html(data.errors[index]);
		                    }
		                    else {
		                        $("#form-" + index + "-error").removeClass("has-error");
		                        $("#" + index + "-error").empty();
		                    }
		                });
		                $('#focus').focus().select();
		            } else {
		                $(".has-error").removeClass("has-error");
		                $(".help-block").empty();
		                $('.loading').hide();
		            }
		            var date = data.created_at.date;
		            $('#tableproducts > tbody').append('<tr id=product'+data.id+'><td>'+date.substring(0, 19)+'</td><td>'+data.name+'</td><td>'+data.price+'</td><td><input type="button" value="Eliminar" class="btn btn-danger"  onclick="return dltrw(\''+data.id+'\', \'/Orderproducts/\', \'product\')"; /></td></tr>'); 
		            $('#totalproductos').text(parseInt($('#totalproductos').text())+ parseInt(data.price));
		            $('#value').val(parseInt($('#totalproductos').text())+ parseInt(data.price));
		            $('#pendiente').text(parseInt($('#totalproductos').text()) - parseInt($('#totalabonos').text()));
		        },
		        error: function (xhr, textStatus, errorThrown) {
		            alert(errorThrown);
		        }
		    });
		    return false;
		});  

		$("#frmdiscount").submit(function (event) {
		    event.preventDefault();
		    $('.loading').show();
		    var form = $(this);
		    var data = new FormData($(this)[0]);
		    var url = form.attr("action");
		    $.ajax({
		        type: "POST",
		        url: url,
		        data: data,
		        async: false,
		        cache: false,
		        contentType: false,
		        processData: false,
		        success: function (data) {
		            if (data.fail) {
		                $('#frm input.required, #frm textarea.required').each(function () {
		                    index = $(this).attr('name');
		                    if (index in data.errors) {
		                        $("#form-" + index + "-error").addClass("has-error");
		                        $("#" + index + "-error").html(data.errors[index]);
		                    }
		                    else {
		                        $("#form-" + index + "-error").removeClass("has-error");
		                        $("#" + index + "-error").empty();
		                    }
		                });
		                $('#focus').focus().select();
		            } else {
		                $(".has-error").removeClass("has-error");
		                $(".help-block").empty();
		                $('.loading').hide();
		            }
		            var date = data.created_at.date;
		            $('#tableproducts > tbody').append('<tr id=discount'+data.id+'><td>'+date.substring(0, 19)+'</td><td>'+data.name+'</td><td>'+data.price+'</td><td><input type="button" value="Eliminar" class="btn btn-danger" onclick="return dltrw(\''+data.id+'\', \'/Orderproducts/\', \'discount\')"; /></td></tr>'); 
		            $('#totalproductos').text(parseInt($('#totalproductos').text())+ parseInt(data.price));
		            $('#value').val(parseInt($('#totalproductos').text())+ parseInt(data.price));
		            $('#pendiente').text(parseInt($('#totalproductos').text()) - parseInt($('#totalabonos').text()));
		        },
		        error: function (xhr, textStatus, errorThrown) {
		            alert(errorThrown);
		        }
		    });
		    return false;
		});  

		function dltrw(id, url, from)
        {
            $('.loading').show();
            var data = {
                    "id": id,
                    "_method": 'DELETE',
                    "_token": "{{ csrf_token() }}",
                };
            var url = url+id;
            $.ajax({
                type: "DELETE",
                url: url,
                data: data,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    if (data.fail) {
                        $('#frm input.required, #frm textarea.required').each(function () {
                            index = $(this).attr('name');
                            if (index in data.errors) {
                                $("#form-" + index + "-error").addClass("has-error");
                                $("#" + index + "-error").html(data.errors[index]);
                            }
                            else {
                                $("#form-" + index + "-error").removeClass("has-error");
                                $("#" + index + "-error").empty();
                            }
                        });
                        $('#focus').focus().select();
                    } else {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").empty();
                        $('.loading').hide();
                    }
                    $('#'+from+id).remove();
                    if(data.type == 1)
                    {
                        $('#totalproductos').text(parseInt($('#totalproductos').text())- parseInt(data.price));
                        $('#value').val(parseInt($('#totalproductos').text())- parseInt(data.price));
                        $('#pendiente').text(parseInt($('#totalproductos').text()) - parseInt($('#totalabonos').text()));
                    }
                    else if(data.type == 2)
                    {
                        $('#totalabonos').text(parseInt($('#totalabonos').text())- parseInt(data.price));           
                        $('#pendiente').text(parseInt($('#totalproductos').text()) + parseInt($('#totalabonos').text()));
                    }
                    
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
            return false;
        }
		</script>
	@stop	