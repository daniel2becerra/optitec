<?php

use Illuminate\Database\Seeder;
use Optitec\Role;
use Optitec\User;

class RoleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create( [
		'id'=>NULL,
		'name'=>'SuperUser',
		'deleted_at'=>NULL,
		'created_at'=>'2018-03-25 00:00:00',
		'updated_at'=>'2018-03-25 00:00:00'
		] );

					
		Role::create( [
		'id'=>NULL,
		'name'=>'Administrador',
		'deleted_at'=>NULL,
		'created_at'=>'2018-03-25 00:00:00',
		'updated_at'=>'2018-03-25 00:00:00'
		] );

					
		Role::create( [
		'id'=>NULL,
		'name'=>'Médico',
		'deleted_at'=>NULL,
		'created_at'=>'2018-03-25 00:00:00',
		'updated_at'=>'2018-03-25 00:00:00'
		] );

					
		Role::create( [
		'id'=>NULL,
		'name'=>'Asesor',
		'deleted_at'=>NULL,
		'created_at'=>'2018-03-25 00:00:00',
		'updated_at'=>'2018-03-25 00:00:00'
		] );

		User::create( [
		'id'=>NULL,
		'firstname'=>'Daniel',
		'lastname'=>'Becerra',
		'email'=>'daniel-3223@hotmail.com',
		'user'=>'daniel.becerra',
		'password'=>'IngenieroDev100',
		'role_id'=>'1',
		'register'=>'1087551792',
		'deleted_at'=>NULL,
		'remember_token'=>NULL,
		'created_at'=>'2018-03-25 00:00:00',
		'updated_at'=>'2018-03-25 00:00:00'
		] );
	}
}
