<?php

use Illuminate\Database\Seeder;
use Optitec\Cie10;

class Cie10Seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        

		Cie10::create( [
		'id'=>2,
		'id10'=>'H00',
		'dec10'=>'Orzuelo y calacio',
		'grp10'=>'|VII1',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>3,
		'id10'=>'H000',
		'dec10'=>'Orzuelo y otras inflamaciones profundas del parpado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>4,
		'id10'=>'H001',
		'dec10'=>'Calacio [chalazion]',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>5,
		'id10'=>'H01',
		'dec10'=>'Otras inflamaciones del parpado',
		'grp10'=>'|VII1',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>6,
		'id10'=>'H010',
		'dec10'=>'Blefaritis',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>7,
		'id10'=>'H011',
		'dec10'=>'Dermatosis no infecciosa del parpado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>8,
		'id10'=>'H018',
		'dec10'=>'Otras inflamaciones especificadas del parpado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>9,
		'id10'=>'H019',
		'dec10'=>'Inflamacion del parpado, no especificada',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>10,
		'id10'=>'H02',
		'dec10'=>'Otros trastornos de los parpados',
		'grp10'=>'|VII1',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>11,
		'id10'=>'H020',
		'dec10'=>'Entropion y triquiasis palpebral',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>12,
		'id10'=>'H021',
		'dec10'=>'Ectropion del parpado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>13,
		'id10'=>'H022',
		'dec10'=>'Lagoftalmos',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>14,
		'id10'=>'H023',
		'dec10'=>'Blefarocalasia',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>15,
		'id10'=>'H024',
		'dec10'=>'Blefaroptosis',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>16,
		'id10'=>'H025',
		'dec10'=>'Otros trastornos funcionales del parpado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>17,
		'id10'=>'H026',
		'dec10'=>'Xantelasma del parpado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>18,
		'id10'=>'H027',
		'dec10'=>'Otros trastornos degenerativos del parpado y del area periocular',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>19,
		'id10'=>'H028',
		'dec10'=>'Otros trastornos especificados del parpado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>20,
		'id10'=>'H029',
		'dec10'=>'Trastornos del parpado, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>21,
		'id10'=>'H03',
		'dec10'=>'Trastornos del parpado en enfermedades clasificadas otra parte',
		'grp10'=>'|VII1',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>22,
		'id10'=>'H030',
		'dec10'=>'Infeccion e infestacion parasitarias del parpado en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>23,
		'id10'=>'H031',
		'dec10'=>'Compromiso del parpado en enfermedades infecciosas clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>24,
		'id10'=>'H038',
		'dec10'=>'Compromiso del parpado en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>25,
		'id10'=>'H04',
		'dec10'=>'Trastornos del aparato lagrimal',
		'grp10'=>'|VII1',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>26,
		'id10'=>'H040',
		'dec10'=>'Dacrioadenitis',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>27,
		'id10'=>'H041',
		'dec10'=>'Otros trastornos de la glandula lagrimal',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>28,
		'id10'=>'H042',
		'dec10'=>'Epifora',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>29,
		'id10'=>'H043',
		'dec10'=>'Inflamacion aguda y la no especificada de las vias lagrimales',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>30,
		'id10'=>'H044',
		'dec10'=>'Inflamacion cronica de las vias lagrimales',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>31,
		'id10'=>'H045',
		'dec10'=>'Estenosis e insuficiencia de las vias lagrimales',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>32,
		'id10'=>'H046',
		'dec10'=>'Otros cambios de las vias lagrimales',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>33,
		'id10'=>'H048',
		'dec10'=>'Otros trastornos especificados del aparato lagrimal',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>34,
		'id10'=>'H049',
		'dec10'=>'Trastorno del aparato lagrimal, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>35,
		'id10'=>'H05',
		'dec10'=>'Trastornos de la orbita',
		'grp10'=>'|VII1',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>36,
		'id10'=>'H050',
		'dec10'=>'Inflamacion aguda de la orbita',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>37,
		'id10'=>'H051',
		'dec10'=>'Trastornos inflamatorios cronicos de la orbita',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>38,
		'id10'=>'H052',
		'dec10'=>'Afecciones exoftalmicas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>39,
		'id10'=>'H053',
		'dec10'=>'Deformidad de la orbita',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>40,
		'id10'=>'H054',
		'dec10'=>'Enoftalmia',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>41,
		'id10'=>'H055',
		'dec10'=>'Retencion de cuerpo extraño (antiguo), consecutiva a herida penetrante de la orbita',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>42,
		'id10'=>'H058',
		'dec10'=>'Otros trastornos de la orbita',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>43,
		'id10'=>'H059',
		'dec10'=>'Trastorno de la orbita, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>44,
		'id10'=>'H06',
		'dec10'=>'Trastornos del aparato lagrimal y de la orbita en enfermedades clasificadas en otra parte',
		'grp10'=>'|VII1',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>45,
		'id10'=>'H060',
		'dec10'=>'Trastornos del aparato lagrimal en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>46,
		'id10'=>'H061',
		'dec10'=>'Infeccion o infestacion parasitaria de la orbita en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>47,
		'id10'=>'H062',
		'dec10'=>'Exoftalmia hipertiroidea (e05.',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>48,
		'id10'=>'H063',
		'dec10'=>'Otros trastornos de la orbita en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>49,
		'id10'=>'H10',
		'dec10'=>'Conjuntivitis',
		'grp10'=>'|VII2',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>50,
		'id10'=>'H100',
		'dec10'=>'Conjuntivitis mucopurulenta',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>51,
		'id10'=>'H101',
		'dec10'=>'Conjuntivitis atopica aguda',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>52,
		'id10'=>'H102',
		'dec10'=>'Otras conjuntivitis agudas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>53,
		'id10'=>'H103',
		'dec10'=>'Conjuntivitis aguda, no especificada',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>54,
		'id10'=>'H104',
		'dec10'=>'Conjuntivitis cronica',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>55,
		'id10'=>'H105',
		'dec10'=>'Blefaroconjuntivitis',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>56,
		'id10'=>'H108',
		'dec10'=>'Otras conjuntivitis',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>57,
		'id10'=>'H109',
		'dec10'=>'Conjuntivitis, no especificada',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>58,
		'id10'=>'H11',
		'dec10'=>'Otros trastornos de la conjuntiva',
		'grp10'=>'|VII2',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>59,
		'id10'=>'H110',
		'dec10'=>'Pterigion',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>60,
		'id10'=>'H111',
		'dec10'=>'Degeneraciones y depositos conjuntivales',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>61,
		'id10'=>'H112',
		'dec10'=>'Cicatrices conjuntivales',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>62,
		'id10'=>'H113',
		'dec10'=>'Hemorragia conjuntival',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>63,
		'id10'=>'H114',
		'dec10'=>'Otros trastornos vasculares y quistes conjuntivales',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>64,
		'id10'=>'H118',
		'dec10'=>'Otros trastornos especificados de la conjuntiva',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>65,
		'id10'=>'H119',
		'dec10'=>'Trastorno de la conjuntiva, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>66,
		'id10'=>'H13',
		'dec10'=>'Trastornos de la conjuntiva en enfermedades clasificadas otra parte',
		'grp10'=>'|VII2',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>67,
		'id10'=>'H130',
		'dec10'=>'Infeccion filarica de la conjuntiva (b74.',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>68,
		'id10'=>'H131',
		'dec10'=>'Conjuntivitis en enfermedades infecciosas y parasitarias clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>69,
		'id10'=>'H132',
		'dec10'=>'Conjuntivitis en otras enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>70,
		'id10'=>'H133',
		'dec10'=>'Penfigoide ocular (l12.',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>71,
		'id10'=>'H138',
		'dec10'=>'Otros trastornos de la conjuntiva en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>72,
		'id10'=>'H15',
		'dec10'=>'Trastornos de la esclerotica',
		'grp10'=>'|VII3',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>73,
		'id10'=>'H150',
		'dec10'=>'Escleritis',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>74,
		'id10'=>'H151',
		'dec10'=>'Episcleritis',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>75,
		'id10'=>'H158',
		'dec10'=>'Otros trastornos de la esclerotica',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>76,
		'id10'=>'H159',
		'dec10'=>'Trastornos de la esclerotica, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>77,
		'id10'=>'H16',
		'dec10'=>'Queratitis',
		'grp10'=>'|VII3',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>78,
		'id10'=>'H160',
		'dec10'=>'Ulcera de la cornea',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>79,
		'id10'=>'H161',
		'dec10'=>'Otras queratitis superficiales sin cunjuntivitis',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>80,
		'id10'=>'H162',
		'dec10'=>'Queratoconjuntivitis',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>81,
		'id10'=>'H163',
		'dec10'=>'Queratitis intersticial y profunda',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>82,
		'id10'=>'H164',
		'dec10'=>'Neovascularizacion de la cornea',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>83,
		'id10'=>'H168',
		'dec10'=>'Otras queratitis',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>84,
		'id10'=>'H169',
		'dec10'=>'Queratitis, no especificada',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>85,
		'id10'=>'H17',
		'dec10'=>'Opacidades y cicatrices corneales',
		'grp10'=>'|VII3',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>86,
		'id10'=>'H170',
		'dec10'=>'Leucoma adherente',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>87,
		'id10'=>'H171',
		'dec10'=>'Otras opacidades centrales de la cornea',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>88,
		'id10'=>'H178',
		'dec10'=>'Otras opacidades o cicatrices de la cornea',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>89,
		'id10'=>'H179',
		'dec10'=>'Cicatriz u opacidad de la cornea, no especificada',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>90,
		'id10'=>'H18',
		'dec10'=>'Otros trastornos de la cornea',
		'grp10'=>'|VII3',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>91,
		'id10'=>'H180',
		'dec10'=>'Pigmentaciones y depositos en la cornea',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>92,
		'id10'=>'H181',
		'dec10'=>'Queratopatia vesicular',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>93,
		'id10'=>'H182',
		'dec10'=>'Otros edemas de la cornea',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>94,
		'id10'=>'H183',
		'dec10'=>'Cambios en las membranas de la cornea',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>95,
		'id10'=>'H184',
		'dec10'=>'Degeneracion de la cornea',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>96,
		'id10'=>'H185',
		'dec10'=>'Distrofia hereditaria de la cornea',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>97,
		'id10'=>'H186',
		'dec10'=>'Queratocono',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>98,
		'id10'=>'H187',
		'dec10'=>'Otras deformidades de la cornea',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>99,
		'id10'=>'H188',
		'dec10'=>'Otros trastornos especificados de la cornea',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>100,
		'id10'=>'H189',
		'dec10'=>'Trastorno de la cornea, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>101,
		'id10'=>'H19',
		'dec10'=>'Trastornos de la esclerotica y de la cornea en enfermedades clasificadas en otra parte',
		'grp10'=>'|VII3',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>102,
		'id10'=>'H190',
		'dec10'=>'Escleritis y episcleritis en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>103,
		'id10'=>'H191',
		'dec10'=>'Queratitis y queratoconjuntivitis por herpes simple (b00.5)',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>104,
		'id10'=>'H192',
		'dec10'=>'Queratitis y queratoconjuntivitis en enfermedades infecciosas y parasitarias, clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>105,
		'id10'=>'H193',
		'dec10'=>'Queratitis y queratoconjuntivitis en otras enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>106,
		'id10'=>'H198',
		'dec10'=>'Otros trastornos de la esclerotica y de la cornea en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>107,
		'id10'=>'H20',
		'dec10'=>'Iridociclitis',
		'grp10'=>'|VII3',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>108,
		'id10'=>'H200',
		'dec10'=>'Iridociclitis aguda y subaguda',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>109,
		'id10'=>'H201',
		'dec10'=>'Iridociclitis cronica',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>110,
		'id10'=>'H202',
		'dec10'=>'Iridociclitis inducida por trastorno del cristalino',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>111,
		'id10'=>'H208',
		'dec10'=>'Otras iridociclitis especificadas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>112,
		'id10'=>'H209',
		'dec10'=>'Iridociclitis, no especificada',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>113,
		'id10'=>'H21',
		'dec10'=>'Otros trastornos del iris y del cuerpo ciliar',
		'grp10'=>'|VII3',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>114,
		'id10'=>'H210',
		'dec10'=>'Hifema',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>115,
		'id10'=>'H211',
		'dec10'=>'Otros trastornos vasculares del iris y del cuerpo ciliar',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>116,
		'id10'=>'H212',
		'dec10'=>'Degeneracion del iris y del cuerpo ciliar',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>117,
		'id10'=>'H213',
		'dec10'=>'Quiste del iris, del cuerpo ciliar y de la camara anterior',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>118,
		'id10'=>'H214',
		'dec10'=>'Membranas pupilares',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>119,
		'id10'=>'H215',
		'dec10'=>'Otras adherencias y desgarros del iris y del cuerpo ciliar',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>120,
		'id10'=>'H218',
		'dec10'=>'Otros trastornos especificados del iris y del cuerpo ciliar',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>121,
		'id10'=>'H219',
		'dec10'=>'Del iris y del cuerpo ciliar, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>122,
		'id10'=>'H22',
		'dec10'=>'Trastornos del iris y del cuerpo ciliar en enfermedades clasificadas en otra parte',
		'grp10'=>'|VII3',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>123,
		'id10'=>'H220',
		'dec10'=>'Iridociclitis en enfermedades infecciosas y parasitarias clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>124,
		'id10'=>'H221',
		'dec10'=>'Iridociclitis en otras enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>125,
		'id10'=>'H228',
		'dec10'=>'Otros trastornos del iris y del cuerpo ciliar en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>126,
		'id10'=>'H25',
		'dec10'=>'Catarata senil',
		'grp10'=>'|VII4',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>127,
		'id10'=>'H250',
		'dec10'=>'Catarata senil incipiente',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>128,
		'id10'=>'H251',
		'dec10'=>'Catarata senil nuclear',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>129,
		'id10'=>'H252',
		'dec10'=>'Catarata senil, tipo morgagnian',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>130,
		'id10'=>'H258',
		'dec10'=>'Otras cataratas seniles',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>131,
		'id10'=>'H259',
		'dec10'=>'Catarata senil, no especificada',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>132,
		'id10'=>'H26',
		'dec10'=>'Otras cataratas',
		'grp10'=>'|VII4',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>133,
		'id10'=>'H260',
		'dec10'=>'Catarata infantil, juvenil y presenil',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>134,
		'id10'=>'H261',
		'dec10'=>'Catarata traumatica',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>135,
		'id10'=>'H262',
		'dec10'=>'Catarata complicada',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>136,
		'id10'=>'H263',
		'dec10'=>'Catarata inducida por drogas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>137,
		'id10'=>'H264',
		'dec10'=>'Catarata residual',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>138,
		'id10'=>'H268',
		'dec10'=>'Otras formas especificadas de catarata',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>139,
		'id10'=>'H269',
		'dec10'=>'Catarata, no especificada',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>140,
		'id10'=>'H27',
		'dec10'=>'Otros trastornos del cristalino',
		'grp10'=>'|VII4',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>141,
		'id10'=>'H270',
		'dec10'=>'Afaquia',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>142,
		'id10'=>'H271',
		'dec10'=>'Luxacion del cristalino',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>143,
		'id10'=>'H278',
		'dec10'=>'Otros trastornos especificados del cristalino',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>144,
		'id10'=>'H279',
		'dec10'=>'Trastorno del cristalino, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>145,
		'id10'=>'H28',
		'dec10'=>'Catarata y otros trastornos del cristalino en enfermedades clasificadas en otra parte',
		'grp10'=>'|VII4',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>146,
		'id10'=>'H280',
		'dec10'=>'Catarata diabetica (e10',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>147,
		'id10'=>'H281',
		'dec10'=>'Catarata en otras enfermedades endocrinas, nutricionales y metabolicas clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>148,
		'id10'=>'H282',
		'dec10'=>'Catarata en otras enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>149,
		'id10'=>'H288',
		'dec10'=>'Otros trastornos del cristalino en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>150,
		'id10'=>'H30',
		'dec10'=>'Inflamacion coriorretiniana',
		'grp10'=>'|VII5',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>151,
		'id10'=>'H300',
		'dec10'=>'Coriorretinitis focal',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>152,
		'id10'=>'H301',
		'dec10'=>'Coriorretinitis diseminada',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>153,
		'id10'=>'H302',
		'dec10'=>'Ciclitis posterior',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>154,
		'id10'=>'H308',
		'dec10'=>'Otras coriorretinitis',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>155,
		'id10'=>'H309',
		'dec10'=>'Coriorretinitis, no especificada',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>156,
		'id10'=>'H31',
		'dec10'=>'Otros trastornos de la coroides',
		'grp10'=>'|VII5',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>157,
		'id10'=>'H310',
		'dec10'=>'Cicatrices coriorretinianas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>158,
		'id10'=>'H311',
		'dec10'=>'Desgeneracion coroidea',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>159,
		'id10'=>'H312',
		'dec10'=>'Distrofia coroidea hereditaria',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>160,
		'id10'=>'H313',
		'dec10'=>'Hemorragia y ruptura de la coroides',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>161,
		'id10'=>'H314',
		'dec10'=>'Desprendimiento de la coroides',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>162,
		'id10'=>'H318',
		'dec10'=>'Otros trastornos especificados de la coroides',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>163,
		'id10'=>'H319',
		'dec10'=>'Trastorno de la coroides, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>164,
		'id10'=>'H32',
		'dec10'=>'Trastornos coriorretinianos en enfermedades clasificadas otra parte',
		'grp10'=>'|VII5',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>165,
		'id10'=>'H320',
		'dec10'=>'Inflamacion coriorretiniana en enfermedades infecciosas y parasitarias clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>166,
		'id10'=>'H328',
		'dec10'=>'Otros trastornos coriorretinianos en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>167,
		'id10'=>'H33',
		'dec10'=>'Desprendimiento y desgarro de la retina',
		'grp10'=>'|VII5',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>168,
		'id10'=>'H330',
		'dec10'=>'Desprendimiento de la retina con ruptura',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>169,
		'id10'=>'H331',
		'dec10'=>'Retinosquisis y quistes de la retina',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>170,
		'id10'=>'H332',
		'dec10'=>'Desprendimiento seroso de la retina',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>171,
		'id10'=>'H333',
		'dec10'=>'Desgarro de la retina sin desprendimiento',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>172,
		'id10'=>'H334',
		'dec10'=>'Desprendimiento de la retina por traccion',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>173,
		'id10'=>'H335',
		'dec10'=>'Otros desprendimiento de la retina',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>174,
		'id10'=>'H34',
		'dec10'=>'Oclusion vascular de la retina',
		'grp10'=>'|VII5',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>175,
		'id10'=>'H340',
		'dec10'=>'Oclusion arterial transitoria de la retina',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>176,
		'id10'=>'H341',
		'dec10'=>'Oclusion de la arteria central de la retina',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>177,
		'id10'=>'H342',
		'dec10'=>'Otras formas de oclusion de la arteria de la retina',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>178,
		'id10'=>'H348',
		'dec10'=>'Otras oclusiones vasculares retinianas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>179,
		'id10'=>'H349',
		'dec10'=>'Oclusion vascular retiniana, sin otra especificacion',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>180,
		'id10'=>'H35',
		'dec10'=>'Otros trastornos de la retina',
		'grp10'=>'|VII5',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>181,
		'id10'=>'H350',
		'dec10'=>'Retinopatias del fondo y cambios vasculares retinianos',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>182,
		'id10'=>'H351',
		'dec10'=>'Retinopatia de la prematuridad',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>183,
		'id10'=>'H352',
		'dec10'=>'Otras retinopatias proliferativas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>184,
		'id10'=>'H353',
		'dec10'=>'Degeneracion de la macula y del polo posterior del ojo',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>185,
		'id10'=>'H354',
		'dec10'=>'Degeneracion periferica de la retina',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>186,
		'id10'=>'H355',
		'dec10'=>'Distrofia hereditaria de la retina',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>187,
		'id10'=>'H356',
		'dec10'=>'Hemorragia retiniana',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>188,
		'id10'=>'H357',
		'dec10'=>'Separacion de las caspas de la retina',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>189,
		'id10'=>'H358',
		'dec10'=>'Otros trastornos especificados de la retina',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>190,
		'id10'=>'H359',
		'dec10'=>'Trastorno de la retina, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>191,
		'id10'=>'H36',
		'dec10'=>'Trastornos de la retina en enfermedades clasificadas en otra parte',
		'grp10'=>'|VII5',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>192,
		'id10'=>'H360',
		'dec10'=>'Retinopatia diabetica (e10',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>193,
		'id10'=>'H368',
		'dec10'=>'Otros trastornos de la retina en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>194,
		'id10'=>'H40',
		'dec10'=>'Glaucoma',
		'grp10'=>'|VII6',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>195,
		'id10'=>'H400',
		'dec10'=>'Sospecha de glaucoma',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>196,
		'id10'=>'H401',
		'dec10'=>'Glaucoma primario de angulo abierto',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>197,
		'id10'=>'H402',
		'dec10'=>'Glaucoma primario de angulo cerrado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>198,
		'id10'=>'H403',
		'dec10'=>'Glaucoma secundario a traumatismo ocular',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>199,
		'id10'=>'H404',
		'dec10'=>'Glaucoma secundario a inflamacion ocular',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>200,
		'id10'=>'H405',
		'dec10'=>'Glaucoma secundario a otros trastornos del ojo',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>201,
		'id10'=>'H406',
		'dec10'=>'Glaucoma secundario a drogas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>202,
		'id10'=>'H408',
		'dec10'=>'Otros glaucomas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>203,
		'id10'=>'H409',
		'dec10'=>'Glaucoma, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>204,
		'id10'=>'H42',
		'dec10'=>'Glaucoma en enfermedades clasificadasif en otra parte',
		'grp10'=>'|VII6',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>205,
		'id10'=>'H420',
		'dec10'=>'Glaucoma en enfermedades endocrinas, nutricionales y metabolicas, clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>206,
		'id10'=>'H428',
		'dec10'=>'Glaucoma en otras enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>207,
		'id10'=>'H43',
		'dec10'=>'Trastornos del cuerpo vitreo',
		'grp10'=>'|VII7',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>208,
		'id10'=>'H430',
		'dec10'=>'Prolapso del vitreo',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>209,
		'id10'=>'H431',
		'dec10'=>'Hemorragia del vitreo',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>210,
		'id10'=>'H432',
		'dec10'=>'Depositos cristalinos en el cuerpo vitreo',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>211,
		'id10'=>'H433',
		'dec10'=>'Otras opacidades vitreas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>212,
		'id10'=>'H438',
		'dec10'=>'Otros trastornos del cuerpo vitreo',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>213,
		'id10'=>'H439',
		'dec10'=>'Trastornos del cuerpo vitreo, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>214,
		'id10'=>'H44',
		'dec10'=>'Trastornos del globo ocular',
		'grp10'=>'|VII7',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>215,
		'id10'=>'H440',
		'dec10'=>'Endoftalmitis purulenta',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>216,
		'id10'=>'H441',
		'dec10'=>'Otras endoftalmitis',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>217,
		'id10'=>'H442',
		'dec10'=>'Miopia degenerativa',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>218,
		'id10'=>'H443',
		'dec10'=>'Otros trastornos degenerativos del globo ocular',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>219,
		'id10'=>'H444',
		'dec10'=>'Hipotonia ocular',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>220,
		'id10'=>'H445',
		'dec10'=>'Afecciones degenerativas del globo ocular',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>221,
		'id10'=>'H446',
		'dec10'=>'Retencion intraocular de cuerpo extraño magnetico (antiguo)',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>222,
		'id10'=>'H447',
		'dec10'=>'Retencion intraocular de cuerpo extraño no magnetico (antiguo)',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>223,
		'id10'=>'H448',
		'dec10'=>'Otros trastornos del globo ocular',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>224,
		'id10'=>'H449',
		'dec10'=>'Trastorno del globo ocular, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>225,
		'id10'=>'H45',
		'dec10'=>'Trastornos del cuerp vitreo y del globo ocular en enfermedades clasificadas en otra parte',
		'grp10'=>'|VII7',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>226,
		'id10'=>'H450',
		'dec10'=>'Hemorragia del vitreo en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>227,
		'id10'=>'H451',
		'dec10'=>'Endoftalmitis en enfernedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>228,
		'id10'=>'H458',
		'dec10'=>'Otros trastornos del cuerpo vitreo y del globo ocular en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>229,
		'id10'=>'H46',
		'dec10'=>'Neuritis optica',
		'grp10'=>'|VII8',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>230,
		'id10'=>'H47',
		'dec10'=>'Otros trastornos del nervio optico [ ii par ] y de las vias opticas',
		'grp10'=>'|VII8',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>231,
		'id10'=>'H470',
		'dec10'=>'Trastornos del nervio optico, no clasificados en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>232,
		'id10'=>'H471',
		'dec10'=>'Papiledema, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>233,
		'id10'=>'H472',
		'dec10'=>'Atrofia optica',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>234,
		'id10'=>'H473',
		'dec10'=>'Otros trastornos del disco optico',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>235,
		'id10'=>'H474',
		'dec10'=>'Trastornos del quiasma optico',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>236,
		'id10'=>'H475',
		'dec10'=>'Trastornos de otras vias opticas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>237,
		'id10'=>'H476',
		'dec10'=>'Trastornos de la corteza visual',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>238,
		'id10'=>'H477',
		'dec10'=>'Trastornos de las vias opticas, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>239,
		'id10'=>'H48',
		'dec10'=>'Otros trastornos del nervio optico [ ii par ] y de las vias opticas en enfermedades clasificadas en otra parte',
		'grp10'=>'|VII8',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>240,
		'id10'=>'H480',
		'dec10'=>'Atrofia optica en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>241,
		'id10'=>'H481',
		'dec10'=>'Neuritis retrobulbar en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>242,
		'id10'=>'H488',
		'dec10'=>'Otros trastornos del nervio optico y de las vias opticas en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>243,
		'id10'=>'H49',
		'dec10'=>'Estrabismo paralitico',
		'grp10'=>'|VII9',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>244,
		'id10'=>'H490',
		'dec10'=>'Paralisis del nervio motor ocular comun [iii par]',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>245,
		'id10'=>'H491',
		'dec10'=>'Paralisis del nervio patetico [iv par]',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>246,
		'id10'=>'H492',
		'dec10'=>'Paralisis del nervio motor ocular externo [vi par]',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>247,
		'id10'=>'H493',
		'dec10'=>'Oftalmoplejia total (externa)',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>248,
		'id10'=>'H494',
		'dec10'=>'Oftalmoplejia externa progresiva',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>249,
		'id10'=>'H498',
		'dec10'=>'Otros estrabismos paraliticos',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>250,
		'id10'=>'H499',
		'dec10'=>'Estrabismo paralitico, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>251,
		'id10'=>'H50',
		'dec10'=>'Otros estrabismos',
		'grp10'=>'|VII9',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>252,
		'id10'=>'H500',
		'dec10'=>'Estrabismo concomitante convergente',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>253,
		'id10'=>'H501',
		'dec10'=>'Estrabismo concomitante divergente',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>254,
		'id10'=>'H502',
		'dec10'=>'Estrabismo vertical',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>255,
		'id10'=>'H503',
		'dec10'=>'Heterotropia intermitente',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>256,
		'id10'=>'H504',
		'dec10'=>'Otras heterotropias o las no especificadas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>257,
		'id10'=>'H505',
		'dec10'=>'Heteroforia',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>258,
		'id10'=>'H506',
		'dec10'=>'Estrabismo mecanico',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>259,
		'id10'=>'H508',
		'dec10'=>'Otros estrabismos especificados',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>260,
		'id10'=>'H509',
		'dec10'=>'Estrabismo, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>261,
		'id10'=>'H51',
		'dec10'=>'Otros trastornos de los movimientos binoculares',
		'grp10'=>'|VII9',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>262,
		'id10'=>'H510',
		'dec10'=>'Paralisis de la conjugacion de la mirada',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>263,
		'id10'=>'H511',
		'dec10'=>'Exceso e insuficiencia de la convergencia ocular',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>264,
		'id10'=>'H512',
		'dec10'=>'Oftalmoplejia internuclear',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>265,
		'id10'=>'H518',
		'dec10'=>'Otros trastornos especificados de los movimientos binoculares',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>266,
		'id10'=>'H519',
		'dec10'=>'Trastornos del movimiento binocular, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>267,
		'id10'=>'H52',
		'dec10'=>'Trastornos de la acomodacion y de la refraccion',
		'grp10'=>'|VII9',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>268,
		'id10'=>'H520',
		'dec10'=>'Hipermetropia',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>269,
		'id10'=>'H521',
		'dec10'=>'Miopia',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>270,
		'id10'=>'H522',
		'dec10'=>'Astigmatismo',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>271,
		'id10'=>'H523',
		'dec10'=>'Anisometropia y aniseiconia',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>272,
		'id10'=>'H524',
		'dec10'=>'Presbicia',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>273,
		'id10'=>'H525',
		'dec10'=>'Trastornos de la acomodacion',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>274,
		'id10'=>'H526',
		'dec10'=>'Otros trastornos de la refraccion',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>275,
		'id10'=>'H527',
		'dec10'=>'Trastorno de la refraccion, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>276,
		'id10'=>'H53',
		'dec10'=>'Alteraciones de la vision',
		'grp10'=>'|VII10',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>277,
		'id10'=>'H530',
		'dec10'=>'Ambliopia ex anopsia',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>278,
		'id10'=>'H531',
		'dec10'=>'Alteraciones visuales subjetivas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>279,
		'id10'=>'H532',
		'dec10'=>'Diplopia',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>280,
		'id10'=>'H533',
		'dec10'=>'Otros trastornos de la vision binocular',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>281,
		'id10'=>'H534',
		'dec10'=>'Defectos del campo visual',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>282,
		'id10'=>'H535',
		'dec10'=>'Deficiencias de la vision cromatica',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>283,
		'id10'=>'H536',
		'dec10'=>'Ceguera nocturna',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>284,
		'id10'=>'H538',
		'dec10'=>'Otras alteraciones visuales',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>285,
		'id10'=>'H539',
		'dec10'=>'Alteracion visual, no especificada',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>286,
		'id10'=>'H54',
		'dec10'=>'Ceguera y disminucion de la agudeza visual',
		'grp10'=>'|VII10',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>287,
		'id10'=>'H540',
		'dec10'=>'Ceguera de ambos ojos',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>288,
		'id10'=>'H541',
		'dec10'=>'Ceguera de un ojo, vision subnormal del otro',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>289,
		'id10'=>'H542',
		'dec10'=>'Vision subnormal de ambos ojos',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>290,
		'id10'=>'H543',
		'dec10'=>'Disminucion indeterminada de la agudeza visual en ambos ojos',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>291,
		'id10'=>'H544',
		'dec10'=>'Ceguera de un ojo',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>292,
		'id10'=>'H545',
		'dec10'=>'Vision subnormal de un ojo',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>293,
		'id10'=>'H546',
		'dec10'=>'Disminucion indeterminada de la agudeza visual de un ojo',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>294,
		'id10'=>'H547',
		'dec10'=>'Disminucion de la agudeza visual, sin especificacion',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>295,
		'id10'=>'H55',
		'dec10'=>'Nistagmo y otros movimientos oculares irregulares',
		'grp10'=>'|VII11',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>296,
		'id10'=>'H57',
		'dec10'=>'Otros trastornos del ojo y sus anexos',
		'grp10'=>'|VII11',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>297,
		'id10'=>'H570',
		'dec10'=>'Anomalias de la funcion pupilar',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>298,
		'id10'=>'H571',
		'dec10'=>'Dolor ocular',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>299,
		'id10'=>'H578',
		'dec10'=>'Otros trastornos especificados del ojo y sus anexos',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>300,
		'id10'=>'H579',
		'dec10'=>'Trastorno del ojo y sus anexos, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>301,
		'id10'=>'H58',
		'dec10'=>'Otros trastornos del ojo y sus anexos en enfermedades clasificadas en otra parte',
		'grp10'=>'|VII11',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>302,
		'id10'=>'H580',
		'dec10'=>'Anomalias de la funcion pupilar en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>303,
		'id10'=>'H581',
		'dec10'=>'Alteraciones de la vision en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>304,
		'id10'=>'H588',
		'dec10'=>'Otros trastornos especificados del ojo en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>305,
		'id10'=>'H59',
		'dec10'=>'Trastornos del ojo y sus anexos consecutivos a procedimientos, no clasificados en otra parte',
		'grp10'=>'|VII11',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>306,
		'id10'=>'H590',
		'dec10'=>'Sindrome vitreo consecutivo a cirugia de catarata',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>307,
		'id10'=>'H598',
		'dec10'=>'Otros trastornos del ojo y sus anexos, consecutivos a procedimientos',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>308,
		'id10'=>'H599',
		'dec10'=>'Trastorno no especificado del ojo y sus anexos, consecutivo a procedimientos',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>309,
		'id10'=>'H60',
		'dec10'=>'Otitis externa',
		'grp10'=>'|VIII1',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>310,
		'id10'=>'H600',
		'dec10'=>'Absceso del oido externo',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>311,
		'id10'=>'H601',
		'dec10'=>'Celulitis del oido externo',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>312,
		'id10'=>'H602',
		'dec10'=>'Otitis externa maligna',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>313,
		'id10'=>'H603',
		'dec10'=>'Otras otitis externas infecciosas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>314,
		'id10'=>'H604',
		'dec10'=>'Colesteatoma del oido externo',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>315,
		'id10'=>'H605',
		'dec10'=>'Otitis externa aguda, no infecciosa',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>316,
		'id10'=>'H608',
		'dec10'=>'Otras otitis externas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>317,
		'id10'=>'H609',
		'dec10'=>'Otitis externa, sin otra especificacion',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>318,
		'id10'=>'H61',
		'dec10'=>'Otros trastornos del oido externo',
		'grp10'=>'|VIII1',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>319,
		'id10'=>'H610',
		'dec10'=>'Pericondritis del oido externo',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>320,
		'id10'=>'H611',
		'dec10'=>'Afecciones no infecciosas del pabellon auditivo',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>321,
		'id10'=>'H612',
		'dec10'=>'Cerumen impactado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>322,
		'id10'=>'H613',
		'dec10'=>'Estenosis adquirida del conducto auditivo externo',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>323,
		'id10'=>'H618',
		'dec10'=>'Otros trastornos especificados del oido externo',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>324,
		'id10'=>'H619',
		'dec10'=>'Trastorno del oido externo, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>325,
		'id10'=>'H62',
		'dec10'=>'Trastornos del oido externo en enfermedades clasificadas en otra parte',
		'grp10'=>'|VIII1',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>326,
		'id10'=>'H620',
		'dec10'=>'Otitis externa en enfermedades bacterianas clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>327,
		'id10'=>'H621',
		'dec10'=>'Otitis externa en enfermedades virales clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>328,
		'id10'=>'H622',
		'dec10'=>'Otitis externa en micosis',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>329,
		'id10'=>'H623',
		'dec10'=>'Otitis externa en otras enfermedades infecciosas y parasitarias clasificadas en otra pate',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>330,
		'id10'=>'H624',
		'dec10'=>'Otitis externa en otras enfermedades clasificadas en otra pate',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>331,
		'id10'=>'H628',
		'dec10'=>'Otros trastornos del oido externo en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>332,
		'id10'=>'H65',
		'dec10'=>'Otitis media no supurativa',
		'grp10'=>'|VIII2',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>333,
		'id10'=>'H650',
		'dec10'=>'Otitis media aguda serosa',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>334,
		'id10'=>'H651',
		'dec10'=>'Otra otitis media aguda, no supurativa',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>335,
		'id10'=>'H652',
		'dec10'=>'Otitis media cronica serosa',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>336,
		'id10'=>'H653',
		'dec10'=>'Otitis media cronica mucoide',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>337,
		'id10'=>'H654',
		'dec10'=>'Otras otitis medias cronicas no supurativas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>338,
		'id10'=>'H659',
		'dec10'=>'Otitis media no supurativa, sin otra especificacion',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>339,
		'id10'=>'H66',
		'dec10'=>'Otitis media supurativa y la no especificada',
		'grp10'=>'|VIII2',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>340,
		'id10'=>'H660',
		'dec10'=>'Otitis media supurativa aguda',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>341,
		'id10'=>'H661',
		'dec10'=>'Otitis media tubotimpanica supurativa cronica',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>342,
		'id10'=>'H662',
		'dec10'=>'Otitis media supurativa cronica aticoantral',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>343,
		'id10'=>'H663',
		'dec10'=>'Otras otitis medias cronicas supurativas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>344,
		'id10'=>'H664',
		'dec10'=>'Otitis media supurativa, sin otra especificacion',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>345,
		'id10'=>'H669',
		'dec10'=>'Otitis media, no especificada',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>346,
		'id10'=>'H67',
		'dec10'=>'Otitis media en enfermedades clasificadas en otra parte',
		'grp10'=>'|VIII2',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>347,
		'id10'=>'H670',
		'dec10'=>'Otitis media en enfermedades bacterianas clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>348,
		'id10'=>'H671',
		'dec10'=>'Otitis media en enfermedades virales clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>349,
		'id10'=>'H678',
		'dec10'=>'Otitis media en otras enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>350,
		'id10'=>'H68',
		'dec10'=>'Inflamacion y obstruccion de la trompa de eustaquio',
		'grp10'=>'|VIII2',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>351,
		'id10'=>'H680',
		'dec10'=>'Salpingitis eustaquiana',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>352,
		'id10'=>'H681',
		'dec10'=>'Obstruccion de la trompa de eustaquio',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>353,
		'id10'=>'H69',
		'dec10'=>'Otros trastornos de la trompa de eustaquio',
		'grp10'=>'|VIII2',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>354,
		'id10'=>'H690',
		'dec10'=>'Distension de la trompa de eustaquio',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>355,
		'id10'=>'H698',
		'dec10'=>'Otros trastornos especificados de la trompa de eustaquio',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>356,
		'id10'=>'H699',
		'dec10'=>'Trastorno de la trompa de eustaquio, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>357,
		'id10'=>'H70',
		'dec10'=>'Mastoiditis y afecciones relacionadas',
		'grp10'=>'|VIII2',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>358,
		'id10'=>'H700',
		'dec10'=>'Mastoiditis aguda',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>359,
		'id10'=>'H701',
		'dec10'=>'Mastoiditis cronica',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>360,
		'id10'=>'H702',
		'dec10'=>'Petrositis',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>361,
		'id10'=>'H708',
		'dec10'=>'Otras mastoiditis y afecciones relacionadas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>362,
		'id10'=>'H709',
		'dec10'=>'Mastoiditis, no especificada',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>363,
		'id10'=>'H71',
		'dec10'=>'Colesteatoma del oido medio',
		'grp10'=>'|VIII2',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>364,
		'id10'=>'H72',
		'dec10'=>'Perforacion de la membrana timpanica',
		'grp10'=>'|VIII2',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>365,
		'id10'=>'H720',
		'dec10'=>'Perforacion central de la membrana timpanica',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>366,
		'id10'=>'H721',
		'dec10'=>'Perforacion atica de la membrana timpanica',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>367,
		'id10'=>'H722',
		'dec10'=>'Otras perforaciones marginales de la membrana timpanica',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>368,
		'id10'=>'H728',
		'dec10'=>'Otras perforaciones de la membrana timpanica',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>369,
		'id10'=>'H729',
		'dec10'=>'Perforacion de la membrana timpanica, sin otra especificacion',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>370,
		'id10'=>'H73',
		'dec10'=>'Otros trastornos de la membrana timpanica',
		'grp10'=>'|VIII2',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>371,
		'id10'=>'H730',
		'dec10'=>'Miringitis aguda',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>372,
		'id10'=>'H731',
		'dec10'=>'Miringitis cronica',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>373,
		'id10'=>'H738',
		'dec10'=>'Otros trastornos especificados de la membrana timpanica',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>374,
		'id10'=>'H739',
		'dec10'=>'Trastorno de la membrana timpanica, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>375,
		'id10'=>'H74',
		'dec10'=>'Otros trastornos del oido medio y de la apofisis mastoides',
		'grp10'=>'|VIII2',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>376,
		'id10'=>'H740',
		'dec10'=>'Timpanosclerosis',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>377,
		'id10'=>'H741',
		'dec10'=>'Enfermedad adhesiva del oido medio',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>378,
		'id10'=>'H742',
		'dec10'=>'Discontinuidad y dislocacion de los huesecillos del oido',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>379,
		'id10'=>'H743',
		'dec10'=>'Otras anormalidades adquiridas de los huesecillos del oido',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>380,
		'id10'=>'H744',
		'dec10'=>'Polipo del oido medio',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>381,
		'id10'=>'H748',
		'dec10'=>'Otros trastornos especificados del oido medio y de la apofisis mastoides',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>382,
		'id10'=>'H749',
		'dec10'=>'Trastorno del oido medio y de la apofisis mastoides, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>383,
		'id10'=>'H75',
		'dec10'=>'Otros trastornos del oido medio y de la apofisis mastoidesen enfermedades clasificadas en otra parte',
		'grp10'=>'|VIII2',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>384,
		'id10'=>'H750',
		'dec10'=>'Mastoiditis en enfermedades infecciosas y parasitarias clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>385,
		'id10'=>'H758',
		'dec10'=>'Otros trastornos especificados del oido medio y de la apofisis mastoides en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>386,
		'id10'=>'H80',
		'dec10'=>'Otosclerosis',
		'grp10'=>'|VIII3',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>387,
		'id10'=>'H800',
		'dec10'=>'Otosclerosis que afecta la ventana oval, no obliterante',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>388,
		'id10'=>'H801',
		'dec10'=>'Otosclerosis que afecta la ventana oval, obliterante',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>389,
		'id10'=>'H802',
		'dec10'=>'Ostosclerosis coclear',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>390,
		'id10'=>'H808',
		'dec10'=>'Otras otosclerosis',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>391,
		'id10'=>'H809',
		'dec10'=>'Otosclerosis, no especificada',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>392,
		'id10'=>'H81',
		'dec10'=>'Trastornos de la funcion vestibular',
		'grp10'=>'|VIII3',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>393,
		'id10'=>'H810',
		'dec10'=>'Enfermedad de meniere',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>394,
		'id10'=>'H811',
		'dec10'=>'Vertigo paroxistico benigno',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>395,
		'id10'=>'H812',
		'dec10'=>'Neuronitis vestibular',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>396,
		'id10'=>'H813',
		'dec10'=>'Otros vertigos perifericos',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>397,
		'id10'=>'H814',
		'dec10'=>'Vertigo de origen central',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>398,
		'id10'=>'H818',
		'dec10'=>'Otros trastornos de la funcion vestibular',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>399,
		'id10'=>'H819',
		'dec10'=>'Trastorno de la funcion vestibular, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>400,
		'id10'=>'H82',
		'dec10'=>'Sindromes vertiginosos en enfermedades clasificadas en otra parte',
		'grp10'=>'|VIII3',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>401,
		'id10'=>'H83',
		'dec10'=>'Otros trastornos del oido interno',
		'grp10'=>'|VIII3',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>402,
		'id10'=>'H830',
		'dec10'=>'Laberintitis',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>403,
		'id10'=>'H831',
		'dec10'=>'Fistula del laberinto',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>404,
		'id10'=>'H832',
		'dec10'=>'Disfuncion del laberinto',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>405,
		'id10'=>'H833',
		'dec10'=>'Efectos del ruido sobre el oido interno',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>406,
		'id10'=>'H838',
		'dec10'=>'Otros trastornos especificados del oido interno',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>407,
		'id10'=>'H839',
		'dec10'=>'Trastorno del oido interno, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>408,
		'id10'=>'H90',
		'dec10'=>'Hipoacusia conductiva y neurosensorial',
		'grp10'=>'|VIII4',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>409,
		'id10'=>'H900',
		'dec10'=>'Hipoacusia conductiva bilateral',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>410,
		'id10'=>'H901',
		'dec10'=>'Hipoacusia conductiva, unilateral con audicion irrestricta contralateral',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>411,
		'id10'=>'H902',
		'dec10'=>'Hipoacusia conductiva, sin otra especificacion',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>412,
		'id10'=>'H903',
		'dec10'=>'Hipoacusia neurosensorial, bilateral',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>413,
		'id10'=>'H904',
		'dec10'=>'Hipoacusia neurosensorial, unilateral con audicion irrestricta contralateral',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>414,
		'id10'=>'H905',
		'dec10'=>'Hipoacusia neurosensorial, sin otra especificacion',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>415,
		'id10'=>'H906',
		'dec10'=>'Hipoacusia mixta conductiva y neurosensorial, bilateral',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>416,
		'id10'=>'H907',
		'dec10'=>'Hipoacusia mixta conductiva y neurosensorial, unilateral con audicion irrestricta contralateral',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>417,
		'id10'=>'H908',
		'dec10'=>'Hipoacusia mixta conductiva y neurosensorial, no especificada',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>418,
		'id10'=>'H91',
		'dec10'=>'Otras hipoacusias',
		'grp10'=>'|VIII4',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>419,
		'id10'=>'H910',
		'dec10'=>'Hipoacusia ototoxica',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>420,
		'id10'=>'H911',
		'dec10'=>'Presbiacusia',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>421,
		'id10'=>'H912',
		'dec10'=>'Hipoacusia subida idiopatica',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>422,
		'id10'=>'H913',
		'dec10'=>'Sordomudez, no clasificada en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>423,
		'id10'=>'H918',
		'dec10'=>'Otras hipoacusias especificadas',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>424,
		'id10'=>'H919',
		'dec10'=>'Hipoacusia, no especificada',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>425,
		'id10'=>'H92',
		'dec10'=>'Otalgia y secrecion del oido',
		'grp10'=>'|VIII4',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>426,
		'id10'=>'H920',
		'dec10'=>'Otalgia',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>427,
		'id10'=>'H921',
		'dec10'=>'Otorrea',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>428,
		'id10'=>'H922',
		'dec10'=>'Otorragia',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>429,
		'id10'=>'H93',
		'dec10'=>'Otros trastornos del oido no clasificados en otra parte',
		'grp10'=>'|VIII4',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>430,
		'id10'=>'H930',
		'dec10'=>'Trastornos degenerativos y vasculares del oido',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>431,
		'id10'=>'H931',
		'dec10'=>'Tinnitus',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>432,
		'id10'=>'H932',
		'dec10'=>'Otras percepciones auditivas anormales',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>433,
		'id10'=>'H933',
		'dec10'=>'Trastornos del nervio auditivo',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>434,
		'id10'=>'H938',
		'dec10'=>'Otros trastornos especificados del oido',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>435,
		'id10'=>'H939',
		'dec10'=>'Trastorno del oido, no especificado',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>436,
		'id10'=>'H94',
		'dec10'=>'Otros trastornos del oido en enfermedades clasificadas en otra parte',
		'grp10'=>'|VIII4',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>437,
		'id10'=>'H940',
		'dec10'=>'Neuritis del nervio auditivo en enfermedades infecciosas y parasitarias clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>438,
		'id10'=>'H948',
		'dec10'=>'Otros trastornos del oido en enfermedades clasificadas en otra parte',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>439,
		'id10'=>'H95',
		'dec10'=>'Trastornos del oido y de la apofisis mastoides consecutivos a procedimientos no clasificados en otra parte',
		'grp10'=>'|VIII4',
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>440,
		'id10'=>'H950',
		'dec10'=>'Colesteatoma recurrente de la cavidad resultante de la mastoidectomia',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>441,
		'id10'=>'H951',
		'dec10'=>'Otros trastornos posteriores a la mastoidectomia',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>442,
		'id10'=>'H958',
		'dec10'=>'Otros trastornos del oido y de la apofisis mastoides, consecutivos a procedimientos',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );

					
		Cie10::create( [
		'id'=>443,
		'id10'=>'H959',
		'dec10'=>'Trastornos no especificados del oido y de la apofisis mastoides, consecutivos a procedimientos',
		'grp10'=>NULL,
		'created_at'=>'2017-11-13 00:00:00',
		'updated_at'=>'2017-11-13 00:00:00'
		] );
    }
}

