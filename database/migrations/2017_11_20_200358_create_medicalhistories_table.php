<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalhistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicalhistories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->text('reasonconsultation');
            $table->string('odwithoutcorrection');
            $table->string('oiwithoutcorrection');
            $table->string('aowithoutcorrection');
            $table->string('odRx')->nullable();
            $table->string('oiRx')->nullable();
            $table->string('addRx')->nullable();
            $table->string('typeLen')->nullable();
            $table->string('odEE');
            $table->string('oiEE');
            $table->string('covertest');
            $table->string('ppm');
            $table->string('ductions');
            $table->string('versions');
            $table->string('odfondo');
            $table->string('oifondo');
            $table->string('odQ');
            $table->string('oiQ');
            $table->string('odR');
            $table->string('oiR');
            $table->string('odS');
            $table->string('oiS');
            $table->text('prescription')->nullable();
            $table->string('OdEsferaRxF');
            $table->string('OdCilindroRxF')->nullable();
            $table->string('OdEjeRxF')->nullable();
            $table->string('OdAvRxF');
            $table->string('OdAddRxF')->nullable();
            $table->string('OiEsferaRxF');
            $table->string('OiCilindroRxF')->nullable();
            $table->string('OiEjeRxF')->nullable();
            $table->string('OiAvRxF');
            $table->string('OiAddRxF')->nullable();
            $table->string('DpRxF');
            $table->string('UsoRxF');
            $table->text('observationRxF');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medicalhistories');
    }
}
