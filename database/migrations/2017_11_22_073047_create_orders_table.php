<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('typeorder_id');
            $table->integer('client_id');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->integer('medicalhistory_id');
            $table->foreign('medicalhistory_id')->references('id')->on('medicalhistories')->onDelete('cascade');
            $table->integer('lab_id');
            $table->foreign('lab_id')->references('id')->on('labs')->onDelete('cascade');
            $table->integer('materiallen_id');
            $table->foreign('materiallen_id')->references('id')->on('materiallens')->onDelete('cascade');
            $table->integer('marklen_id');
            $table->foreign('marklen_id')->references('id')->on('marklens')->onDelete('cascade');
            $table->integer('typelen_id');
            $table->foreign('typelen_id')->references('id')->on('typelens')->onDelete('cascade');
            $table->integer('filterlen_id');
            $table->foreign('filterlen_id')->references('id')->on('filterlens')->onDelete('cascade');
            $table->string('alt');
            $table->text('observation');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
